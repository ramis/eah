<?php

namespace Shop\Domain\Front\Controller;

use Shop\Common;
use Shop\Filter\Catalog\Product as FilterProduct;
use Shop\Model\Catalog\Product as ModelProduct;
use Shop\Model\Message as ModelMessage;
use Shop\Model\Subscription;

class Message extends Base
{

	public function __construct(Common\Facade $app)
	{
		$this->app = $app;
	}

	/**
	 * Сообщение
	 */
	public function actionMessage()
	{
		try {
			if ($this->app->request->isPost()) {

				$fio = $this->app->request->post('name');
				if ($fio === null) {
					throw new Common\Exception('Поле "Ваше имя" не должно быть пустым');
				}

				$telephone = $this->app->request->post('phone', 0);
				if ($telephone !== 0) {
					$telephone = (int)preg_replace('#\D+#', '', $telephone);
					$telephone = ($telephone > 70000000000) ? $telephone : 0;
				}

				$email = $this->app->request->post('email', '');

				if ($telephone === 0 && $email === '') {
					if ($this->app->request->post('phone') !== null && $telephone === 0) {
						throw new Common\Exception('Поле "телефон" не должно быть пустым');
					} else {
						throw new Common\Exception('Поле "эл почта" не должно быть пустым');
					}
				}

				$message = new ModelMessage();
				$message->message_status_id = 1;
				$message->fio = $fio;
				$message->telephone = $telephone;
				$message->email = $email;
				$message->create_time = date('Y-m-d h:t:s');


				$message->comment = $this->app->request->post('text', '');

				$product = ModelProduct::fetchById($this->app->request->post('product_id', 0));
				if ($product) {
					$message->catalog_product_id = $product->id;
					$message->catalog_product_title = $product->title;
					$message->catalog_product_model = $product->article . '/' . $product->code;
				}

				$message->save();
				$message->send();

				if ($message->comment === 'Уведомить о поступление') {
					$responce_text['success'] = 'Как только данный товар появится в продаже.<br/>' .
						'Мы сразу сообщим вам об этом';
				} else {
					$responce_text['success'] = 'Ваш заявка была успешно сохранена.<br/>' .
						'В ближайщее время наш менеджер свяжется';
				}
			}else{
				throw new Common\Exception('Поле "Ваше имя" не должно быть пустым');
			}
		}catch (Common\Exception $e){
			$responce_text['error'] = $e->getMessage();
		}
		print json_encode($responce_text);
	}

	/**
	 * Подписка
	 */
	public function actionSubscription()
	{
		try{
			if ($this->app->request->isPost()) {
				$fio = $this->app->request->post('fio', '');
				if ($fio === '') {
					throw new Common\Exception('Поле "Ваше имя" не должно быть пустым');
				}

				$telephone = $this->app->request->post('phone', 0);
				if ($telephone !== 0) {
					$telephone = (int)preg_replace('#\D+#', '', $telephone);
					$telephone = ($telephone > 70000000000) ? $telephone : 0;
				}
				if ($telephone === 0){
					throw new Common\Exception('Поле "телефон" не должно быть пустым');
				}

				$email = $this->app->request->post('email', '');
				if ($email === '') {
					throw new Common\Exception('Поле "эл почта" не должно быть пустым');
				}

				$subscription = new Subscription();
				$subscription->fio = $fio;
				$subscription->email = $email;
				$subscription->telephone = $telephone;
				$subscription->count_send = 0;
				$subscription->create_time = date('Y-m-d h:t:s');
				$product = ModelProduct::fetchById($this->app->request->post('product_id', 0));
				if($product){
					$subscription->catalog_product_id = $product->id;
				}

				if($subscription->save()){
					$subscription->send();
					$responce_text['success'] = 'Как только данный товар появится в продаже.<br/>' .
						'Мы сразу сообщим вам об этом';
				}else{
					throw new Common\Exception('Ошибка сохранения подписки');
				}
			}else{
				throw new Common\Exception('Поле "Ваше имя" не должно быть пустым');
			}
		}catch (Common\Exception $e){
			$responce_text['error'] = $e->getMessage();
		}
		print json_encode($responce_text);
	}

}