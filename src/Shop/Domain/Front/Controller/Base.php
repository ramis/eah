<?php

namespace Shop\Domain\Front\Controller;

use Shop\Common\Controller;
use Shop\Model\Catalog\Category;
use Shop\Model\Catalog\Product;
use Shop\Model\Image;
use Shop\Model\Setting;
use Shop\Model\Tips;
use Shop\Filter;
use Shop\Common\Cart;

class Base extends Controller
{

	/**
	 * Заголовки для дизайна
	 *
	 * @return array
	 */
	public function header()
	{

		return array(
			'site_name' => Setting::getValue('site_name'),
			'site_url' => HTTP_FRONT,
			'meta_keyword' => Setting::getValue('meta_keyword'),
			'meta_description' => Setting::getValue('meta_description'),

			'catalog' => $this->gradient('Каталог'),
			'news' => $this->gradient('Новости'),
			'advice' => $this->gradient('Советы'),
			'description' => $this->gradient('Описание'),

			'add_review' => $this->gradient('Добавьте отзыв'),
			'callback' => $this->gradient('Перезвоните мне'),
			'question' => $this->gradient('Задать вопрос'),
			'checkout' => $this->gradient('Оформление заказа'),
			'buy_one_click' => $this->gradient('Купить в 1 клик'),
			'notify_stoke' => $this->gradient('Уведомить о поступление'),

			'rebel' => $this->gradient('Rebel'),
			'royal' => $this->gradient('Royal'),
			'title404' => $this->gradient('Запрашиваемая страница не найдена!'),
		);
	}

	/**
	 * @param $data
	 */
	public function defautlMetaData(&$data)
	{
		$data['title'] = Setting::getValue('main_seo_title');
		$data['meta_keyword'] = Setting::getValue('main_meta_keyword');
		$data['meta_description'] = Setting::getValue('main_meta_description');
	}

	/**
	 * Продукты в корзине
	 * @param array $data
	 */
	public function getCartProduct(array &$data)
	{
		$cart = Cart::getInstance();
		$data['cart'] = array(
			'is_product' => $cart->getCountProduct(),
			'user' => $this->getHumanPluralForm($cart->getCountProduct(), array('товар', 'товара', 'товаров')) .
				' на сумму ' . $cart->getTotal(),
		);
	}

	/**
	 * Продукты
	 *
	 * @param Filter\Catalog\Product $filter_product
	 * @return array
	 */
	public function getProducts(Filter\Catalog\Product $filter_product)
	{
		$data_products = array();
		$products = Product::fetch($filter_product);
		foreach ($products as $p) {
			$data_products[] = $this->getProduct($p);
		}

		return $data_products;
	}

	/**
	 * Product info
	 *
	 * @param Product $product
	 * @return array
	 */
	public function getProduct(Product $product)
	{
		$image = Image::fetchById($product->main_image_id);

		if ($image === null) {
			$image = Image::find_by_hash('no_foto.png');
		}
		$main_image = array(
			'image70x70' => $image->getHttpImage(70, 70),
			'image100x100' => $image->getHttpImage(100, 100),
			'image150x150' => $image->getHttpImage(150, 150),
			'image200x200' => $image->getHttpImage(200, 200),
			'image340x340' => $image->getHttpImage(340, 340),
			'image500x500' => $image->getHttpImage(500, 500)
		);

		$p = array(
			'id' => $product->id,
			'title' => $product->title,
			'code' => $product->code,
			'article' => $product->article,
			'quantity' => $product->quantity,
			'description' => $product->description,
			'rating' => $product->dt_review_rating,
			'count_review' => $product->dt_review_count,
			'category_title' => $product->getMainCatalogCategory()->title,
			'category_url' => $this->app->urlFor('url',
				array('url' => $product->getMainCatalogCategory()->url)),
			'video' => $product->video,
			'price' => $product->price,
			'special_price' => $product->special_price,
			'url' => $this->app->urlFor('product', array('product' => $product->id)),
			'main_image' => $main_image,
		);
		return $p;
	}

	/**
	 * Category info
	 *
	 * @param Category $category
	 * @return array
	 */
	public function getCategory(Category $category)
	{
		$image_href = '';
		$image = Image::fetchById($category->image_id);
		if ($image) {
			$image_href = HTTP_IMAGE_BASE . $image->hash;
		}
		return array(
			'id' => $category->id,
			'title' => $category->title,
			'title_eng' => $category->title_eng,
			'description' => $category->description,
			'image' => $image_href,
			'url' => $this->app->urlFor('url', array('url' => $category->url)),
		);
	}

	/**
	 * Категории каталога
	 *
	 * @param array $data
	 */
	public function getCategoriesCatalog(array &$data)
	{
		//Category
		$filter = new Filter\Catalog\Category;
		$filter
			->publish(true)
			->catalog(true)
			->sort();
		$categories = Category::fetch($filter);
		foreach ($categories as $c) {
			$data['categories'][] = $this->getCategory($c);
		}

		//Советы
		$this->getTips($data);

		//Royal
		$royal = Category::find_by_url('royal');
		if ($royal instanceof Category) {
			$filter = new Filter\Catalog\Category;
			$filter
				->publish(true)
				->parent($royal)
				->sort();
			$categories = Category::fetch($filter);
			foreach ($categories as $c) {
				$data['royal_categories'][] = $this->getCategory($c);
			}
		}

		//Rebel
		$rebel = Category::find_by_url('rebel');
		if ($rebel instanceof Category) {
			$filter = new Filter\Catalog\Category;
			$filter
				->publish(true)
				->parent($rebel)
				->sort();
			$categories = Category::fetch($filter);
			foreach ($categories as $c) {
				$data['rebel_categories'][] = $this->getCategory($c);
			}
		}
	}

	/**
	 * Поиск продукта по фильтру
	 *
	 * @param array $data
	 */
	public function getProductFilterSearch(array &$data)
	{
		$filter = new Filter\Catalog\Product\Filter;
		$filter
			->publish(true)
			->group(true)
			->sort();
		$filter_groups = Product\Filter::fetch($filter);
		foreach ($filter_groups as $g) {
			$filter
				->group(false)
				->parent($g);

			$filters = Product\Filter::fetch($filter);

			$group_filters = array();
			foreach ($filters as $f) {
				$group_filters[] = $this->getProductFilter($f);
			}

			$group = $this->getProductFilter($g);
			$group['filters'] = $group_filters;
			$data['filters'][] = $group;
		}
	}

	/**
	 * Полулярные продукты
	 *
	 * @param array $data
	 */
	public function getProductsPopular(array &$data)
	{
		$filter_product = new Filter\Catalog\Product;
		$filter_product
			->sort('viewed desc')
			->publish(true)
			->limit(4);
		$data['products_popular'] = $this->getProducts($filter_product);
	}

	/**
	 * 404
	 */
	public function view404()
	{
		$main = new Main($this->app);
		$main->action404();
	}


	/**
	 * Gradient title
	 *
	 * @param string $text
	 * @return string
	 */
	public function gradient($text)
	{
		$theColorBegin = 0xc2164e;
		$theColorEnd = 0x651169;
		$theNumSteps = mb_strlen($text, 'UTF-8');

		$theR0 = ($theColorBegin & 0xff0000) >> 16;
		$theG0 = ($theColorBegin & 0x00ff00) >> 8;
		$theB0 = ($theColorBegin & 0x0000ff) >> 0;

		$theR1 = ($theColorEnd & 0xff0000) >> 16;
		$theG1 = ($theColorEnd & 0x00ff00) >> 8;
		$theB1 = ($theColorEnd & 0x0000ff) >> 0;

		$result = '';
		for ($i = 0; $i <= $theNumSteps; $i++) {
			$theR = $this->interpolate($theR0, $theR1, $i, $theNumSteps);
			$theG = $this->interpolate($theG0, $theG1, $i, $theNumSteps);
			$theB = $this->interpolate($theB0, $theB1, $i, $theNumSteps);

			$theVal = ((($theR << 8) | $theG) << 8) | $theB;
			$w = (mb_substr($text, $i, 1, 'UTF-8'));

			$result .= '<span style="color: #' . dechex($theVal) . '">' . $w . '</span>';
		}

		return $result;
	}

	/**
	 * @param $pBegin
	 * @param $pEnd
	 * @param $pStep
	 * @param $pMax
	 * @return mixed
	 */
	private function interpolate($pBegin, $pEnd, $pStep, $pMax)
	{
		if ($pBegin < $pEnd) {
			return (($pEnd - $pBegin) * ($pStep / $pMax)) + $pBegin;
		} else {
			return (($pBegin - $pEnd) * (1 - ($pStep / $pMax))) + $pEnd;
		}
	}

	/**
	 * Советы
	 *
	 * @param array $data
	 */
	protected function getTips(array &$data)
	{
		$filter = new Filter\Tips();
		$filter->limit(1);
		$tips = Tips::fetch($filter);
		$data['tips'] = array();
		foreach ($tips as $t) {
			$data['tips'][] = array(
				'url' => $t->url,
				'description' => $t->description,
				'product' => $this->getProduct($t->getProduct()),
			);
		}
	}

	/**
	 * Pagination
	 *
	 * @param array $data
	 * @param integer $product_total
	 * @param integer $page
	 * @param integer $limit
	 */
	protected function pagination(array &$data, $product_total, $page, $limit)
	{

		$n = ceil($product_total / $limit);
		$data['num_pages'] = array();

		if ($n < 5) {
			for ($i = 1; $i <= $n; $i++) {
				$data['num_pages'][] = $i;
			}
		} elseif ($page < 3) {
			for ($i = 1; $i <= 5; $i++) {
				$data['num_pages'][] = $i;
			}
		} elseif ($page > ($n - 2)) {
			for ($i = $n - 4; $i <= $n; $i++) {
				$data['num_pages'][] = $i;
			}
		} else {
			for ($i = $page - 2; $i <= $page + 2; $i++) {
				$data['num_pages'][] = $i;
			}
		}

		$data['page'] = $page;
		$data['page_last'] = $n;
		$data['page_url'] = '/';
	}

	/**
	 * @param $num int число чего-либо
	 * @param $titles array варинаты написания для количества 1, 2 и 5
	 * @return string
	 */
	function getHumanPluralForm($num, $titles = array('комментарий', 'комментария', 'комментариев'))
	{
		$cases = array(2, 0, 1, 1, 1, 2);
		return $num . " " . $titles[($num % 100 > 4 && $num % 100 < 20) ? 2 : $cases[min($num % 10, 5)]];
	}

}
