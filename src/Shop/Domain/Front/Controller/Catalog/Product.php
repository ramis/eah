<?php

namespace Shop\Domain\Front\Controller\Catalog;

use Shop\Common;
use Shop\Domain\Front\Controller\Base;
use Shop\Filter\Catalog\Product as FilterProduct;
use Shop\Filter\Image as FilterImage;
use Shop\Model\Image as ModelImage;
use Shop\Model\Catalog\Product as ModelProduct;

class Product extends Base
{

	public function __construct(Common\Facade $app)
	{
		$this->app = $app;
	}

	/**
	 * Карточка продукта
	 *
	 * @param integer $product_id
	 */
	public function actionIndex($product_id)
	{
		$data = array();
		$data['headers'] = $this->header();
		$data['headers']['see'] = $this->gradient('Недавно просмотренные товары');
		$data['headers']['buy'] = $this->gradient('Похожие товары');
		$data['headers']['specification'] = $this->gradient('Характеристики');
		$data['headers']['review'] = $this->gradient('Отзывы');
		$data['headers']['video'] = $this->gradient('Видео');

		$product = ModelProduct::fetchById($product_id);
		if (!($product instanceof ModelProduct && (bool)$product->is_publish)) {
			$this->view404();
		}
		$this->getCartProduct($data);

		$data['title'] = $product->seo_title ? $product->seo_title : $product->title;
		if($product->meta_description){
			$data['meta_description'] = $product->meta_description;
		}
		if($product->meta_keyword){
			$data['meta_keyword'] = $product->meta_keyword;
		}

		//Инфо продукта
		$data['product'] = $this->getProduct($product);
		$data['product']['title_h2'] = $this->gradient($product->title);

		$data['category'] = $this->getCategory($product->getMainCatalogCategory());

		//Категории из каталога
		$this->getCategoriesCatalog($data);

		//Фото продукта
		$filter = new FilterImage();
		$filter
			->parent('product')
			->parentId($product->id);
		$data['images'] = array();
		$images = ModelImage::fetch($filter);
		foreach ($images as $i) {
			$data['images'][] = array(
				'image70x70'   => $i->getHttpImage(70, 70),
				'image100x100' => $i->getHttpImage(100, 100),
				'image150x150' => $i->getHttpImage(150, 150),
				'image200x200' => $i->getHttpImage(200, 200),
				'image340x340' => $i->getHttpImage(340, 340),
				'image500x500' => $i->getHttpImage(500, 500)
			);
		}
		//Аттрибуты
		$data['attributes'] = array();
		foreach ($product->getAttributes() as $a) {
			$data['attributes'][] = array(
				'title' => $a->getAttributeTitle(),
				'value' => $a->text,
			);
		}
		//Отзывы
		$filter = new FilterProduct\Review();
		$filter
			->publish(true)
			->product($product);
		$reviews = ModelProduct\Review::fetch($filter);
		$data['reviews'] = array();
		foreach ($reviews as $r) {
			$data['reviews'][] = array(
				'author' => $r->author,
				'text' => $r->text,
				'rating' => $r->rating,
			);
		}

		//С этим товаром покупают
		$data['products_buy'] = array();
		$products_buy = ModelProduct::productBuy($product->id);
		foreach ($products_buy as $p) {
			$product_buy = ModelProduct::fetchById($p->product_id);
			if($product_buy === null){
				continue;
			}
			$data['products_buy'][] = $this->getProduct($product_buy);
		}

		//Недавно просмотренные
		$data['products_see'] = array();
		if (isset($_SESSION) && isset($_SESSION['products_see_ids'])) {

			$product_look_ids = (array)$_SESSION['products_see_ids'];

			unset($product_look_ids[$product->id]);

			foreach ($product_look_ids as $id) {
				$item = ModelProduct::fetchById($id);
				if ($item instanceof ModelProduct) {
					$data['products_see'][] = $this->getProduct($item);
				}
			}

			$product_look_ids[$product->id] = $product->id;
			$_SESSION['products_see_ids'] = $product_look_ids;
		} else {
			$_SESSION['products_see_ids'] = array($product->id => $product->id);
		}

		$this->app->render('product.twig', $data);
	}

	/**
	 * Добавить отзыв
	 *
	 * @param int $product_id
	 */
	public function actionReview($product_id)
	{
		$message = array();
		try {
			$product = ModelProduct::fetchById($product_id);

			if ($product === null) {
				throw new Common\Exception('Ошибка добавления отзыва');
			}

			if ($this->app->request->isPost()) {
				$author = $this->app->request->post('author', '');
				$text = $this->app->request->post('text', '');
				$dignity = $this->app->request->post('dignity', '');
				$shortcomings = $this->app->request->post('shortcomings', '');
				$rating = (int)$this->app->request->post('rating', 1);

				$text = '<p class="txt">' . $text . '</p><p class="txt"><span>Достоинства:</span> ' .
					$dignity . '</p><p class="txt"><span>Недостатки:</span> ' . $shortcomings . '</p>';

				if (mb_strlen($author, 'UTF-8') < 3 || mb_strlen($author, 'UTF-8') > 25) {
					throw new Common\Exception('Введите ваше имя');
				}

				if (mb_strlen($text, 'UTF-8') < 25 || mb_strlen($text, 'UTF-8') > 2000) {
					throw new Common\Exception('Отзыв не должен быть слишком короткий');
				}

				$review = new ModelProduct\Review();
				$review->author = $author;
				$review->email = $this->app->request->post('email', '');
				$review->text = $text;
				$review->rating = ($rating - 1);
				$review->create_time = date('Y-m-d h:t:s');
				$review->catalog_product_id = $product->id;
				$review->is_publish = 0;

				if($review->save()){
					$message['success'] = 'Спасибо за ваш отзыв. Он поступил администратору
					для проверки на спам и вскоре будет опубликован.';
				}else{
					throw new Common\Exception('Ошибка добавления отзыва');
				}
			}else{
				throw new Common\Exception('Ошибка добавления отзыва');
			}
		}catch (Common\Exception $e){
			$message['error'] = $e->getMessage();
		}
		print json_encode($message);
	}

}