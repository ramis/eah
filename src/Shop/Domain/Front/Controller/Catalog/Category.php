<?php

namespace Shop\Domain\Front\Controller\Catalog;

use Shop\Common;
use Shop\Domain\Front\Controller\Base;
use Shop\Filter\Catalog\Product as FilterProduct;
use Shop\Model\Catalog\Product;
use Shop\Model\Catalog\Category as ModelCategory;

class Category extends Base
{

	public function __construct(Common\Facade $app)
	{
		$this->app = $app;
	}

	public function index(ModelCategory $category)
	{
		//Массив данных для twig
		$data = array();
		$data['headers'] = $this->header();

		$data['category'] = $this->getCategory($category);
		$data['category']['title_h2'] = $this->gradient($category->title);

		//Meta
		$data['title'] = $category->seo_title ? $category->seo_title : $category->title;
		if($category->meta_description){
			$data['meta_description'] = $category->meta_description;
		}
		if($category->meta_keyword){
			$data['meta_keyword'] = $category->meta_keyword;
		}

		//Категории из каталога
		$this->getCategoriesCatalog($data);

		$this->getCartProduct($data);

		//Параметры сортировки

		//Номер страницы
		$page = $this->app->request->get('page') ? (int)$this->app->request->get('page') : 1;

		$filter_product = new FilterProduct();
		$filter_product
			->category($category)
			->sort('viewed desc')
			->publish(true);
		$product_total = Product::fetchCount($filter_product);

		//Продукты категории
		$filter_product->limit(10, $page);
		$data['products'] = $this->getProducts($filter_product);

		$this->pagination($data, $product_total, $page, 10);

		$this->app->render('category.twig', $data);

	}

}