<?php

namespace Shop\Domain\Front\Controller;

use Shop\Model\Urls;
use Shop\Model\Setting;
use Shop\Model\Banners;
use Shop\Model\Catalog\Category;
use Shop\Filter;
use Shop\Common;

class Main extends Base
{

	public function __construct(Common\Facade $app)
	{
		$this->app = $app;
	}

	/**
	 * Главная страница
	 */
	public function actionIndex()
	{
		Category::getCatalogCategories();
die;
		//Массив данных для twig
		$data = array();

		$data['headers'] = $this->header();
		$this->defautlMetaData($data);

		$data['seo_text'] = Setting::getValue('main_seo_text');

		$this->getCartProduct($data);

		//Баннеры
		$banners = Banners::fetch(new Filter\Banners);
		foreach ($banners as $b) {
			$data['banners'][] = array(
				'title' => $b->title,
				'url' => $b->url,
				'image' => HTTP_IMAGE_BASE . $b->getImage()->hash,
			);
		}

		//Категории из каталога
		$this->getCategoriesCatalog($data);

		//Из категории "товары недели"
		$category = Category::find_by_url('products_week');
		if ($category instanceof Category) {
			$filter_product = new Filter\Catalog\Product;
			$filter_product
				->category($category)
				->sort('viewed desc')
				->publish(true);
			$data['products_week'] = $this->getProducts($filter_product);
			//Градиентные заголовки
			$data['headers']['products_week'] = $this->gradient($category->title);
		}
		$this->app->render('index.twig', $data);
	}

	/**
	 * 404
	 *
	 * @throws \Slim\Exception\Stop
	 */
	public function action404()
	{
		$this->app->response->status(404);

		//Массив данных для twig
		$data = array();

		$data['headers'] = $this->header();
		$data['title'] = 'Запрашиваемая страница не найдена!';

		//Категории из каталога
		$this->getCategoriesCatalog($data);

		$this->app->render('404.twig', $data);

		$this->app->stop();
	}

	/**
	 * @param string $query
	 */
	public function actionUrl($query)
	{

		$url = Urls::find_by_query($query);

		if ($url !== NUll) {
			$classies = array_map(function($st){ return ucfirst($st); }, explode('_', $url->parent_table));

			$class_name = sprintf("\Shop\Domain\Front\Controller\%s", implode('\\', $classies));

			if (class_exists($class_name)) {
				$catalog_controller = new $class_name($this->app);
				$class_name = sprintf("\Shop\Model\%s", implode('\\', $classies));

				$obj = $class_name::fetchById($url->parent_id);
				if ($obj) {
					$catalog_controller->index($obj);
					return;
				}
			}
		}
		$this->action404();
	}

}