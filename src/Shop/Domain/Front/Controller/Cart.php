<?php

namespace Shop\Domain\Front\Controller;

use Shop\Common;
use Shop\Model\Geo;
use Shop\Model\Order;
use Shop\Model\Catalog\Product;
use Shop\Model\Geo\Ip;
use Shop\Model\Shipping\Geo\Value;
use Shop\Filter\Shipping\Geo\Value as FilterValue;
use Shop\Filter\Geo as FilterGeo;

class Cart extends Base
{

	public function __construct(Common\Facade $app)
	{
		$this->app = $app;
	}

	/**
	 * Корзина
	 */
	public function actionIndex()
	{
		$data = array();
		$data['headers'] = $this->header();
		$data['cart_title'] = $this->gradient('Корзина');
		$data['title'] = 'Корзина';

		$this->getCategoriesCatalog($data);
		$this->getCartProduct($data);

		$products = Common\Cart::getInstance()->getProducts();
		foreach ($products as $p) {
			$product = Product::fetchById($p->catalog_product_id);
			if ($product === null) {
				continue;
			}
			$data['products'][] = array(
				'product' => $this->getProduct($product),
				'total' => $p->total,
				'price' => $p->price,
				'quantity' => $p->quantity,
			);
		}

		$data['geo_selected_id'] = Ip::getGeoIp($_SERVER['REMOTE_ADDR']);
		$data['shipping'] = array();
		$data['shipping_geo'] = array();
		$data['shipping_selected_id'] = 0;
		$data['shipping_geo_selected_id'] = 0;
		$geos = Geo::fetch(new FilterGeo());
		$filter = new FilterValue();

		foreach ($geos as $g) {
			$data['geos'][] = array(
				'id' => $g->id,
				'title' => $g->title,
				'selected' => ($g->id == $data['geo_selected_id'] ? 'selected' : ''),
			);

			$filter->geo($g);
			$values = Value::fetch($filter);
			foreach ($values as $v) {

				$s = $v->getShipping();
				$key = ($s->sort + 1) . $s->id;
				$data['shipping'][$g->id][$key] = array(
					'id' => $s->id,
					'key' => $key,
					'title' => $s->title,
				);

				$data['shipping_geo'][$g->id][$key][] = array(
					'id' => $v->id,
					'description' => $v->description,
					'price' => $v->price,
				);
			}
		}

		$data['shipping'] = json_encode($data['shipping']);
		$data['shipping_geo'] = json_encode($data['shipping_geo']);

		$data['total'] = Common\Cart::getInstance()->getTotal();
		$data['total_full'] = $data['total'];

		$this->app->render('cart.twig', $data);
	}

	/**
	 * Добавление товара в корзину
	 */
	public function actionAdd()
	{
		$message = array();
		try{
			if ($this->app->request->isPost()) {

				$product = Product::fetchById($this->app->request->post('product_id'));
				if ($product === null) {
					throw new Common\Exception('Такой товар не найден!');
				}
				$quantity = (int)$this->app->request->post('quantity');
				$is_quantity_replase = (bool)$this->app->request->post('replase');

				$cart = Common\Cart::getInstance();
				$cart->addProduct($product, $quantity, $is_quantity_replase);

				$message['total'] = $cart->getTotal();
				$message['total_full'] = $cart->getTotalFull();

				foreach ($cart->getProducts() as $p) {
					$message['products'][] = array(
						'product_id' => $p->catalog_product_id,
						'total' => $p->total,
						'price' => $p->price,
						'quantity' => $p->quantity,
					);
				}

				if ($cart->getCountProduct() > 0) {
					$message['link_cart'] =
						$this->getHumanPluralForm($cart->getCountProduct(), array('товар', 'товара', 'товаров')) .
						' на сумму ' . $cart->getTotal();
				} else {
					$message['link_cart'] = 'В корзине пусто!';
				}

				$message['success'] = 'Товар успешно добавлен в вашу корзину!';
			} else {
				throw new Common\Exception('Выберете интересующий вас товар!');
			}
		}catch (Common\Exception $e){
			$message['error'] = $e->getMessage();
		}

		print json_encode($message);
	}

	/**
	 * Удаление товара из корзины
	 */
	public function actionRemove()
	{
		$message = array();
		try{
			if ($this->app->request->isPost()) {

				$product = Product::fetchById($this->app->request->post('product_id'));
				if ($product === null) {
					throw new Common\Exception('Такой товар не найден!');
				}
				$cart = Common\Cart::getInstance();
				$cart->removeProduct($product);

				$message['total'] = $cart->getTotal();
				$message['total_cart'] = 0;
				$message['total_full'] = $message['total'] + $message['total_cart'];
				foreach ($cart->getProducts() as $p) {
					$message['products'][] = array(
						'product_id' => $p->catalog_product_id,
						'total' => $p->total,
						'price' => $p->price,
						'quantity' => $p->quantity,
					);
				}
			} else {
				throw new Common\Exception('Выберете интересующий вас товар!');
			}
		}catch (Common\Exception $e){
			$message['error'] = $e->getMessage();
		}

		print json_encode($message);
	}

	/**
	 * Выбор способа доставки
	 */
	public function actionShipping()
	{
		$message = array();
		try {
			if ($this->app->request->isPost()) {
				$value = Value::fetchById($this->app->request->post('shipping'));
				if ($value === null) {
					throw new Common\Exception('Такая доставка не найдена!');
				} else {
					$cart = Common\Cart::getInstance();
					$cart->setShippingGeoValue($value);
					$message['shipping_id'] = $cart->getShippingGeoValue()->shipping_id;
					$message['total_full'] = $cart->getTotalFull();
				}
			} else {
				throw new Common\Exception('Такая доставка не найдена!');
			}
		}catch (Common\Exception $e){
			$message['error'] = $e->getMessage();
		}

		print json_encode($message);
	}

	/**
	 * Оформление заказа
	 */
	public function actionCheckout()
	{
		$message = array();
		try {
			if ($this->app->request->isPost()) {
				$cart = Common\Cart::getInstance();
				if ($cart->getCountProduct() === 0) {
					throw new Common\Exception('Ваша корзина пуста, выберите какой нибудь товар из каталога');
				}

				foreach ($cart->getProducts() as $p) {
					$product = Product::find($p->catalog_product_id);
					if ($product->minimum > $p->quantity) {
						throw new Common\Exception('Нет необходимого товара на складе');
					}
				}

				$fio = $this->app->request->post('fio', '');
				if ($fio === '') {
					throw new Common\Exception('Поле "фио" не должно быть пустым');
				}
				$phone = $this->app->request->post('phone', 0);
				$phone = (int)preg_replace('#\D+#', '', $phone);
				$phone = ($phone > 70000000000) ? $phone : 0;

				if ($phone === 0) {
					throw new Common\Exception('Поле "телефон" не должно быть пустым');
				}

				$shipping_geo_value = $cart->getShippingGeoValue();
				if($shipping_geo_value === null){
					throw new Common\Exception('Доставка не должна быть пустой');
				}

				$address = $this->app->request->post('address', '');
				if ($address === '' && $shipping_geo_value->shipping_id !== 3) {
					throw new Common\Exception('Поле "адрес" не должно быть пустым');
				}

				if($shipping_geo_value->shipping_id === 2) {
					$index = $this->app->request->post('index', '');
					if ($index === '') {
						throw new Common\Exception('Поле "индекс" не должно быть пустым');
					}
					$area = $this->app->request->post('area', '');
					if ($area === '') {
						throw new Common\Exception('Поле "регион" не должно быть пустым');
					}
					$region = $this->app->request->post('region', '');
					if ($region === '') {
						throw new Common\Exception('Поле "область" не должно быть пустым');
					}
					$city = $this->app->request->post('city', '');
					if ($city === '') {
						throw new Common\Exception('Поле "город" не должно быть пустым');
					}
					$shipping_post = array(
						'index' => $index,
						'area' => $area,
						'region' => $region,
						'city' => $city,
						'fio' => $fio,
						'address' => $address,
					);
				}
				$email = $this->app->request->post('email', '');
				if ($email === '') {
					throw new Common\Exception('Поле "эл почта" не должно быть пустым');
				}

				$shipping_title = $shipping_geo_value->getGeo()->title . ' ' .
					$shipping_geo_value->getShipping()->title . ($shipping_geo_value->description !== '' ?
						'(' . $shipping_geo_value->description . ')' : '');

				$order = new Order();
				$order->fio = $fio;
				$order->telephone = $phone;
				$order->address = $address;
				$order->email = $email;
				$order->shipping_geo_value_id = $shipping_geo_value->id;
				$order->shipping_title = $shipping_title;
				$order->shipping_cost = $shipping_geo_value->cost;
				$order->shipping_post = serialize($shipping_post);
				$order->comment = $this->app->request->post('comment', '');
				$order->order_status_id = 1;
				$order->create_time = date('Y-m-d H:i:s');
				$order->update_time = $order->create_time;

				$order->total = $cart->getTotal();
				if ($order->save()) {
					$cart->clean();
					$message['success'] = 'Ваш заказ был успешно сохранен.<br/>' .
						'В ближайщее время наш менеджер свяжется ' .
						'с вами по указанному вами телефону.';
				}else{
					throw new Common\Exception('Ошибка добавления заказа');
				}
			}else{
				throw new Common\Exception('Поля отмеченные * не должны быть пустыми');
			}
		}catch (Common\Exception $e){
			$message['error'] = $e->getMessage();
		}

		print json_encode($message);
	}
}