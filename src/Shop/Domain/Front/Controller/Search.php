<?php

namespace Shop\Domain\Front\Controller;

use Shop\Common;
use Shop\Model\Catalog\Product;
use Shop\Filter\Catalog\Product as FilterProduct;

class Search extends Base
{

	public function __construct(Common\Facade $app)
	{
		$this->app = $app;
	}

	public function actionIndex()
	{
		//Массив данных для twig
		$data = array();
		$data['headers'] = $this->header();

		$data['search'] = $this->gradient('Поиск');

		//Параметры сортировки
		$search = $this->app->request->get('search') ? (string)$this->app->request->get('search') : '';
		//Номер страницы
		$page = $this->app->request->get('page') ? (int)$this->app->request->get('page') : 1;

		$this->getCartProduct($data);

		//Категории из каталога
		$this->getCategoriesCatalog($data);
		$filter_product = new FilterProduct();
		$filter_product
			->sort('viewed desc')
			->publish(true);

		if($search !== ''){
			$filter_product->title($search);
		}
		$product_total = Product::fetchCount($filter_product);
		$filter_product->limit(10, $page);
		$data['products'] = $this->getProducts($filter_product);

		$this->pagination($data, $product_total, $page, 10);
		$this->app->render('search.twig', $data);
	}

}