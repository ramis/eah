<?php

namespace Shop\Domain\Front\Controller;

use Shop\Common;
use Shop\Filter\Catalog\Product as FilterProduct;
use Shop\Model\Catalog\Product as ModelProduct;
use Shop\Model;

class Information extends Base
{

	public function __construct(Common\Facade $app)
	{
		$this->app = $app;
	}

	public function index(Model\Information $information)
	{
		//Массив данных для twig
		$data = array();
		$data['headers'] = $this->header();

		$this->getCategoriesCatalog($data);
		$data['title'] = $information->seo_title ? $information->seo_title : $information->title;
		if($information->meta_description){
			$data['meta_description'] = $information->meta_description;
		}
		if($information->meta_keyword){
			$data['meta_keyword'] = $information->meta_keyword;
		}

		$this->getCartProduct($data);

		//Страница "акции и скидки"
		$data['products'] = array();
		if($information->url === 'action'){
			$filter = new FilterProduct();
			$filter
				->publish(true)
				->specialPrice(0);
			$products = ModelProduct::fetch($filter);
			foreach($products as $product){
				$data['products'][] = $this->getProduct($product);
			}
		}

		$data['information'] = array(
			'id' => $information->id,
			'title' => $information->title,
			'title_h2' => $this->gradient($information->title),
			'description' => $information->description,

		);

		$this->app->render('information.twig', $data);
	}

}