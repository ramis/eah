<?php

namespace Shop\Domain\Front\Controller;

use Shop\Common;
use Shop\Model\Geo;
use Shop\Model\Order as ModelOrder;
use Shop\Filter\Order as FilterOrder;
use Shop\Model\Message as ModelMessage;
use Shop\Filter\Message as FilterMessage;
use Shop\Model\Catalog\Product as ModelProduct;
use Shop\Filter\Catalog\Product as FilterProduct;
use Shop\Model\Shipping\Geo\Value;
use Shop\Model\Image as ModelImage;
use Shop\Filter\Image as FilterImage;

class v3toys extends Base
{

	CONST CODE_OK = 200;
	CONST CODE_ERROR = 400;
	CONST API_VERSION = '0.4';

	/**
	 * @var array
	 */
	private $versions = array();

	/**
	 * @var array
	 */
	private $response = array();

	/**
	 * @var array
	 */
	private $error = array();

	/**
	 * @param Common\Facade $app
	 */
	public function __construct(Common\Facade $app)
	{
		$this->app = $app;

		$this->versions = array(self::API_VERSION => 'apiVersion0_4');
		$this->status = self::CODE_OK;

		$this->response = array(
			'v' => '',
			'method' => '',
			'data' => array(),
		);
		$this->error = array(
			'v' => '',
			"error_code" => "",
			"error_message" => "",
		);
	}

	/**
	 *
	 */
	public function actionApi()
	{

		try {
			if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
				throw new Common\Exception('Do not post request', 1000);
			}

			$raw_request = file_get_contents('php://input');
			$request = json_decode($raw_request, true);

			//Проверяем версию
			if (!isset($request['v'])) {
				throw new Common\Exception('Version can not be null', 1001);
			}

			if (!isset($this->versions[$request['v']])) {
				//Не правильная версия
				throw new Common\Exception('This version is not supported', 1002);
			}

			$func = $this->versions[$request['v']];

			$this->$func($request);

		} catch (Exception $e) {
			$this->status = self::CODE_ERROR;
			$this->error['error_code'] = $e->getCode();
			$this->error['error_message'] = $e->getMessage();
		}
		if ($this->status === self::CODE_ERROR) {
			header("HTTP/1.0 400 Bad Request", true, self::CODE_ERROR);
		}
		print $this->status === self::CODE_OK ? json_encode($this->response) : json_encode($this->error);
	}

	private function apiVersion0_4($request)
	{

		//Проверяем method
		if (!isset($request['method'])) {
			throw new Common\Exception('Method can not be null', 1010);
		}

		//Проверяем парамерты
		if (!(isset($request['params']))) {
			throw new Common\Exception('Params can not be null', 1020);
		}

		$v = $request['v'];
		$method = $request['method'];

		switch ($method) {
			case 'getOrderIdsByPeriod':

				//Проверяем если все входные данные
				if (!(isset($request['params']['start']))) {
					throw new Common\Exception('"start" from params cannot be null', 1021);
				}

				$start = new \DateTime($request['params']['start']);
				$filter = new FilterOrder();
				$filter->start($start->format('Y-m-d H:i:s'));

				$end_time = null;
				if (isset($request['params']['end'])) {
					$end = new \DateTime($request['params']['end']);
					$filter->end($end->format('Y-m-d H:i:s'));
				}

				$orders_ids = array();

				$orders = ModelOrder::fetch($filter);
				foreach ($orders as $o) {
					$orders_ids[] = $o->id;
				}

				$this->response = array(
					'v' => $v,
					'method' => $method,
					'data' => array(
						'orders_ids' => $orders_ids,
					),
				);
				break;
			case 'getOrderDataById':

				if (!(isset($request['params']['order_id']))) {
					throw new Common\Exception('"order_id" from params cannot be null', 1022);
				}

				$order = ModelOrder::fetchById((int)$request['params']['order_id']);

				$data_order = array();

				if ($order) {

					$order_products = array();
					foreach($order->getProducts() as $product){
						$order_products[] = array(
							'product_id' => (int)$product->catalog_product_code,
							'price' => (int)$product->price,
							'quantity' => (int)$product->quantity,
						);
					}

					$shipping_geo = Value::fetchById($order->shipping_geo_value_id);
					$shipping_method = '';
					$shipping_cost = 0;
					$shipping_data = array();
					switch ($shipping_geo->shipping_id) {
						case 1:
							$geo = Geo::fetchById($shipping_geo->geo_id);

							$city = $geo->title === 'Москва' ? 'Москва до МКАД' : $geo->title;
							$city = $city === 'Санкт-Петербург' ? 'Санкт-Петербург до КАД' : $city;

							$shipping_method = 'COURIER';
							$shipping_cost = $order->shipping_cost;
							$shipping_data = array(
								'city' => $city,
								'address' => $order->address,
							);
							break;
						case 3:
							$geo = Geo::fetchById($shipping_geo->geo_id);
							$city = $geo->title === 'Московская область' ? 'Москва' : $geo->title;
							$city = $city === 'Ленинградская область' ? 'Санкт-Петербург' : $city;

							$shipping_method = 'PICKUP';
							$shipping_cost = $order->shipping_cost;
							$shipping_data = array(
								'city' => $city,
								'point_id' => $shipping_geo->point_id,
							);
							break;
						case 2:
							$geo = json_decode($order->shipping_post, true);
							$shipping_method = 'POST';
							$shipping_cost = $order->shipping_cost;
							$shipping_data = array(
								'index' => $geo['index'],
								'area' => $geo['area'],
								'region' => $geo['region'],
								'city' => $geo['city'],
								'address' => $order->address,
								'recipient' => $order->fio
							);

							break;
						default:
							break;
					}
					$data_order = array(
						'order_id' => $order->id,
						'fake' => false,
						'created_at' => $order->create_time,
						'full_name' => $order->fio,
						'phone' => $order->telephone,
						'email' => $order->email,

						'products' => $order_products,

						'shipping_method' => $shipping_method,
						'shipping_cost' => $shipping_cost,
						'shipping_data' => $shipping_data,

						'comment' => $order->comment,
						'discount' => 0,
					);
				}
				$this->response = array(
					'v' => $v,
					'method' => $method,
					'data' => $data_order,
				);

				break;

			case 'getProductDataById':

				if (!(isset($request['params']['product_id']))) {
					throw new Common\Exception('"product_id" from params cannot be null', 1023);
				}

				$product_id = (int)$request['params']['product_id'];
				$filter = new FilterProduct;
				$filter->code($product_id);
				$product = ModelProduct::fetchOne($filter);

				if ($product === false) {
					throw new Common\Exception('Product with this "product_id" is no', 400);
				}

				$images = array();
				$filter = new FilterImage();
				$filter
					->parent(ModelImage::PARENT_PRODUCT)
					->parentId($product->id);
				$product_images = ModelImage::fetch($filter);
				foreach($images as $i){
					$product_images[] = HTTP_IMAGE . $i->hash;
				}

				$this->response = array(
					'v' => $v,
					'method' => $method,
					'data' => array(
						'product_id' => (int)$product->code,
						'price' => (int)$product->price,
						'images' => $images,
					),
				);
				break;
			case 'getMessageIdsByPeriod':

				//Проверяем если все входные данные
				if (!(isset($request['params']['start']))) {
					throw new Common\Exception('"start" from params cannot be null', 1021);
				}

				$start = new \DateTime($request['params']['start']);
				$filter = new FilterMessage();
				$filter
					->start($start->format('Y-m-d H:i:s'));

				if (isset($request['params']['end'])) {
					$end = new \DateTime($request['params']['end']);
					$filter->end($end->format('Y-m-d H:i:s'));
				}

				$message_ids = array();
				$messages = ModelMessage::fetch($filter);
				foreach ($messages as $m) {
					$message_ids[] = (int)$m->id;
				}

				$this->response = array(
					'v' => $v,
					'method' => $method,
					'data' => array(
						'message_ids' => $message_ids,
					),
				);
				break;
			case 'getMessageDataById':
				if (!(isset($request['params']['message_id']))) {
					throw new Common\Exception('"message_id" from params cannot be null', 1022);
				}

				$message_id = (int)$request['params']['message_id'];
				$message = ModelMessage::fetchById($message_id);
				$data_message = array();

				if ($message instanceof ModelMessage) {
					$message_products = array();
					$product = ModelProduct::fetchById($message->catalog_product_id);
					if ($product instanceof ModelProduct) {
						$message_products = array(
							'product_id' => $product->code,
							'price' => $product->price,
							'quantity' => 1,
						);
					}
					$data_message = array(
						'message_id' => $message->id,
						'fake' => false,
						'created_at' => $message->create_time,
						'full_name' => $message->fio,
						'phone' => $message->telephone,
						'email' => $message->email,
						'products' => $message_products,
						'comment' => $message->comment,

					);
				}
				$this->response = array(
					'v' => $v,
					'method' => $method,
					'data' => $data_message,
				);
				break;
			default:
				throw new Common\Exception('This method is not supported', 1011);
		}
	}
}