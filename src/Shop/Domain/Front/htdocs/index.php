<?php
define ('PATH', realpath (dirname (__FILE__) . '/../../../../../') . '/');
if(isset($_SERVER['APPLICATION_ENV']) && $_SERVER['APPLICATION_ENV'] !== ''){
	define('APPLICATION_ENV', $_SERVER['APPLICATION_ENV']);
}else{
	define('APPLICATION_ENV', 'local');
}

set_include_path (get_include_path () . PATH_SEPARATOR . PATH);

require PATH . 'vendor/autoload.php';

$bootstrap = new Shop\Domain\Front\Bootstrap();

$bootstrap->initConfig();
$bootstrap->initActiveRecord();
$bootstrap->initSession();
//$bootstrap->initMemcached();
$bootstrap->initSlim(dirname (__FILE__) . '/../template/');
$bootstrap->initRoutes();
$bootstrap->run();

