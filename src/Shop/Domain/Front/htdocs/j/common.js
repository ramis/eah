$(function () {

	$('#button_checkout').click(function () {
		val = $('#shipping_geo_id').val();
		if (val > 0) {
			$('#address').show();
			if (val == 2) {
				$('#show_shipping_post_address').show();
			} else {
				if (val == 3) {
					$('#address').hide();
				}
				$('#show_shipping_post_address').hide();
			}
			$('#checkout').arcticmodal();
		} else {
			$('.no_change_shipping').show();
		}
	});

	$('.colorbox1').click(function () {

		url_image = $(this).attr('href');
		image_current = parseInt($(this).attr('it'));
		$('#colorbox1_image').attr('src', url_image);
		$('.popup_window').css('display', 'block');
		$('#colorbox1').css('display', 'block');
		$('body').css('overflow', 'hidden');
		return false;
	});
	$('.colorbox_image_block a').click(function () {

		url_image = $(this).attr('href');
		image_current = parseInt($(this).attr('it'));
		$('#colorbox1_image').attr('src', url_image);

		return false;
	});
	$('.arrow_left_img').click(function () {

		image_current = parseInt(image_current - 1);
		if (image_current < 0) {
			image_current = image_count;
		}
		url_image = $('#colorbox1_link_' + image_current).attr('href');
		$('#colorbox1_image').attr('src', url_image);
		return false;
	});
	$('.arrow_right_img').click(function () {
		image_current = parseInt(image_current + 1);
		if (image_current > image_count) {
			image_current = 0;
		}
		url_image = $('#colorbox1_link_' + image_current).attr('href');
		$('#colorbox1_image').attr('src', url_image);
		return false;
	});
	$('.review_add_close').live('click', function () {
		$('body').css('overflow', '');
		$('.popup_window').css('display', 'none');
		$(this).parent().css('display', 'none');
	});


	if (window.pluso) {
		if (typeof window.pluso.start == "function") {
			return;
		}
	}
	var listRU = $.masksSort($.masksLoad("/j/phones-ru.json"), ['#'], /[0-9]|#/, "mask");
	var optsRU = {
		inputmask: {
			definitions: {
				'#': {
					validator: "[0-9]",
					cardinality: 1
				}
			},
			//clearIncomplete: true,
			showMaskOnHover: false,
			autoUnmask: true
		},
		match: /[0-9]/,
		replace: '#',
		list: listRU,
		listKey: "mask",
		onMaskChange: function (maskObj, determined) {
			if (determined) {
				if (maskObj.type != "mobile") {
					$("#descr").html(maskObj.city.toString() + " (" + maskObj.region.toString() + ")");
				} else {
					$("#descr").html("мобильные");
				}
			} else {
				$("#descr").html("Маска ввода");
			}
			$(this).attr("placeholder", $(this).inputmask("getemptymask"));
		}
	};
	$('#buy_one_click_phone').inputmasks(optsRU);
	$('#customer_phone').inputmasks(optsRU);
	$('#callback_phone').inputmasks(optsRU);
	$('#notify_stoke_phone').inputmasks(optsRU);

	$('.button_quantity_empty').bind('click', function () {
		product_id = $(this).attr('product_id');
		$('#product_id').val(product_id);
		$('#notify_stoke').arcticmodal();
	});

	$('#button_checkout_add').bind('click', function () {
		$.ajax({
			url: '/checkout/',
			type: 'post',
			data: $('#checkout input[type=\'text\'], #checkout textarea'),
			dataType: 'json',
			beforeSend: function () {
				showSpiner();
			},
			success: function (data) {
				hideSpiner();
				if (data['error']) {
					$('#checkout-ad-title').html('<div class="alert alert-danger" role="alert">' + data['error'] + '</div>');
				}

				if (data['success']) {
					$('#checkout-ad-title').html('<div class="alert alert-success" role="alert">' + data['success'] + '</div>');
					setTimeout(function () {
						$('.arcticmodal-close').trigger('click');
						$('#checkout-ad-title').html('');
						window.location.href = window.location.href;
					}, 2000);
				}
			}
		});
		return false;
	});
	$('.product_quantity_update').change(function () {
		product_id = $(this).attr('product_id');
		quantity = $('#product_quantity_' + product_id + " option:selected").val();

		$.ajax({
			url: '/cart/add/',
			type: 'post',
			data: {product_id: product_id, quantity: quantity, replase: 1},
			dataType: 'json',
			beforeSend: function () {
				showSpiner();
			},
			success: function (data) {
				hideSpiner();
				if (data['products']) {
					for (i = 0; i < data['products'].length; i++) {
						$('#product_price_total_' + data['products'][i]['product_id']).html(data['products'][i]['total']);
					}
					$('#price_total').html(data['total']);
					$('#cart-total').html(data['total_cart']);
					$('#price_total_full').html(data['total_full']);
				}
			}
		});
	});

	$('.remove').bind('click', function () {
		var product_id = $(this).attr('product_id');
		$.ajax({
			url: '/cart/remove/',
			type: 'post',
			data: {product_id: product_id},
			dataType: 'json',
			beforeSend: function () {
				showSpiner();
			},
			success: function (data) {
				hideSpiner();
				addToCart(0, 0);
				if (data['products']) {
					$('#cell_' + product_id).remove();
					for (i = 0; i < data['products'].length; i++) {
						$('#product_price_total_' + data['products'][i]['product_id']).html(data['products'][i]['total']);
					}
					$('#price_total').html(data['total']);
					$('#price_total_full').html(data['total_full']);
					$('#cart-total').html(data['total_cart']);
				} else {
					$('#cart_content').html('Ваша корзина пуста!');
					//$('#cart_clean').html('Ваша корзина пуста!');
					//$('#cart-total').html('В корзине пусто!');
				}
			}
		});
		return false;
	});

	$('.button_message').bind('click', function () {

		var parent_id = '#' + $(this).parents('.a-modal').attr('id');
		$(parent_id + ' .checkout-title').html('');
		$.ajax({
			url: '/message/',
			type: 'post',
			data: $(parent_id + ' input[type=\'text\'], ' + parent_id + ' input[type=\'hidden\'], ' + parent_id + ' textarea'),
			dataType: 'json',
			beforeSend: function () {
				showSpiner();
			},
			success: function (data) {
				hideSpiner();
				if (data['error']) {
					$(parent_id + ' .checkout-title').html('<div class="alert alert-danger" role="alert">' + data['error'] + '</div>');
				}

				if (data['success']) {
					$(parent_id + ' form').css('display', 'none');

					$(parent_id + ' input[type=\'text\']').val('');
					$(parent_id + ' textarea').val('');

					$(parent_id + ' .checkout-title').html('<div class="alert alert-success" role="alert">' + data['success'] + '</div>');
					setTimeout(function () {
						$(parent_id + ' .checkout-title').html('');
						$(parent_id + ' form').css('display', 'block');
						$('.arcticmodal-close').trigger('click');
					}, 2000);

				}
			}
		});
		return false;
	});

	$('#button_notify_stoke').bind('click', function () {

		$('#notify_stoke .checkout-title').html('');
		params = 'product_id=' + $('#product_id').val() + '&fio=' +
		encodeURIComponent($('#notify_stoke input[name=\'fio\']').val()) + '&phone=' +
		encodeURIComponent($('#notify_stoke input[name=\'phone\']').val()) + '&email=' +
		encodeURIComponent($('#notify_stoke input[name=\'email\']').val());

		$.ajax({
			url: '/subscription/',
			type: 'post',
			data: params,
			dataType: 'json',
			beforeSend: function () {
				showSpiner();
			},
			success: function (data) {
				hideSpiner();
				if (data['error']) {
					$('#notify_stoke .checkout-title').html('<div class="alert alert-danger" role="alert">' + data['error'] + '</div>');
				}

				if (data['success']) {
					$('#notify_stoke input[type=\'text\']').val('');

					$('#notify_stoke .checkout-title').html('<div class="alert alert-success" role="alert">' + data['success'] + '</div>');
					setTimeout(function () {
						$('#notify_stoke .checkout-title').html('');
						$('.arcticmodal-close').trigger('click');
					}, 2000);

				}
			}
		});
		return false;
	});

	$('#button-review').click(function () {

		$('#review-title').html('');
		params = 'author=' + encodeURIComponent($('#review input[name=\'author\']').val()) +
		'&email=' + encodeURIComponent($('#review input[name=\'email\']').val()) +
		'&text=' + encodeURIComponent($('#review textarea[name=\'text\']').val()) +
		'&dignity=' + encodeURIComponent($('#review textarea[name=\'dignity\']').val()) +
		'&shortcomings=' + encodeURIComponent($('#review textarea[name=\'shortcomings\']').val()) +
		'&rating=' + encodeURIComponent($('#review input[name=\'rating\']:checked').val() ? $('#review input[name=\'rating\']:checked').val() : 1);

		$.ajax({
			url: '/product/' + $('#product_id').val() + '/review/add/',
			type: 'post',
			dataType: 'json',
			data: params,
			beforeSend: function () {
				showSpiner();
			},
			success: function (data) {
				hideSpiner();
				if (data['error']) {
					$('#review-title').html('<div class="alert alert-danger" role="alert">' + data['error'] + '</div>');
				}

				if (data['success']) {
					$('#review-title').html('<div class="alert alert-success" role="alert">' + data['success'] + '</div>');

					$('input[name=\'name\']').val('');
					$('input[name=\'email\']').val('');
					$('textarea[name=\'text\']').val('');
					$('textarea[name=\'dignity\']').val('');
					$('textarea[name=\'shortcomings\']').val('');
					$('input[name=\'rating\']:checked').attr('checked', '');

					setTimeout(function () {
						$('#review-title').html('');
						$('.arcticmodal-close').trigger('click');
					}, 2000);

				}
			}
		});
		return false;
	});

	$('.button_notify_stoke').bind('click', function () {

		var parent_id = '#' + $(this).parents('.a-modal').attr('id');
		$(parent_id + ' .checkout-title').html('');

		$.ajax({
			url: '/notify/',
			type: 'post',
			data: $(parent_id + ' input[type=\'text\'], ' + parent_id + ' input[type=\'hidden\']'),
			dataType: 'json',
			beforeSend: function () {
				showSpiner();
			},
			success: function (data) {
				hideSpiner();
				if (data['error']) {
					$(parent_id + ' .checkout-title').html('<div class="alert alert-danger" role="alert">' + data['error'] + '</div>');
				}

				if (data['success']) {
					$(parent_id + ' form').css('display', 'none');
					$(parent_id + ' input[type=\'text\']').val('');

					$(parent_id + ' .checkout-title').html('<div class="alert alert-success" role="alert">' + data['success'] + '</div>');

					setTimeout(function () {
						$(parent_id + ' .checkout-title').html('');
						$(parent_id + ' form').css('display', 'block');
						$('.arcticmodal-close').trigger('click');
					}, 2000);

				}
			}
		});

		return false;
	});

	$('.product_add_cart').click(function () {
		if ($(this).hasClass('disabled')) {
			return false;
		}
		product_id = $(this).attr('product_id');
		quantity = 1;
		obj = $('#image_product_' + product_id);

		if ($(this).attr('type') == 'is_big') {
			ob = $('.gallerybox');
			t = parseInt(ob.offset().top + 40);
			l = parseInt(ob.offset().left);
		} else if ($(this).attr('type') == 'is_small') {
			obj = $('#image_small_product_' + product_id);
			t = parseInt($(obj).offset().top);
			l = parseInt($(obj).offset().left);
			if (l == -391) {
				l = 708
			}
			if (l == -611) {
				l = 488;
			}
			if (l == -171) {
				l = 928;
			}
		} else {
			t = parseInt($(obj).offset().top);
			l = parseInt($(obj).offset().left);
		}

		src = obj.attr('src');

		$('<img src="' + src + '" id="temp_cart_animate" style="z-index: 2000; position: absolute; top:' +
			t + 'px; left:' +
			l + 'px;width:' +
			$(obj).attr('width') + ';height:' + $(obj).attr('height') + ' " >'
		).prependTo('body');

		$('#temp_cart_animate').animate({
				top: 70,
				left: $('#container').width() - 50,
				width: 150,
				height: 50
			},
			1000,
			function () {
				$('#temp_cart_animate').remove();
				addToCart(product_id, quantity);
			});

		return false;
	});

	$('.star_non').click(function () {
		$('.star_non').each(function () {
			$(this).removeClass('star_f');
		});
		$('.rating_review').each(function () {
			$(this).prop('checked', false);
		});

		it = $(this).attr('it');
		for (i = 1; i <= it; i++) {
			$('#star' + i).addClass('star_f');
		}

		$('#rating' + i).prop('checked', true);
	});
})
;

addToCart = function (product_id, quantity) {
	quantity = typeof(quantity) != 'undefined' ? quantity : 1;
	$('#link_cart').html('');
	$.ajax({
		url: '/cart/add/',
		type: 'post',
		data: 'product_id=' + product_id + '&quantity=' + quantity,
		dataType: 'json',
		success: function (json) {
			$('.success, .warning, .attention, .information, .error').remove();

			if (json['error']) {
				$('#notification').html('<div class="alert alert-danger" role="alert">' + json['error'] + '</div>');
				$('html, body').animate({scrollTop: 0}, 'slow');
			}

			if (json['success']) {
				$('#notification').html('<div class="alert alert-success" role="alert">' + json['success'] + '</div>');
				if (json['total'] > 0) {
					$('#link_cart').html('<a href="/cart/">' + json['link_cart'] + ' <span class="rub">a</span></a>');
				} else {
					$('#link_cart').html(json['link_cart']);
				}
			}
		}
	});
}

showSpiner = function () {
	$('#window_spinner').css('display', 'block');
}

hideSpiner = function () {
	$('#window_spinner').css('display', 'none');
}
