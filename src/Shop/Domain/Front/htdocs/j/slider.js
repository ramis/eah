$(function(){

	$('.hit-slide').bxSlider({
        auto: true,
	    controls: false
	});

	$('.ulslider').bxSlider({
		auto: true,
		pager: false,
		controls: false
	});

	$('.gallery').bxSlider({
	  pagerCustom: '#gallery-pager',
	  controls: false
	});

	$('.gallery2').bxSlider({
	  pagerCustom: '#gallery-pager2',
      auto: true,
	  controls: true,
	  responsive: true
	});

	$('.relslide').bxSlider({
		slideWidth: 215,
		minSlides: 2,
		maxSlides: 3,
		moveSlides: 1,
		slideMargin: 5
    });

	$('.seeslide').bxSlider({
		slideWidth: 215,
		minSlides: 2,
		maxSlides: 3,
		moveSlides: 1,
		slideMargin: 5
    });

});