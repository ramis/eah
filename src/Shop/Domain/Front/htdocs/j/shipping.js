$(function () {
	if(geo_selected_id > 0){
		selectShipping(geo_selected_id);
	}
	$('#geos').change (function(e){
		selectShipping(e.target[e.target.selectedIndex].value);
	});
	$('#shipping').change (function(e){
		selectShippingGeo(e.target[e.target.selectedIndex].value);
	});
});

selectShipping = function (geo_id){
	$('#shipping').css('display','none').html('');
	$('#shipping_geo').css('display','none').html('');
	$('.deliverysum').css('top', '-5px');
	$('#cost').html(0);
	$('#shipping_description').html('');
	$('#shipping_geo_id').val(0);

	if(shipping[geo_id]) {
		$('#shipping').css('display','inline').html('<option  value="0">Выберите доставку</option>');
		$('.deliverysum').css('top', '35px');
		for (key in shipping[geo_id]){
			s = shipping[geo_id][key];
			if((shipping_selected_id != 0) && (shipping_selected_id == s['shipping_id'])){
				sel = 'selected';
			}else{
				sel = '';
			}
			$("#shipping").append('<option value="' + s['key'] + '" '+sel+'>' + s['title'] + '</option>');
		}
		if(shipping_selected_id > 0){
			selectShippingGeo(shipping_selected_id);
		}
	}
}

selectShippingGeo = function (shipping_id){
	$('#shipping_geo').css('display','none').html('');
	$('.deliverysum').css('top', '35px');
	$('#shipping_description').html('');
	$('#shipping_geo_id').val(0);

	geo_id = $('#geos option:selected').val();
	if(shipping_selected_id > 0){
		shipping_selected_id = 0;
	}else {
		$('#rub').css('display', 'inline');
		$('#cost').html(0);
	}
	if(shipping_geo[geo_id] && shipping_geo[geo_id][shipping_id]) {
		if(shipping_geo[geo_id][shipping_id].length > 1) {
			$('#shipping_geo').html('<option value="0">Выберите</option>').css('display','inline');
			$('.deliverysum').css('top', '75px');
			for (key in shipping_geo[geo_id][shipping_id]){
				s = shipping_geo[geo_id][shipping_id][key];
				if((shipping_geo_selected_id != 0) && (shipping_geo_selected_id == s['shipping_geo_id'])){
					sel = 'selected';
					$('#shipping_geo_id').val(s['shipping_geo_id']);
					$('#rub').css('display', 'inline');
					$('#cost').html(s['price']);
				}else{
					sel = '';
				}
				$("#shipping_geo").append('<option value="' + s['price'] + '" '+sel+' shipping_geo_id="' + s['id']+ '">' + s['description'] + '</option>');
			}

			$('#shipping_geo').change (function(e){
				option = $('#shipping_geo :selected');
				$('#rub').css('display', 'inline');
				$('#cost').html(option.val());
				$('#shipping_geo_id').val(0);
				calculateShipping(option.attr('shipping_geo_id'));
			});

		}else{
			if(shipping_geo[geo_id][shipping_id][0]['price'] > 0){
				$('#rub').css('display', 'inline');
				$('#cost').html(shipping_geo[geo_id][shipping_id][0]['price']);
			}else{
				$('#cost').html('*');
				$('#rub').css('display', 'none');
				var decoded = $('<textarea/>').html(shipping[geo_id][shipping_id]['description']).val();
				$('#shipping_description').html(decoded);
			}
			calculateShipping(shipping_geo[geo_id][shipping_id][0]['id']);
		}
	}
}

calculateShipping = function (shipping_geo_id){
	$.ajax({
		url: '/shipping/',
		type: 'post',
		data: { shipping: shipping_geo_id},
		dataType: 'json',
		beforeSend: function() {
			showSpiner();
		},
		success: function(data) {
			hideSpiner();
			if(data['total_full']){
				$('#price_total_full').html(data['total_full']);
				$('#shipping_geo_id').val(data['shipping_id']);
				$('.no_change_shipping').hide();
			}
		}
	});
}
