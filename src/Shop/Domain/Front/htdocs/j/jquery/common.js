$(function (){

   $('.product_add_cart').click(function(){
        addToCart($(this).attr('product_id'), $(this).attr('quantity'));
       return false;
   });

    $('.quantity_empty_help').hover(
        function() {
			$(this).parent().addClass('hover');
		}
    );

	$('.quantity_empty_content').on('mouseout', function() {
		$(this).parent().removeClass('hover');
	});

	$('.star_non').click(function(){
		id = $(this).attr('it');
		$('#rating' + id).prop("checked",true);

		$('.star_non').removeClass("star_yes");
		for(i=1;i < 6;i++){
			if(i <= id){
				$('#star' + i).addClass("star_yes");
			}
		}
	});

	$('.quantity_empty_content').click(function(){
		$('#product_id').val($(this).attr('product_id'));
		$('#notify_stoke').arcticmodal();
	});

	$('#button_checkout_add').bind('click', function() {
        $.ajax({
            url: '/checkout/',
            type: 'post',
            data: $('#checkout input[type=\'text\'], #checkout textarea'),
            dataType: 'json',
            beforeSend: function() {
            },
            complete: function() {
            },
            success: function(data) {
                if (data['error']) {
                    $('#checkout-ad-title').html('<div class="warning">' + data['error'] + '</div>');
                }

                if (data['success']) {
                    $('#checkout-ad-title').html('<div class="success">' + data['success'] + '</div>');
					var yaParams = {
						order_id: data['order_id'],
						order_price: data['order_price'],
						currency: "RUR",
						exchange_rate: 1,
						goods: data['goods']
					};
					yaCounter26140530.reachGoal('ORDER', yaParams);


					setTimeout(function(){
                        $.ajax({
                            url: '/clear/?is_buy_now=1',
                            type: 'post',
                            data: 'is_key=1',
                            dataType: 'json',
                            success: function(data) {
                                $('.arcticmodal-close').trigger('click');
								$('#checkout-ad-title').html('');
                                window.location.href = window.location.href;
                            }
                        });
                    }, 2000);
                }
            }
        });
        return false;
    });

    $('.button_message').bind('click', function() {

        var parent_id = '#' + $(this).parents('.a-modal').attr('id');
        $(parent_id + ' .checkout-title').html('');
        $.ajax({
            url: '/message/',
            type: 'post',
            data: $(parent_id + ' input[type=\'text\'], ' + parent_id + ' input[type=\'hidden\'], ' + parent_id + ' textarea'),
            dataType: 'json',
            beforeSend: function() {
            },
            complete: function() {
            },
            success: function(data) {
                if (data['error']) {
                    $(parent_id + ' .checkout-title').html('<div class="warning">' + data['error'] + '</div>');
                }

                if (data['success']) {
                    $(parent_id + ' input[type=\'text\']').val('');
                    $(parent_id + ' textarea').val('');

                    $(parent_id + ' .checkout-title').html('<div class="success">' + data['success'] + '</div>');
                    setTimeout(function(){
						$(parent_id + ' .checkout-title').html('');
						$('.arcticmodal-close').trigger('click');
                    }, 2000);

                }
            }
        });
        return false;
    });

    $('#button-review').bind('click', function() {
        $('#review-title').html('');
        params = 'name=' + encodeURIComponent($('#review input[name=\'name\']').val()) +
            '&email=' + encodeURIComponent($('#review input[name=\'email\']').val()) +
            '&text=' + encodeURIComponent($('#review textarea[name=\'text\']').val()) +
            '&dignity=' + encodeURIComponent($('#review textarea[name=\'dignity\']').val()) +
            '&shortcomings=' + encodeURIComponent($('#review textarea[name=\'shortcomings\']').val()) +
            '&rating=' + encodeURIComponent($('#review input[name=\'rating\']:checked').val() ? $('#review input[name=\'rating\']:checked').val() : '');
        $.ajax({
            url: '/product/write/' + product_id + '/',
            type: 'post',
            dataType: 'json',
            data: params,
            beforeSend: function() {
            },
            complete: function() {
            },
            success: function(data) {
                if (data['error']) {
                    $('#review-title').html('<div class="warning">' + data['error'] + '</div>');
                }

                if (data['success']) {
                    $('#review-title').html('<div class="success">' + data['success'] + '</div>');

                    $('input[name=\'name\']').val('');
                    $('input[name=\'email\']').val('');
                    $('textarea[name=\'text\']').val('');
                    $('textarea[name=\'dignity\']').val('');
                    $('textarea[name=\'shortcomings\']').val('');
                    $('input[name=\'rating\']:checked').attr('checked', '');

                    setTimeout(function(){
						$('#review-title').html('');
						$('.arcticmodal-close').trigger('click');
                    }, 2000);

                }
            }
        });
        return false;
    });

    $('.button_notify_stoke').bind('click', function() {

		var parent_id = '#' + $(this).parents('.a-modal').attr('id');
		$(parent_id + ' .checkout-title').html('');

		$.ajax({
            url: '/notify/',
            type: 'post',
            data: $(parent_id + ' input[type=\'text\'], ' + parent_id + ' input[type=\'hidden\']'),
            dataType: 'json',
            beforeSend: function() {
            },
            complete: function() {
            },
            success: function(data) {
                if (data['error']) {
                    $(parent_id + ' .checkout-title').html('<div class="warning">' + data['error'] + '</div>');
                }

                if (data['success']) {
                    $(parent_id + ' input[type=\'text\']').val('');
//                    $(parent_id + ' textarea').val('');

                    $(parent_id + ' .checkout-title').html('<div class="success">' + data['success'] + '</div>');
                    setTimeout(function(){
						$(parent_id + ' .checkout-title').html('');
                        $('.arcticmodal-close').trigger('click');
                    }, 2000);

                }
            }
        });

        return false;
    });


	$('.colorbox1').click(function(){

		url_image = $(this).attr('href');
		image_current = parseInt($(this).attr('it'));
		$('#colorbox1_image').attr('src', url_image);
		$('.popup_window').css('display', 'block');
		$('#colorbox1').css('display', 'block');
		$('body').css('overflow', 'hidden');
		return false;
	});
	$('.colorbox_image_block a').click(function(){

		url_image = $(this).attr('href');
		image_current = parseInt($(this).attr('it'));
		$('#colorbox1_image').attr('src', url_image);

		return false;
	});
	$('.arrow_left_img').click (function(){

		image_current = parseInt(image_current - 1);
		if(image_current < 0){
			image_current = image_count;
		}
		url_image = $('#colorbox1_link_' + image_current).attr('href');
		$('#colorbox1_image').attr('src', url_image);
		return false;
	});
	$('.arrow_right_img').click (function(){
		image_current = parseInt(image_current + 1);
		if(image_current > image_count){
			image_current = 0;
		}
		url_image = $('#colorbox1_link_' + image_current).attr('href');
		$('#colorbox1_image').attr('src', url_image);
		return false;
	});
	$('.review_add_close').live('click', function() {
		$('body').css('overflow', '');
		$('.popup_window').css('display', 'none');
		$(this).parent().css('display', 'none');
	});


	var listRU = $.masksSort($.masksLoad("/catalog/view/theme/spony/js/phones-ru.json"), ['#'], /[0-9]|#/, "mask");
	var optsRU = {
		inputmask: {
			definitions: {
				'#': {
					validator: "[0-9]",
					cardinality: 1
				}
			},
			//clearIncomplete: true,
			showMaskOnHover: false,
			autoUnmask: true
		},
		match: /[0-9]/,
		replace: '#',
		list: listRU,
		listKey: "mask",
		onMaskChange: function(maskObj, determined) {
			if (determined) {
				if (maskObj.type != "mobile") {
					$("#descr").html(maskObj.city.toString() + " (" + maskObj.region.toString() + ")");
				} else {
					$("#descr").html("мобильные");
				}
			} else {
				$("#descr").html("Маска ввода");
			}
			$(this).attr("placeholder", $(this).inputmask("getemptymask"));
		}
	};
	$('#buy_one_click_phone').inputmasks(optsRU);
	$('#customer_phone').inputmasks(optsRU);
	$('#callback_phone').inputmasks(optsRU);

});

function addToCart(product_id, quantity) {
    quantity = typeof(quantity) != 'undefined' ? quantity : 1;

    $.ajax({
        url: '/add/',
        type: 'post',
        data: 'product_id=' + product_id + '&quantity=' + quantity,
        dataType: 'json',
        success: function(json) {
            $('.success, .warning, .attention, .information, .error').remove();

            if (json['redirect']) {
                location = json['redirect'];
            }

            if (json['success']) {
                $('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');

                $('.success').fadeIn('slow');

                $('#cart-total').html(json['total']);

                $('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        }
    });
	return false;
}