<?php

namespace Shop\Domain\Front;

use Slim\Views\Twig;
use Shop\Common;

class Bootstrap extends Common\Bootstrap
{
	/*
	 * @var Facade
	 */
	private $facade;

	/**
	 * Init Slim
	 *
	 * @param string $twig_template_path
	 * @return void
	 */
	public function initSlim($twig_template_path)
	{
		$view = new Twig(array('autoescape' => false));
		$view->setTemplatesDirectory($twig_template_path);

		$this->facade = new Common\Facade($twig_template_path);
		$this->facade->setDomain('Front');
		$this->facade->view($view);
	}

	/**
	 * Routes
	 *
	 * @return void
	 */
	public function initRoutes()
	{
		//Главная страница
		$this->facade
			->addRoute('/', 'main::index')
			->name('main');

		//404
		$this->facade
			->addRoute('/404/', 'main::404')
			->name('404');

		//Страница поиска
		$this->facade
			->addRoute('/search/', 'search::index')
			->name('search');

		//Корзина
		$this->facade
			->addRoute('/cart/', 'cart::index')
			->name('cart');
		$this->facade
			->addRoute('/cart/add/', 'cart::add')
			->name('add')
			->via('POST');
		$this->facade
			->addRoute('/shipping/', 'cart::shipping')
			->name('shipping')
			->via('POST');
		$this->facade
			->addRoute('/cart/remove/', 'cart::remove')
			->name('remove')
			->via('POST');
		$this->facade
			->addRoute('/checkout/', 'cart::checkout')
			->name('checkout')
			->via('POST');

		//Страница продукта
		$this->facade
			->addRoute('/product/:product/', 'catalog.product::index')
			->name('product')
			->conditions(array('product' => "\d+"));
		$this->facade
			->addRoute('/product/:product/review/add/', 'catalog.product::review')
			->name('product_review')
			->via('POST')
			->conditions(array('product' => "\d+"));

		//Сообщение

		//Страница категории или новости
		$this->facade
			->addRoute('/:url/', 'main::url')
			->name('url');

		/**
		 * Ajax
		 */
		$this->facade
			->addRoute('/message/', 'message::message')
			->name('message')
			->via('POST');
		$this->facade
			->addRoute('/subscription/', 'message::subscription')
			->name('subscription')
			->via('POST');
		/**
		 * Api v3toys
		 */
		$this->facade
			->addRoute('/v3toys/', 'v3toys::api')
			->name('api')
			->via('POST');

	}

	/**
	 * Run
	 *
	 * @return void
	 */
	public function run()
	{
		$this->facade->run();
	}

}