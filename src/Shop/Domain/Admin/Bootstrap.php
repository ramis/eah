<?php

namespace Shop\Domain\Admin;

use Shop\Domain\Admin\Helper\CheckAccess;
use Slim\Views\Twig;
use Shop\Common;
use Shop\Domain\Admin\Helper\Auth;

class Bootstrap extends Common\Bootstrap
{
	/*
	 * @var Common\Facade
	 */
	private $facade;

	/**
	 * Init Slim
	 *
	 * @param string $twig_template_path
	 * @return void
	 */
	public function initSlim($twig_template_path)
	{
		$view = new Twig();
		$view->setTemplatesDirectory($twig_template_path);

		$this->facade = new Common\Facade($twig_template_path);

		$facade = $this->facade;

		$this->facade->hook('slim.before.dispatch', function () use ($facade) {

			$routeName = $facade->router()->getCurrentRoute()->getName();
			$auth = Auth::getInstance();

			if ($routeName !== 'login' && $auth->isAuth() === false) {
				$facade->redirect($facade->urlFor('login'), 301);
			}

		});

		$this->facade->setDomain('Admin');
		$this->facade->view($view);

	}

	/**
	 * Routes
	 *
	 * @return void
	 */
	public function initRoutes()
	{
		//Главная страница
		$this->facade
			->addRoute('/', 'main::index')
			->name('main');

		//Логин
		$this->facade
			->addRoute('/login/', 'login::login')
			->name('login')
			->via('POST');
		$this->facade
			->addRoute('/logout/', 'login::logout')
			->name('logout');

		//Картинки
		$this->facade
			->addRoute('/upload/', 'image::upload')
			->name('upload')
			->via('POST');
		$this->facade
			->addRoute('/delete/', 'image::delete')
			->name('delete')
			->via('POST');

		/**
		 * Каталог
		 */
		//Категории
		$this->facade
			->addRoute('/categories/list/', 'catalog.category::list')
			->name('categories_list');
		$this->facade
			->addRoute('/category/create/', 'catalog.category::save')
			->name('category_create')
			->via('POST');
		$this->facade
			->addRoute('/category/:category/edit/', 'catalog.category::save')
			->name('category_edit')
			->conditions(array('category' => "\d+"))
			->via('POST');
		$this->facade
			->addRoute('/category/:category/delete/', 'catalog.category::delete')
			->name('category_delete')
			->conditions(array('category' => "\d+"));

		//Продукты
		$this->facade
			->addRoute('/product/list/', 'catalog.product::list')
			->name('product_list');
		$this->facade
			->addRoute('/product/list/ajax/', 'catalog.product::listJson')
			->name('product_list_ajax');
		$this->facade
			->addRoute('/product/create/', 'catalog.product::save')
			->name('product_create')
			->via('POST');
		$this->facade
			->addRoute('/product/:product/edit/', 'catalog.product::save')
			->name('product_edit')
			->conditions(array('product' => "\d+"))
			->via('POST');
		$this->facade
			->addRoute('/get/product/', 'catalog.product::get')
			->name('product_get')
			->via('POST');
		$this->facade
			->addRoute('/product/:product/delete/', 'catalog.product::delete')
			->name('product_delete')
			->conditions(array('product' => "\d+"));

		//Аттрибуты
		$this->facade
			->addRoute('/attribute/list/', 'catalog.product.attribute::list')
			->name('attribute_list');
		$this->facade
			->addRoute('/attribute/create/', 'catalog.product.attribute::save')
			->name('attribute_create')
			->via('POST');
		$this->facade
			->addRoute('/attribute/:attribute/edit/', 'catalog.product.attribute::save')
			->name('attribute_edit')
			->conditions(array('attribute' => "\d+"))
			->via('POST');
		$this->facade
			->addRoute('/attribute/:attribute/delete/', 'catalog.product.attribute::delete')
			->name('attribute_delete')
			->conditions(array('attribute' => "\d+"));

		//Фильтра
		$this->facade
			->addRoute('/filter/list/', 'catalog.product.filter::list')
			->name('filter_list');

		//Отзывы
		$this->facade
			->addRoute('/review/list/', 'catalog.product.review::list')
			->name('review_list');
		$this->facade
			->addRoute('/review/:review/edit/', 'catalog.product.review::edit')
			->name('review_edit')
			->conditions(array('review' => "\d+"))
			->via('POST');
		$this->facade
			->addRoute('/review/:review/delete/', 'catalog.product.review::delete')
			->name('review_delete')
			->conditions(array('review' => "\d+"));

		//Статьи
		$this->facade
			->addRoute('/information/list/', 'information::list')
			->name('information_list');
		$this->facade
			->addRoute('/information/create/', 'information::save')
			->name('information_create')
			->via('POST');
		$this->facade
			->addRoute('/information/:information/edit/', 'information::save')
			->name('information_edit')
			->conditions(array('information' => "\d+"))
			->via('POST');
		$this->facade
			->addRoute('/information/:information/delete/', 'information::delete')
			->name('information_delete')
			->conditions(array('information' => "\d+"));

		//Заказы
		$this->facade
			->addRoute('/order/list/', 'order::list')
			->name('order_list');
		$this->facade
			->addRoute('/order/:order/edit/', 'order::save')
			->name('order_edit')
			->conditions(array('order' => "\d+"))
			->via('POST');
		$this->facade
			->addRoute('/order/list/ajax/', 'order::ajax')
			->name('order_list_ajas')
			->via('POST');
		$this->facade
			->addRoute('/order/history/add/', 'order::historyAdd')
			->name('history_add')
			->via('POST');
		$this->facade
			->addRoute('/order/:order/delete/', 'order::delete')
			->name('order_delete')
			->conditions(array('order' => "\d+"));

		//Заявки
		$this->facade
			->addRoute('/message/list/', 'message::list')
			->name('message_list');
		$this->facade
			->addRoute('/message/:message/edit/', 'message::edit')
			->name('message_edit')
			->conditions(array('message' => "\d+"))
			->via('POST');
		$this->facade
			->addRoute('/message/:message/delete/', 'message::delete')
			->name('message_delete')
			->conditions(array('message' => "\d+"));

		//Подписки
		$this->facade
			->addRoute('/subscription/list/', 'subscription::list')
			->name('subscription_list');
		$this->facade
			->addRoute('/subscription/:subscription/delete/', 'subscription::delete')
			->name('subscription_delete')
			->conditions(array('subscription' => "\d+"));

		//Баннеры
		$this->facade
			->addRoute('/banners/list/', 'banners::list')
			->name('banner_list');
		$this->facade
			->addRoute('/banners/create/', 'banners::save')
			->name('banner_create')
			->via('POST');
		$this->facade
			->addRoute('/banners/:banner/edit/', 'banners::save')
			->name('banner_edit')
			->conditions(array('banner' => "\d+"))
			->via('POST');
		$this->facade
			->addRoute('/banners/:banner/delete/', 'banners::delete')
			->name('banner_delete')
			->conditions(array('banner' => "\d+"));

		//гео
		$this->facade
			->addRoute('/geo/list/', 'geo::list')
			->name('geo_list');
		$this->facade
			->addRoute('/geo/create/', 'geo::save')
			->name('geo_create')
			->via('POST');
		$this->facade
			->addRoute('/geo/:geo/edit/', 'geo::save')
			->name('geo_edit')
			->conditions(array('geo' => "\d+"))
			->via('POST');
		$this->facade
			->addRoute('/geo/:geo/delete/', 'geo::delete')
			->name('geo_delete')
			->conditions(array('geo' => "\d+"));

		//Доставки
		$this->facade
			->addRoute('/shipping/list/', 'shipping::list')
			->name('shipping_list');
		$this->facade
			->addRoute('/shipping/create/', 'shipping::save')
			->name('shipping_create')
			->via('POST');
		$this->facade
			->addRoute('/shipping/geo/add/', 'shipping::geoAdd')
			->name('shipping_geo_add')
			->via('POST');
		$this->facade
			->addRoute('/shipping/:shipping/edit/', 'shipping::save')
			->name('shipping_edit')
			->conditions(array('shipping' => "\d+"))
			->via('POST');
		$this->facade
			->addRoute('/shipping/:shipping/delete/', 'shipping::delete')
			->name('shipping_delete')
			->conditions(array('shipping' => "\d+"));

		//Советы
		$this->facade
			->addRoute('/tips/list/', 'tips::list')
			->name('tips_list');
		$this->facade
			->addRoute('/tips/create/', 'tips::save')
			->name('tips_create')
			->via('POST');
		$this->facade
			->addRoute('/tips/:tips/edit/', 'tips::save')
			->name('tips_edit')
			->conditions(array('tips' => "\d+"))
			->via('POST');
		$this->facade
			->addRoute('/tips/:tips/delete/', 'tips::delete')
			->name('tips_delete')
			->conditions(array('tips' => "\d+"));

		//Пользователи
		$this->facade
			->addRoute('/user/list/', 'user::list')
			->name('user_list');
		$this->facade
			->addRoute('/user/create/', 'user::save')
			->name('user_create')
			->via('POST');
		$this->facade
			->addRoute('/user/:user/edit/', 'user::save')
			->name('user_edit')
			->conditions(array('user' => "\d+"))
			->via('POST');
		$this->facade
			->addRoute('/user/:tips/delete/', 'user::delete')
			->name('user_delete')
			->conditions(array('user' => "\d+"));

		//логи выполнения крон скриптов
		$this->facade
			->addRoute('/logs/list/', 'logs::list')
			->name('logs_list');
		$this->facade
			->addRoute('/logs/:logs/view/', 'logs::view')
			->name('logs_view')
			->conditions(array('logs' => "\d+"));
		$this->facade
			->addRoute('/logs/:logs/delete/', 'logs::delete')
			->name('logs_delete')
			->conditions(array('logs' => "\d+"));

		//формы письма пользователям
		$this->facade
			->addRoute('/template/list/', 'template::list')
			->name('template_list');
		$this->facade
			->addRoute('/template/create/', 'template::save')
			->name('template_create')
			->via('POST');
		$this->facade
			->addRoute('/template/:template/edit/', 'template::save')
			->name('template_edit')
			->conditions(array('template' => "\d+"))
			->via('POST');
		$this->facade
			->addRoute('/setting/:tips/delete/', 'template::delete')
			->name('template_delete')
			->conditions(array('template' => "\d+"));

		//настройки системы
		$this->facade
			->addRoute('/setting/', 'setting::setting')
			->via('POST')
			->name('setting');
	}

	/**
	 * Run
	 *
	 * @return void
	 */
	public function run()
	{
		$this->facade->run();
	}

}