<?php
namespace Shop\Domain\Admin\Controller;

use ActiveRecord\RecordNotFound;
use Shop\Common\Facade;
use Shop\Filter\Catalog\Product;
use Shop\Filter\Tips as FilterTips;
use Shop\Model\Tips as ModelTips;

class Tips extends AbstractController implements Menu
{
	/**
	 * @var array
	 */
	private $data = array();

	public function __construct(Facade $app)
	{
		$this->app = $app;

		if(parent::checkUserGroupRoles(__CLASS__) === false){
			$this->app->redirect($this->app->urlFor('main'), 301);
		}

		$this->getMenu($this->data, 'tips');

		$this->data['breadcrumb'] = array(
			array(
				'title' => 'Главная',
				'url' => $this->app->urlFor('main'),
			),
			array(
				'title' => 'Советы',
				'url' => $this->app->urlFor('tips_list'),
			),
		);
	}

	/**
	 * Элемент меню
	 *
	 * @param Facade $app
	 * @param string $class
	 * @return array
	 */
	public static function getElemMenu (Facade $app, $class)
	{
		return parent::checkUserGroupRoles($class) ? array(AbstractController::MENU_NEWS_ID ,array(
			'url' => $app->urlFor('tips_list'),
			'title' => 'Советы',
			'selected' => '',
		)) : array(false, false);
	}

	/**
	 * Советы список
	 */
	public function actionList()
	{
		$this->data['create'] = $this->app->urlFor('tips_create');

		$this->data['tips'] = array();
		$tips = ModelTips::fetch(new FilterTips());
		foreach ($tips as $t) {
			$this->data['tips'][] = array(
				'id' => $t->id,
				'url' => $t->url,
				'description' => $t->description,
				'product' => $t->getProduct()->title,
				'url_edit' => $this->app->urlFor('tips_edit', array('tips' => $t->id)),
				'url_del' => $this->app->urlFor('tips_delete', array('tips' => $t->id)),
			);
		}
		$this->data['title'] = 'Cоветы';
		$this->app->render('tips/list.twig', $this->data);
	}

	/**
	 * Совет
	 *
	 * @param int $tips_id
	 */
	public function actionSave($tips_id = 0)
	{
		$tips = ModelTips::fetchById($tips_id);
		if ($tips === null) {
			$tips = new ModelTips();
		}

		if ($this->app->request->isPost()) {

			$tips->url = (string)$this->app->request->post('url');
			$tips->description = (string)$this->app->request->post('description');
			$tips->product_id = $this->app->request->post('product_id');

			if ($tips->save()) {
				//Редирект
				$this->app->redirect($this->app->urlFor('tips_list'), 301);
			} else {
				//Какая то ошибка
			}
		}

		$this->data['tips'] = array(
			'id' => $tips->id,
			'url' => $tips->url,
			'description' => $tips->description,
			'product_id' => $tips->product_id,
		);

		$products = \Shop\Model\Product::fetch(new Product());
		foreach ($products as $p) {
			$this->data['products'][] = array(
				'id' => $p->id,
				'title' => $p->title,
				'select' => ($p->id === $tips->product_id ? 'select' : "")
			);
		}
		$this->data['breadcrumb'][] = array(
			'title' => ($tips->id ? 'Изменить' : 'Создать'),
			'url' => '',
		);

		$this->data['title'] = ($tips->id ? 'Изменить' : 'Создать') . ' совет';
		$this->app->render('tips/save.twig', $this->data);
	}

	/**
	 * @param int $tips_id
	 */
	public function actionDelete($tips_id)
	{
		try {
			$tips = ModelTips::fetchById($tips_id);
			$tips->delete();
		} catch (RecordNotFound $r) {
		}

		$this->app->redirect($this->app->urlFor('tips_list'), 301);
	}

}