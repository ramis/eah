<?php
namespace Shop\Domain\Admin\Controller;

use ActiveRecord\RecordNotFound;
use Shop\Common\Facade;
use Shop\Filter\Order as FilterOrder;
use Shop\Model\Order as ModelOrder;
use Shop\Model\Catalog\Product as ModelProduct;
use Shop\Filter\Catalog\Product as FilterProduct;

class Order extends AbstractController implements Menu
{
	/**
	 * @var array
	 */
	private $data = array();

	/**
	 * @param Facade $app
	 */
	public function __construct(Facade $app)
	{
		$this->app = $app;

		if(parent::checkUserGroupRoles(__CLASS__) === false){
			$this->app->redirect($this->app->urlFor('main'), 301);
		}

		$this->getMenu($this->data, 'order');

		$this->data['breadcrumb'] = array(
			array(
				'title' => 'Главная',
				'url' => $this->app->urlFor('main'),
			),
			array(
				'title' => 'Заказы',
				'url' => $this->app->urlFor('order_list'),
			),
		);
		$this->data['title'] = 'Заказы';
	}

	/**
	 * Элемент меню
	 *
	 * @param Facade $app
	 * @param string $class
	 * @return array
	 */
	public static function getElemMenu (Facade $app, $class)
	{
		return parent::checkUserGroupRoles($class) ? array(AbstractController::MENU_SHOP_ID ,array(
			'url' => $app->urlFor('order_list'),
			'title' => 'Заказы',
			'selected' => '',
		)) : array(false, false);
	}

	/**
	 * List order
	 */
	public function actionList()
	{
		$this->app->render('order/list.twig', $this->data);
	}

	/**
	 * Return order by filter Ajax
	 */
	public function actionAjax()
	{
		$this->data['orders'] = array();

		$filter = new FilterOrder();
		//По номеру
		if ($this->app->request->get('id')) {
			$filter->id($this->app->request->get('id'));
		}
		//По цене
		if ($this->app->request->get('price')) {
			$filter->price($this->app->request->get('price'));
		}
		//По титлу
		if ($this->app->request->get('fio')) {
			$filter->fio($this->app->request->get('fio'));
		}

		$this->data['products'] = array();
		$count = ModelOrder::fetchCount($filter);

		$this->data['pages'] = ceil($count / 10);

		//Страница
		$page = $this->app->request->get('page') ? (int)$this->app->request->get('page') : 1;
		$filter->limit(10, $page);
		$this->data['page'] = $page;

		//Сортировка
		if ($this->app->request->get('sort')) {
			$filter->sort($this->app->request->get('sort'));
		}
		$orders = ModelOrder::fetch($filter);

		foreach ($orders as $o) {
			$this->data['orders'][] = array(
				'id' => $o->id,
				'fio' => $o->fio,
				'status' => $o->getStatus()->title,
				'total' => $o->total,
				'create_time' => $o->create_time,
				'update_time' => $o->update_time,
				'url_edit' => $this->app->urlFor('order_edit', array('order' => $o->id)),
				'url_del' => $this->app->urlFor('order_delete', array('order' => $o->id)),
			);
		}

		$orders = $this->app->view->fetch('order/list_json.twig', $this->data);
		$nav = $this->app->view->fetch('order/nav_json.twig', $this->data);
		print json_encode(array('elements' => $orders, 'nav' => $nav));
	}

	/**
	 * Add history order
	 *
	 * @throws RecordNotFound
	 */
	public function actionHistoryAdd()
	{
		$json = array();
		if ($this->app->request->isPost()) {

			$history = new ModelOrder\History();
			$history->order_status_id = (int)$this->app->request->post('status');
			$history->order_id = (int)$this->app->request->post('order');
			$history->comment = $this->app->request->post('comment');
			$history->create_time = date('Y-m-d H:i:s');

			if ($history->save()) {
				$order = \Shop\Model\Order::find($history->order_id);
				$order->order_status_id = $history->order_status_id;
				$order->save();
				$json = array(
					'comment' => $history->comment,
					'status' => $history->getOrderStatus()->title,
					'date' => date('d.m.Y'),
				);
			}
		}
		print json_encode($json);
	}

	/**
	 * Save
	 */
	public function actionSave($order_id)
	{
		$order = ModelOrder::fetchById($order_id);
		if ($order === null) {
			$this->app->redirect($this->app->urlFor('order_list'), 301);
		}

		if ($this->app->request->isPost()) {

			$order->fio = (string)$this->app->request->post('fio');
			$order->telephone = (string)$this->app->request->post('telephone');
			$order->email = (string)$this->app->request->post('email');
			$order->address = (string)$this->app->request->post('address');
			$order->comment = (string)$this->app->request->post('comment');

			$order->update_time = (string)date('Y-m-d H:i:s');

			if ($order->save()) {
				$this->app->redirect($this->app->urlFor('order_list'), 301);
			} else {
				//Какая то ошибка
				$this->data['errors'] = array();
				foreach ($order->errors as $k => $v) {
					$this->data['errors'][] = $v;
				}
			}
		}
		$this->data['order'] = array(
			'id' => $order->id,
			'fio' => $order->fio,
			'telephone' => $order->telephone,
			'email' => $order->email,
			'address' => $order->address,
			'comment' => $order->comment,
			'products' => array(),
			'history' => array(),
		);

		if ($order->id) {
			$filter = new FilterOrder\Product();
			$filter->order($order);
			$products = ModelOrder\Product::fetch($filter);
			foreach ($products as $p) {
				$this->data['order']['products'][] = array(
					'id' => $p->id,
					'title' => $p->catalog_product_title,
					'article' => $p->catalog_product_article,
					'code' => $p->catalog_product_code,
					'quantity' => $p->quantity,
					'price' => $p->price,
					'total' => $p->total,
				);
			}
			$filter = new FilterOrder\History();
			$filter->order($order);
			$history = ModelOrder\History::fetch($filter);
			foreach ($history as $h) {
				$this->data['order']['history'][] = array(
					'id' => $h->id,
					'create_time' => $h->create_time,
					'comment' => $h->comment,
					'status' => $h->getOrderStatus()->title,
				);
			}
		}

		$status = ModelOrder\Status::fetch(new FilterOrder\Status());
		foreach ($status as $s) {
			$this->data['status'][] = array(
				'id' => $s->id,
				'title' => $s->title,
			);
		}
		$products = ModelProduct::fetch(new FilterProduct());
		foreach ($products as $p) {
			$this->data['products'][] = array(
				'id' => $p->id,
				'title' => $p->title,
			);
		}

		$this->data['breadcrumb'][] = array(
			'title' => ($order->id ? 'Изменить заказ' : 'Добавить заказ'),
			'url' => '',
		);

		$this->app->render('order/save.twig', $this->data);
	}

	/**
	 * Delete order
	 *
	 * @param int $order_id
	 */
	public function actionDelete($order_id)
	{
		try {
			$order = ModelOrder::fetchById($order_id);
			$order->delete();
		} catch (RecordNotFound $r) {
		}
		$this->app->redirect($this->app->urlFor('order_list'), 301);
	}

}