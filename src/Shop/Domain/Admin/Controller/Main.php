<?php
namespace Shop\Domain\Admin\Controller;

use Shop\Common\Facade;

class Main extends AbstractController
{

	/**
	 * @var array
	 */
	private $data = array();

	public function __construct(Facade $app)
	{
		$this->app = $app;

		$this->getMenu($this->data, 'category');

		$this->data['breadcrumb'] = array(
			array(
				'title' => 'Главная',
				'url' => $this->app->urlFor('main'),
			)
		);
		$this->data['title'] = 'Главная';
	}

	/**
	 *
	 */
	public function actionIndex()
	{
		$this->app->render('index.twig', $this->data);
	}

}