<?php
namespace Shop\Domain\Admin\Controller;

use Shop\Common\Facade;
use Shop\Filter\Setting as FilterSetting;
use Shop\Model\Setting as ModelSetting;

class Setting extends AbstractController implements Menu
{
	/**
	 * @var array
	 */
	private $data = array();

	public function __construct(Facade $app)
	{
		$this->app = $app;

		if(parent::checkUserGroupRoles(__CLASS__) === false){
			$this->app->redirect($this->app->urlFor('main'), 301);
		}

		$this->getMenu($this->data, 'setting');

		$this->data['breadcrumb'] = array(
			array(
				'title' => 'Главная',
				'url' => $this->app->urlFor('main'),
			),
			array(
				'title' => 'Настройки',
				'url' => $this->app->urlFor('setting'),
			),
		);
	}

	/**
	 * Элемент меню
	 *
	 * @param Facade $app
	 * @param string $class
	 * @return array
	 */
	public static function getElemMenu (Facade $app, $class)
	{
		return parent::checkUserGroupRoles($class) ? array(AbstractController::MENU_SETTING_ID ,array(
			'url' => $app->urlFor('setting'),
			'title' => 'Настройки',
			'selected' => '',
		)) : array(false, false);
	}

	/**
	 * Сохранить настройки
	 */
	public function actionSetting()
	{
		$this->data['title'] = 'Настройки';

		if ($this->app->request->isPost()) {
			foreach($_POST as $key=>$val){
				$setting = ModelSetting::find_by_key($key);
				if($setting === null){
					$setting = new ModelSetting();
				}
				$setting->key = $key;
				$setting->value = $val;
				$setting->save();
			}
			$this->app->redirect($this->app->urlFor('setting'), 301);
		}

		$this->data['setting'] = array();
		$setting = ModelSetting::fetch(new FilterSetting());
		foreach ($setting as $s) {
			$this->data[$s->key] = $s->value;
		}

		$this->app->render('setting/setting.twig', $this->data);
	}

}