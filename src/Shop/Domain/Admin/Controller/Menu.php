<?php
namespace Shop\Domain\Admin\Controller;

use Shop\Common\Facade;

interface Menu
{
	/**
	 * Элемент меню
	 *
	 * @param Facade $app
	 * @param string $class
	 */
	public static function getElemMenu(Facade $app, $class);

}