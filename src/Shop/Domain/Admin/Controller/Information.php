<?php
namespace Shop\Domain\Admin\Controller;

use ActiveRecord\RecordNotFound;
use Shop\Common\Facade;
use Shop\Filter\Information as FilterInformation;
use Shop\Model\Information as ModelInformation;

class Information extends AbstractController implements Menu
{
	/**
	 * @var array
	 */
	private $data = array();

	public function __construct(Facade $app)
	{
		$this->app = $app;

		if(parent::checkUserGroupRoles(__CLASS__) === false){
			$this->app->redirect($this->app->urlFor('main'), 301);
		}

		$this->getMenu($this->data, 'information');

		$this->data['breadcrumb'] = array(
			array(
				'title' => 'Главная',
				'url' => $this->app->urlFor('main'),
			),
			array(
				'title' => 'Статьи',
				'url' => $this->app->urlFor('information_list'),
			),
		);
		$this->data['title'] = 'Статьи';
	}

	/**
	 * Элемент меню
	 *
	 * @param Facade $app
	 * @param string $class
	 * @return array
	 */
	public static function getElemMenu (Facade $app, $class)
	{
		return parent::checkUserGroupRoles($class) ? array(AbstractController::MENU_NEWS_ID ,array(
			'url' => $app->urlFor('information_list'),
			'title' => 'Статьи',
			'selected' => '',
		)) : array(false, false);
	}

	/**
	 * List catalog information
	 */
	public function actionList()
	{
		$this->data['create'] = $this->app->urlFor('information_create');
		$filter = new FilterInformation();
		$informations = ModelInformation::fetch($filter);

		foreach ($informations as $i) {
			$this->data['information'][] = array(
				'id' => $i->id,
				'title' => $i->title,
				'url' => $i->url,
				'url_edit' => $this->app->urlFor('information_edit', array('information' => $i->id)),
				'url_del' => $this->app->urlFor('information_delete', array('information' => $i->id)),
			);
		}
		$this->app->render('information/list.twig', $this->data);
	}

	/**
	 * Save catalog information
	 */
	public function actionSave($information_id = 0)
	{
		$information = ModelInformation::fetchById($information_id);
		if($information === null){
			$information = new ModelInformation();
		}

		if ($this->app->request->isPost()) {

			$information->title = (string)$this->app->request->post('title');
			$information->url = (string)$this->app->request->post('url');
			$information->description = (string)$this->app->request->post('description');
			$information->meta_description = (string)$this->app->request->post('meta_description');
			$information->meta_keyword = (string)$this->app->request->post('meta_keyword');
			$information->seo_title = (string)$this->app->request->post('seo_title');
			$information->seo_h1 = (string)$this->app->request->post('seo_h1');

			$information->update_time = (string)date('Y-m-d H:i:s');
			if ($information->id === NULL) {
				$information->create_time = (string)date('Y-m-d H:i:s');
			}

			if ($information->save()) {
				//Редирект
				$this->app->redirect($this->app->urlFor('information_list'), 301);
			} else {
				//Какая то ошибка
				$this->data['errors'] = array();
				foreach ($information->errors as $k => $v) {
					$this->data['errors'][] = $v;
				}
			}
		}

		$this->data['information'] = array(
			'title' => $information->title,
			'url' => $information->url,
			'description' => $information->description,
			'meta_description' => $information->meta_description,
			'meta_keyword' => $information->meta_keyword,
			'seo_title' => $information->seo_title,
			'seo_h1' => $information->seo_h1,
		);

		$this->data['breadcrumb'][] = array(
			'title' => ($information->id ? 'Изменить' : 'Создать'),
			'url' => '',
		);

		$this->app->render('information/save.twig', $this->data);
	}

	/**
	 * Delete catalog information
	 *
	 * @param int $information_id
	 */
	public function actionDelete($information_id)
	{
		try {
			$information = ModelInformation::fetchById($information_id);
			$information->delete();
		} catch (RecordNotFound $r) {
		}

		$this->app->redirect($this->app->urlFor('information_list'), 301);
	}

}