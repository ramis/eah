<?php
namespace Shop\Domain\Admin\Controller;

use ActiveRecord\RecordNotFound;
use Shop\Common\Facade;
use Shop\Filter\Geo as FilterGeo;
use Shop\Model\Geo as ModelGeo;
use Shop\Filter\Shipping as FilterShipping;
use Shop\Model\Shipping as ModelShipping;
use Shop\Filter\Shipping\Geo\Value as FilterValue;
use Shop\Model\Shipping\Geo\Value as ModelValue;

class Shipping extends AbstractController implements Menu
{
	/**
	 * @var array
	 */
	private $data = array();

	public function __construct(Facade $app)
	{
		$this->app = $app;

		if(parent::checkUserGroupRoles(__CLASS__) === false){
			$this->app->redirect($this->app->urlFor('main'), 301);
		}

		$this->getMenu($this->data, 'shipping');

		$this->data['breadcrumb'] = array(
			array(
				'title' => 'Главная',
				'url' => $this->app->urlFor('main'),
			),
			array(
				'title' => 'Доставки',
				'url' => $this->app->urlFor('shipping_list'),
			),
		);
		$this->data['title'] = 'Доставки';
	}

	/**
	 * Элемент меню
	 *
	 * @param Facade $app
	 * @param string $class
	 * @return array
	 */
	public static function getElemMenu (Facade $app, $class)
	{
		return parent::checkUserGroupRoles($class) ? array(AbstractController::MENU_SHIPPING_ID ,array(
			'url' => $app->urlFor('shipping_list'),
			'title' => 'Доставки',
			'selected' => '',
		)) : array(false, false);
	}

	/**
	 * List shipping
	 */
	public function actionList()
	{
		$this->data['create'] = $this->app->urlFor('shipping_create');
		$filter = new FilterShipping();
		$filter->sort('sort asc');
		$shippings = ModelShipping::fetch($filter);
		foreach ($shippings as $s) {
			$this->data['shipping'][] = array(
				'id' => $s->id,
				'title' => $s->title,
				'sort' => $s->sort,
				'url_edit' => $this->app->urlFor('shipping_edit', array('shipping' => $s->id)),
				'url_del' => $this->app->urlFor('shipping_delete', array('shipping' => $s->id)),
			);
		}
		$this->app->render('shipping/list.twig', $this->data);
	}

	/**
	 * Save shipping
	 */
	public function actionSave($shipping_id = 0)
	{
		$shipping = ModelShipping::fetchById($shipping_id);

		if ($shipping === null) {
			$shipping = new ModelShipping();
		}

		if ($this->app->request->isPost()) {

			$shipping->title = (string)$this->app->request->post('title');
			$shipping->sort = (int)$this->app->request->post('sort');
			$shipping->is_publish = (int)$this->app->request->post('is_publish');

			if ($shipping->save()) {
				$value_id = (array)$this->app->request->post('value_id');
				$value_geo = (array)$this->app->request->post('value_geo');
				$value_price = (array)$this->app->request->post('value_price');
				$value_description = (array)$this->app->request->post('value_description');
				$value_point_id = (array)$this->app->request->post('value_point_id');

				$filter = new FilterValue();
				$filter
					->shipping($shipping);
				foreach($value_geo as $key=>$geo_id){
					$geo = ModelGeo::fetchById((int)$geo_id);
					if($geo instanceof ModelGeo){

						$value = ModelValue::fetchById((int)$value_id[$key]);

						if($value instanceof ModelValue){
							$value->price = (int)$value_price[$key];
							$value->description = $value_description[$key];
							$value->point_id = (int)$value_point_id[$key];
							$value->save();
						}
					}
				}
				//Редирект
				$this->app->redirect($this->app->urlFor('shipping_list'), 301);
			} else {
				//Какая то ошибка
				$this->data['errors'] = array();
				foreach ($shipping->errors as $k => $v) {
					$this->data['errors'][] = $v;
				}
			}
		}

		$this->data['shipping'] = array(
			'id' => $shipping->id,
			'title' => $shipping->title,
			'sort' => $shipping->sort,
			'is_publish' => $shipping->is_publish,
		);

		$filter = new FilterValue();
		$filter
			->shipping($shipping)
			->sort('geo_id asc');
		$values = ModelValue::fetch($filter);
		foreach ($values as $v) {
			$this->data['values'][] = array(
				'id' => $v->id,
				'geo_id' => $v->getGeo()->id,
				'geo_title' => $v->getGeo()->title,
				'price' => $v->price,
				'description' => $v->description,
				'point_id' => $v->point_id,
			);
		}

		$filter = new FilterGeo();
		$filter
			->sort('sort asc');
		$geos = ModelGeo::fetch($filter);
		foreach ($geos as $g) {
			$this->data['geos'][] = array(
				'id' => $g->id,
				'title' => $g->title,
			);
		}

		$this->data['breadcrumb'][] = array(
			'title' => ($shipping->id ? 'Изменить' : 'Создать'),
			'url' => '',
		);

		$this->app->render('shipping/save.twig', $this->data);
	}

	/**
	 * Delete catalog shipping
	 *
	 * @param int $shipping_id
	 */
	public function actionDelete($shipping_id)
	{
		try {
			$shipping = ModelShipping::fetchById($shipping_id);
			$shipping->delete();
		} catch (RecordNotFound $r) {
		}
		$this->app->redirect($this->app->urlFor('shipping_list'), 301);
	}

	/**
	 * Add Shipping_Geo_Value
	 *
	 * @throws RecordNotFound
	 */
	public function actionGeoAdd()
	{
		$json = array();
		if ($this->app->request->isPost()) {

			$value = new ModelValue();

			$value->shipping_id = (int)$this->app->request->post('shipping_id');
			$value->geo_id = (int)$this->app->request->post('geo_id');
			$value->price = (int)$this->app->request->post('price');
			$value->description = (string)$this->app->request->post('description', '');
			$value->point_id = (int)$this->app->request->post('point_id', '');

			if ($value->save()) {
				$json = array(
					'id' => $value->id,
					'price' => $value->price,
					'description' => $value->description,
					'point_id' => $value->point_id,
					'geo_id' => $value->getGeo()->id,
					'geo_title' => $value->getGeo()->title,
				);
			}
		}
		print json_encode($json);
	}
}