<?php
namespace Shop\Domain\Admin\Controller;

use ActiveRecord\RecordNotFound;
use Shop\Common\Facade;
use Shop\Filter\Banners as FilterBanners;
use Shop\Model\Banners as ModelBanners;
use Shop\Model\Image;

class Banners extends AbstractController implements Menu
{
	/**
	 * @var array
	 */
	private $data = array();

	public function __construct(Facade $app)
	{
		$this->app = $app;

		if(parent::checkUserGroupRoles(__CLASS__) === false){
			$this->app->redirect($this->app->urlFor('main'), 301);
		}

		$this->getMenu($this->data);

		$this->data['breadcrumb'] = array(
			array(
				'title' => 'Главная',
				'url' => $this->app->urlFor('main'),
			),
			array(
				'title' => 'Баннеры',
				'url' => $this->app->urlFor('banner_list'),
			),
		);
		$this->data['title'] = "Баннеры";
	}

	/**
	 * Элемент меню
	 *
	 * @param Facade $app
	 * @param string $class
	 * @return array
	 */
	public static function getElemMenu (Facade $app, $class)
	{
		return parent::checkUserGroupRoles($class) ? array(AbstractController::MENU_SHOP_ID ,array(
			'url' => $app->urlFor('banner_list'),
			'title' => 'Баннеры',
			'selected' => '',
		)) : array(false, false);
	}

	/**
	 * Список баннеров
	 */
	public function actionList()
	{
		$this->data['create'] = $this->app->urlFor('banner_create');
		$this->data['title'] = "Баннеры";

		$this->data['banners'] = array();
		$filter = new FilterBanners();
		$filter->sort('type');

		$banners = ModelBanners::fetch($filter);
		foreach ($banners as $b) {
			$image = '';
			if($i = $b->getImage()){
				$image = HTTP_IMAGE_BASE . $i->hash;
			}
			$this->data['banners'][] = array(
				'id' => $b->id,
				'title' => $b->title,
				'url' => $b->url,
				'image' => $image,
				'type' => $b->getTypeTitle(),
				'url_edit' => $this->app->urlFor('banner_edit', array('banner' => $b->id)),
				'url_del' => $this->app->urlFor('banner_delete', array('banner' => $b->id)),
			);
		}

		$this->app->render('banners/list.twig', $this->data);
	}

	/**
	 * Save banner
	 *
	 * @param int $banners_id
	 */
	public function actionSave($banners_id = 0)
	{
		$banner = ModelBanners::fetchById($banners_id);
		if($banner === null){
			$banner = new ModelBanners();
		}

		if ($this->app->request->isPost()) {

			$banner->title = (string)$this->app->request->post('title');
			$banner->url = (string)$this->app->request->post('url');
			$banner->image_id = (int)$this->app->request->post('image_id');
			$banner->type = (int)$this->app->request->post('type');
			$is_new_banner = ($banner->id === NULL && $banner->image_id);
			if($banner->save()){
				if($is_new_banner){
					$image = Image::find($banner->image_id);
					$image->parent_id = (int)$banner->id;
					$image->change();
				}
				//Редирект
				$this->app->redirect($this->app->urlFor('banner_list'), 301);
			}else{
				//Какая то ошибка
				$this->data['errors'] = array();
				foreach($banner->errors as $k=>$v){
					$this->data['errors'][] = $v;
				}
			}
		}

		$image = array();
		if($i = $banner->getImage()){
			$image = array(
				'id' => $i->id,
				'hash' => HTTP_IMAGE_BASE . $i->hash,
			);
		}
		$this->data['banner'] = array(
			'id' => $banner->id,
			'title' => $banner->title,
			'type' => $banner->type,
			'url' => $banner->url,
			'image' => $image,
		);

		$this->data['breadcrumb'][] = array(
			'title' => ($banner->id ? 'Изменить' : 'Создать'),
			'url' => '',
		);

		$this->data['type_titles'] = $banner->getTypeTitles();

		$this->data['HTTP_IMAGE_BASE'] = HTTP_IMAGE_BASE;

		$this->app->render('banners/save.twig', $this->data);
	}

	public function actionDelete($banners_id)
	{
		try{
			$banners = ModelBanners::fetchById($banners_id);
			$banners->delete();
		}catch (RecordNotFound $r){
		}
		$this->app->redirect($this->app->urlFor('banner_list'), 301);
	}

}