<?php
namespace Shop\Domain\Admin\Controller;

use ActiveRecord\RecordNotFound;
use Shop\Common\Facade;
use Shop\Filter\Message as FilterMessage;
use Shop\Model\Message as ModelMessage;
use Shop\Model\Catalog\Product;

class Message extends AbstractController implements Menu
{
	/**
	 * @var array
	 */
	private $data = array();

	public function __construct(Facade $app)
	{
		$this->app = $app;

		if(parent::checkUserGroupRoles(__CLASS__) === false){
			$this->app->redirect($this->app->urlFor('main'), 301);
		}

		$this->getMenu($this->data, 'message');

		$this->data['breadcrumb'] = array(
			array(
				'title' => 'Главная',
				'url' => $this->app->urlFor('main'),
			),
			array(
				'title' => 'Заявки',
				'url' => $this->app->urlFor('message_list'),
			),
		);
	}

	/**
	 * Элемент меню
	 *
	 * @param Facade $app
	 * @param string $class
	 * @return array
	 */
	public static function getElemMenu (Facade $app, $class)
	{
		return parent::checkUserGroupRoles($class) ? array(AbstractController::MENU_SHOP_ID ,array(
			'url' => $app->urlFor('message_list'),
			'title' => 'Заявки',
			'selected' => '',
		)) : array(false, false);
	}

	/**
	 * Список заявок
	 */
	public function actionList()
	{
		$this->data['title'] = 'Заявки';

		$this->data['message'] = array();
		$message = ModelMessage::fetch(new FilterMessage());
		foreach ($message as $m) {
			$this->data['message'][] = array(
				'fio' => $m->fio,
				'telephone' => $m->telephone,
				'email' => $m->email,
				'comment' => $m->comment,
				'create_time' => $m->create_time,
				'product_title' => $m->catalog_product_title,
				'url_edit' => $this->app->urlFor('message_edit', array('message' => $m->id)),
				'url_del' => $this->app->urlFor('message_delete', array('message' => $m->id)),
			);
		}

		$this->app->render('message/list.twig', $this->data);
	}

	/**
	 * Удаление заявки
	 *
	 * @param $message_id
	 */
	public function actionDelete($message_id)
	{
		try {
			$message = ModelMessage::fetchById($message_id);
			$message->delete();
		} catch (RecordNotFound $r) {
		}
		$this->app->redirect($this->app->urlFor('message_list'), 301);
	}

}