<?php
namespace Shop\Domain\Admin\Controller;

use ActiveRecord\RecordNotFound;
use Shop\Common\Facade;
use Shop\Filter\Subscription as FilterSubscription;
use Shop\Model\Subscription as ModelSubscription;
use Shop\Model\Catalog\Product;

class Subscription extends AbstractController implements Menu
{
	/**
	 * @var array
	 */
	private $data = array();

	public function __construct(Facade $app)
	{
		$this->app = $app;

		if(parent::checkUserGroupRoles(__CLASS__) === false){
			$this->app->redirect($this->app->urlFor('main'), 301);
		}

		$this->getMenu($this->data, 'subscription');

		$this->data['breadcrumb'] = array(
			array(
				'title' => 'Главная',
				'url' => $this->app->urlFor('main'),
			),
			array(
				'title' => 'Подписки',
				'url' => $this->app->urlFor('subscription_list'),
			),
		);
	}

	/**
	 * Элемент меню
	 *
	 * @param Facade $app
	 * @param string $class
	 * @return array
	 */
	public static function getElemMenu (Facade $app, $class)
	{
		return parent::checkUserGroupRoles($class) ? array(AbstractController::MENU_SHOP_ID ,array(
			'url' => $app->urlFor('subscription_list'),
			'title' => 'Подписки',
			'selected' => '',
		)) : array(false, false);
	}

	/**
	 * Список подписок
	 */
	public function actionList()
	{
		$this->data['title'] = 'Подписки';

		$this->data['subscription'] = array();
		$filter = new FilterSubscription();
		$filter->sort('create_time desc');

		$subscription = ModelSubscription::fetch($filter);
		foreach ($subscription as $s) {
			$product = Product::fetchById($s->catalog_product_id);
			if ($product === null) {
				continue;
			}

			$this->data['subscription'][] = array(
				'fio' => $s->fio,
				'email' => $s->email,
				'telephone' => $s->telephone,
				'product' => array(
					'id' => $product->id,
					'title' => $product->title,
					'article' => $product->article,
					'code' => $product->code,
				),
				'count_send' => $s->count_send,
				'create_time' => $s->create_time,
				'url_del' => $this->app->urlFor('subscription_delete', array('subscription' => $s->id)),
			);
		}

		$this->app->render('subscription/list.twig', $this->data);
	}

	public function actionDelete($subscription_id)
	{
		try {
			$subscription = ModelSubscription::fetchById($subscription_id);
			$subscription->delete();
		} catch (RecordNotFound $r) {
		}
		$this->app->redirect($this->app->urlFor('subscription_list'), 301);
	}

}