<?php
namespace Shop\Domain\Admin\Controller;

use ActiveRecord\RecordNotFound;
use Shop\Common\Facade;
use Shop\Filter\Geo as FilterGeo;
use Shop\Model\Geo as ModelGeo;

class Geo extends AbstractController implements Menu
{
	/**
	 * @var array
	 */
	private $data = array();

	public function __construct(Facade $app)
	{
		$this->app = $app;

		if(parent::checkUserGroupRoles(__CLASS__) === false){
			$this->app->redirect($this->app->urlFor('main'), 301);
		}

		$this->getMenu($this->data, 'geo');

		$this->data['breadcrumb'] = array(
			array(
				'title' => 'Главная',
				'url' => $this->app->urlFor('main'),
			),
			array(
				'title' => 'Города',
				'url' => $this->app->urlFor('geo_list'),
			),
		);
		$this->data['title'] = 'Города';
	}

	/**
	 * Элемент меню
	 *
	 * @param Facade $app
	 * @param string $class
	 * @return array
	 */
	public static function getElemMenu (Facade $app, $class)
	{
		return parent::checkUserGroupRoles($class) ? array(AbstractController::MENU_SHIPPING_ID ,array(
			'url' => $app->urlFor('geo_list'),
			'title' => 'Города',
			'selected' => '',
		)) : array(false, false);
	}

	/**
	 * List geo
	 */
	public function actionList()
	{
		$this->data['create'] = $this->app->urlFor('geo_create');

		$geos = ModelGeo::fetch(new FilterGeo());
		foreach ($geos as $g) {
			$this->data['geo'][] = array(
				'id' => $g->id,
				'title' => $g->title,
				'url_edit' => $this->app->urlFor('geo_edit', array('geo' => $g->id)),
				'url_del' => $this->app->urlFor('geo_delete', array('geo' => $g->id)),
			);
		}
		$this->app->render('geo/list.twig', $this->data);
	}

	/**
	 * Save geo
	 */
	public function actionSave($geo_id = 0)
	{
		$geo = ModelGeo::fetchById($geo_id);
		if($geo === null){
			$geo = new ModelGeo();
		}

		if ($this->app->request->isPost()) {

			$geo->title = (string)$this->app->request->post('title');
			$geo->alias = (string)$this->app->request->post('alias');
			$geo->sort = (int)$this->app->request->post('sort');

			if ($geo->save()) {
				//Редирект
				$this->app->redirect($this->app->urlFor('geo_list'), 301);
			} else {
				//Какая то ошибка
				$this->data['errors'] = array();
				foreach ($geo->errors as $k => $v) {
					$this->data['errors'][] = $v;
				}
			}
		}

		$this->data['geo'] = array(
			'title' => $geo->title,
			'alias' => $geo->alias,
			'sort' => $geo->sort,
		);

		$this->data['breadcrumb'][] = array(
			'title' => ($geo->id ? 'Изменить' : 'Создать'),
			'url' => '',
		);

		$this->app->render('geo/save.twig', $this->data);
	}

	/**
	 * Delete catalog geo
	 *
	 * @param int $geo_id
	 */
	public function actionDelete($geo_id)
	{
		try {
			$geo = ModelGeo::fetchById($geo_id);
			$geo->delete();
		} catch (RecordNotFound $r) {
		}
		$this->app->redirect($this->app->urlFor('geo_list'), 301);
	}

}