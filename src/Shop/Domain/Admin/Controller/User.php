<?php
namespace Shop\Domain\Admin\Controller;

use Shop\Common\Facade;
use Shop\Model\Users;

class User extends AbstractController implements Menu
{

	/**
	 * @var array
	 */
	private $data = array();

	public function __construct(Facade $app)
	{
		$this->app = $app;

		if(parent::checkUserGroupRoles(__CLASS__) === false){
			$this->app->redirect($this->app->urlFor('main'), 301);
		}

		$this->getMenu($this->data, 'category');

		$this->data['breadcrumb'] = array(
			array(
				'title' => 'Главная',
				'url' => $this->app->urlFor('main'),
			),
			array(
				'title' => 'Пользователи',
				'url' => '',
			)
		);
		$this->data['title'] = 'Пользователи';
	}

	/**
	 * Элемент меню
	 *
	 * @param Facade $app
	 * @param string $class
	 * @return array
	 */
	public static function getElemMenu (Facade $app, $class)
	{
		return parent::checkUserGroupRoles($class) ? array(AbstractController::MENU_SETTING_ID ,array(
			'url' => $app->urlFor('user_list'),
			'title' => 'Пользователи',
			'selected' => '',
		)) : array(false, false);
	}


	public function actionList()
	{
//		$this->data['create'] = $this->app->urlFor('user_create');
		$this->data['users'] = array();
		$users = Users::fetch(new \Shop\Filter\Users());
		foreach ($users as $u) {
			$this->data['users'][] = array(
				'id' => $u->id,
				'login' => $u->login,
				'is_active' => $u->is_active,
//				'url_edit' => $this->app->urlFor('users_edit', array('user' => $u->id)),
//				'url_del' => $this->app->urlFor('users_delete', array('user' => $u->id)),
			);
		}

		$this->app->render('users/list.twig', $this->data);
	}

}