<?php
namespace Shop\Domain\Admin\Controller;

use ActiveRecord\RecordNotFound;
use Shop\Common\Facade;
use Shop\Domain\Admin\Helper;

class Image extends AbstractController
{
	public function __construct(Facade $app)
	{
		$this->app = $app;
	}

	/**
	 * Upload
	 *
	 * @return void
	 */
	public function actionUpload()
	{
		if (isset($_FILES)) {
			$image = new \Shop\Model\Image();
			$image->tmp_name = $_FILES['img']['tmp_name'];
			$image->parent = $this->app->request->post('parent');
			$image->parent_id = $this->app->request->post('parent_id', 0);
			if ($image->save()) {
				$cache =$this->app->request->post('get_cache');
				if($cache){
					list($width, $height) = explode('x', $cache);
					$image->getHttpImage($width, $height);
				}
				$data = array('image_id' => $image->id, 'hash' => $image->hash);
			}else{
				$data = array('error' => 'No save file');
			}
		} else {
			$data = array('error' => 'No file upload');
		}
		echo json_encode($data);
	}

	/**
	 * Delete
	 *
	 * @return void
	 */
	public function actionDelete()
	{
		try {
			$image = \Shop\Model\Image::fetchById($this->app->request->post('image_id', 0));
			if($image && $image->delete()){
				$data = array('delete' => 'Ok');
			}else{
				$data = array('error' => 'No delete file');
			}
		}catch (RecordNotFound $r) {
			$data = array('error' => 'No delete image');
		}

		echo json_encode($data);
	}

}