<?php
namespace Shop\Domain\Admin\Controller;

use ActiveRecord\RecordNotFound;
use Shop\Common\Facade;
use Shop\Filter\Template as FilterTemplate;
use Shop\Model\Template as ModelTemplate;

class Template extends AbstractController implements Menu
{
	/**
	 * @var array
	 */
	private $data = array();

	public function __construct(Facade $app)
	{
		$this->app = $app;

		if(parent::checkUserGroupRoles(__CLASS__) === false){
			$this->app->redirect($this->app->urlFor('main'), 301);
		}

		$this->getMenu($this->data, 'template');

		$this->data['breadcrumb'] = array(
			array(
				'title' => 'Главная',
				'url' => $this->app->urlFor('main'),
			),
			array(
				'title' => 'Шаблоны писем',
				'url' => $this->app->urlFor('template_list'),
			),
		);
		$this->data['title'] = 'Шаблоны писем';
	}

	/**
	 * Элемент меню
	 *
	 * @param Facade $app
	 * @param string $class
	 * @return array
	 */
	public static function getElemMenu (Facade $app, $class)
	{
		return parent::checkUserGroupRoles($class) ? array(AbstractController::MENU_SETTING_ID ,array(
			'url' => $app->urlFor('template_list'),
			'title' => 'Шаблоны писем',
			'selected' => '',
		)) : array(false, false);
	}

	/**
	 * List template
	 */
	public function actionList()
	{
		$this->data['create'] = $this->app->urlFor('template_create');

		$templates = ModelTemplate::fetch(new FilterTemplate());
		foreach ($templates as $t) {
			$this->data['template'][] = array(
				'id' => $t->id,
				'title' => $t->title,
				'alias' => $t->alias,
				'url_edit' => $this->app->urlFor('template_edit', array('template' => $t->id)),
				'url_del' => $this->app->urlFor('template_delete', array('template' => $t->id)),
			);
		}
		$this->app->render('template/list.twig', $this->data);
	}

	/**
	 * Save template
	 */
	public function actionSave($template_id = 0)
	{
		$template = ModelTemplate::fetchById($template_id);
		if ($template === null) {
			$template = new ModelTemplate();
		}

		if ($this->app->request->isPost()) {

			$template->title = (string)$this->app->request->post('title');
			$template->alias = (string)$this->app->request->post('alias');
			$template->subject = (string)$this->app->request->post('subject');
			$template->message = (string)$this->app->request->post('message');

			if ($template->save()) {
				//Редирект
				$this->app->redirect($this->app->urlFor('template_list'), 301);
			} else {
				//Какая то ошибка
				$this->data['errors'] = array();
				foreach ($template->errors as $k => $v) {
					$this->data['errors'][] = $v;
				}
			}
		}

		$this->data['template'] = array(
			'title' => $template->title,
			'alias' => $template->alias,
			'subject' => $template->subject,
			'message' => $template->message,
		);

		$this->data['breadcrumb'][] = array(
			'title' => ($template->id ? 'Изменить' : 'Создать'),
			'url' => '',
		);

		$this->app->render('template/save.twig', $this->data);
	}

	/**
	 * Delete catalog template
	 *
	 * @param int $template_id
	 */
	public function actionDelete($template_id)
	{
		try {
			$template = ModelTemplate::fetchById($template_id);
			$template->delete();
		} catch (RecordNotFound $r) {
		}

		$this->app->redirect($this->app->urlFor('template_list'), 301);
	}

}