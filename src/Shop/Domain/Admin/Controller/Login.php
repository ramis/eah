<?php
namespace Shop\Domain\Admin\Controller;

use Shop\Common\Facade;
use Shop\Domain\Admin\Helper;

class Login extends AbstractController
{
	public function __construct(Facade $app)
	{
		$this->app = $app;
	}
	/**
	 * Logout
	 *
	 * @return void
	 */
	public function actionLogin()
	{
		$auth = Helper\Auth::getInstance();

		$data = array();
		if ($this->app->request->isPost()) {
			$allPostVars = $this->app->request->post();
			if (isset($allPostVars['login']) && isset($allPostVars['pass'])) {
				if ($auth->login($allPostVars) === false) {
					$data['login'] = $allPostVars['login'];
					$data['error'] = 'Не верные login или password';
				}
			}
		}
		if ($auth->isAuth()) {
			$this->app->redirect($this->app->urlFor('main'));
		}
		$this->app->render('login.twig', $data);
	}

	/**
	 * Logout
	 *
	 * @return void
	 */
	public function actionLogout()
	{
		$auth = Helper\Auth::getInstance();
		$auth->logout();
		$this->app->redirect($this->app->urlFor('login'), 301);
	}

}