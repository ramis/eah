<?php
namespace Shop\Domain\Admin\Controller;

use ActiveRecord\RecordNotFound;
use Shop\Common\Facade;
use Shop\Filter\Logs as FilterLogs;
use Shop\Model\Logs as ModelLogs;
use Shop\Model\Catalog\Product;

class Logs extends AbstractController implements Menu
{
	/**
	 * @var array
	 */
	private $data = array();

	public function __construct(Facade $app)
	{
		$this->app = $app;

		if(parent::checkUserGroupRoles(__CLASS__) === false){
			$this->app->redirect($this->app->urlFor('main'), 301);
		}

		$this->getMenu($this->data, 'logs');

		$this->data['breadcrumb'] = array(
			array(
				'title' => 'Главная',
				'url' => $this->app->urlFor('main'),
			),
			array(
				'title' => 'Логи',
				'url' => $this->app->urlFor('logs_list'),
			),
		);
		$this->data['title'] = 'Логи';
	}

	/**
	 * Элемент меню
	 *
	 * @param Facade $app
	 * @param string $class
	 * @return array
	 */
	public static function getElemMenu (Facade $app, $class)
	{
		return parent::checkUserGroupRoles($class) ? array(AbstractController::MENU_SETTING_ID ,array(
			'url' => $app->urlFor('logs_list'),
			'title' => 'Логи',
			'selected' => '',
		)) : array(false, false);
	}

	/**
	 * Список логов
	 */
	public function actionList()
	{
		$this->data['logs'] = array();
		$filter = new FilterLogs();
		$logs = ModelLogs::fetch($filter->sort('create_time desc'));
		foreach ($logs as $l) {
			$this->data['logs'][] = array(
				'title' => $l->title,
				'cron' => $l->cron,
				'create_time' => $l->create_time,
				'url_view' => $this->app->urlFor('logs_view', array('logs' => $l->id)),
				'url_del' => $this->app->urlFor('logs_delete', array('logs' => $l->id)),
			);
		}

		$this->app->render('logs/list.twig', $this->data);
	}

	/**
	 * Просмотр лога
	 *
	 * @param $logs_id
	 */
	public function actionView($logs_id)
	{
		$logs = ModelLogs::fetchById($logs_id);
		if ($logs === null) {
			$this->app->redirect($this->app->urlFor('logs_list'), 301);
		}

		$this->data['log'] = array(
			'title' => $logs->title,
			'cron' => $logs->cron,
			'log' => $logs->log,
			'create_time' => $logs->create_time,
		);
		$this->app->render('logs/view.twig', $this->data);

	}

	/**
	 * Удаление лога
	 *
	 * @param $logs_id
	 */
	public function actionDelete($logs_id)
	{
		try {
			$logs = ModelLogs::fetchById($logs_id);
			$logs->delete();
		} catch (RecordNotFound $r) {
		}
		$this->app->redirect($this->app->urlFor('logs_list'), 301);
	}

}