<?php
namespace Shop\Domain\Admin\Controller\Catalog;

use ActiveRecord\RecordNotFound;
use Shop\Model\Catalog\Product\Category as ProductCategory;
use Shop\Common\Facade;
use Shop\Common\Hierarchy;
use Shop\Domain\Admin\Controller\AbstractController;
use Shop\Domain\Admin\Controller\Menu;
use Shop\Filter\Catalog\Product as FilterProduct;
use Shop\Filter\Catalog\Category as FilterCategory;
use Shop\Filter\Catalog\Product\Attribute as FilterAttribute;
use Shop\Filter\Catalog\Product\Category as FilterProductCategory;
use Shop\Model\Image;
use Shop\Model\Catalog\Product as ModelProduct;
use Shop\Model\Catalog\Category as ModelCategory;
use Shop\Model\Catalog\Product\Attribute as ModelAttribute;
use Shop\Model\Catalog\Product\Attribute\Value as ModelValue;

class Product extends AbstractController implements Menu
{
	/**
	 * @var array
	 */
	private $data = array();

	public function __construct(Facade $app)
	{
		$this->app = $app;

		if(parent::checkUserGroupRoles(__CLASS__) === false){
			$this->app->redirect($this->app->urlFor('main'), 301);
		}

		$this->getMenu($this->data, 'product');

		$this->data['breadcrumb'] = array(
			array(
				'title' => 'Главная',
				'url' => $this->app->urlFor('main'),
			),
			array(
				'title' => 'Продукты',
				'url' => $this->app->urlFor('product_list'),
			),
		);
		$this->data['title'] = 'Продукты';
	}

	/**
	 * Элемент меню
	 *
	 * @param Facade $app
	 * @param string $class
	 * @return array
	 */
	public static function getElemMenu (Facade $app, $class)
	{
		return parent::checkUserGroupRoles($class) ? array(AbstractController::MENU_CATALOG_ID ,array(
			'url' => $app->urlFor('product_list'),
			'title' => 'Продукты',
			'selected' => '',
		)) : array(false, false);
	}

	/**
	 * List catalog product
	 */
	public function actionList()
	{
		$this->data['create'] = $this->app->urlFor('product_create');

		//Все категории
		$filter = new FilterCategory();
		$filter->sort();

		$this->data['categories'] = ModelCategory::getCatalogCategories();

		$this->app->render('catalog/product/list.twig', $this->data);
	}

	/**
	 * List catalog product JSON
	 */
	public function actionListJson()
	{
		$filter = new FilterProduct();
		//По названию
		if ($this->app->request->get('title')) {
			$filter->title($this->app->request->get('title'));
		}
		//По категории
		if ($this->app->request->get('category')) {
			$category = Category::fetchById($this->app->request->get('category', 0));
			if ($category) {
				$filter->category($category);
			}
		}
		//По цене
		if ($this->app->request->get('price')) {
			$filter->price($this->app->request->get('price'));
		}
		//По коду
		if ($this->app->request->get('code')) {
			$filter->code($this->app->request->get('code'));
		}
		//По артиклу
		if ($this->app->request->get('article')) {
			$filter->article($this->app->request->get('article'));
		}

		$filter->publish(true);
		if ($this->app->request->get('all')) {
			$filter->publish(false);
		}

		$this->data['products'] = array();
		$count = ModelProduct::fetchCount($filter);

		$this->data['pages'] = ceil($count / 10);
		//Страница
		$page = $this->app->request->get('page') ? (int)$this->app->request->get('page') : 1;
		$filter->limit(10, $page);
		$this->data['page'] = $page;

		//Сортировка
		if ($this->app->request->get('sort')) {
			$filter->sort($this->app->request->get('sort'));
		}
		$products = ModelProduct::fetch($filter);

		foreach ($products as $p) {

			$i = $p->getMainImage();

			$categories = array();
			foreach ($p->getCatalogCategories() as $c) {
				$categories[] = $c->title;
			}

			$this->data['products'][] = array(
				'id' => $p->id,
				'title' => $p->title,
				'code' => $p->code,
				'price' => $p->price,
				'special_price' => $p->special_price,
				'article' => $p->article,
				'categories' => $categories,
				'hash' => ($i ? $i->getHttpImage(100, 100) : ''),
				'url_edit' => $this->app->urlFor('product_edit', array('product' => $p->id)),
				'url_del' => $this->app->urlFor('product_delete', array('product' => $p->id)),
			);
		}

		$products = $this->app->view->fetch('catalog/product/list_json.twig', $this->data);
		$nav = $this->app->view->fetch('catalog/product/nav_json.twig', $this->data);

		print json_encode(array('elements' => $products, 'nav' => $nav));

	}

	/**
	 * Save catalog product
	 * @param int $product_id
	 */
	public function actionSave($product_id = 0)
	{
		$product = ModelProduct::fetchById($product_id);

		if ($product === null) {
			$product = new ModelProduct();
		}

		if ($this->app->request->isPost()) {
			$product->title = (string)$this->app->request->post('title');
			$product->description = (string)$this->app->request->post('description');
			$product->meta_description = (string)$this->app->request->post('meta_description');
			$product->meta_keyword = (string)$this->app->request->post('meta_keyword');
			$product->seo_title = (string)$this->app->request->post('seo_title');
			$product->seo_h1 = (string)$this->app->request->post('seo_h1');
			$product->is_publish = (bool)$this->app->request->post('is_publish');
			$product->update_time = (string)date('Y-m-d H:i:s');

			if ($product->id === NULL) {
				$product->create_time = (string)date('Y-m-d H:i:s');
			}

			$product->article = (string)$this->app->request->post('article', '');
			$product->video = (string)$this->app->request->post('video', '');
			$product->code = (string)$this->app->request->post('code', '');
			$product->quantity = (int)$this->app->request->post('quantity', 0);
			$product->price = (int)$this->app->request->post('price', 0);
			$product->special_price = (int)$this->app->request->post('special_price', 0);

			//Аттрибуты
			$attribute_ids = (array)$this->app->request->post('attribute_ids', array());
			$values = array();
			$filter_value = new FilterAttribute\Value();
			foreach ($attribute_ids as $item) {
				if ($this->app->request->post($item, false)) {

					$key = (int)preg_replace('/\D+/', '', $item);
					$val = trim($this->app->request->post($item));

					$attribute = ModelAttribute::fetchById($key);
					if($attribute === null){
						continue;
					}

					$filter_value->product($product);
					$filter_value->attribute($attribute);
					$value = ModelValue::fetchOne($filter_value);
					if ($value === null) {
						$value = new ModelValue();
						$value->catalog_product_id = $product->id;
						$value->catalog_product_attribute_id = $attribute->id;
					}
					$value->text = $val;
					$values[] = $value;
				}
			}

			$product->setAttributes($values);

			//Категории
			$categories = (array)$this->app->request->post('categories', array());
			$product_categories = array();
			$filter = new FilterProductCategory();
			foreach ($categories as $k=>$c) {
				$category = ModelCategory::fetchById($c);
				if ($category === null) {
					continue;
				}

				$product_category = null;
				if($product->id){
					$filter->product($product);
					$filter->category($category);
					$product_category = ProductCategory::fetchOne($filter);
				}
				if($product_category === null){
					$product_category = new ProductCategory();
				}

				$product_category->catalog_category_id = $category->id;
				$product_category->is_main = ($k === 0);
				$product_categories[] = $product_category;
			}
			$product->setCatalogCategories($product_categories);

			//Image
			$product->main_image_id = $this->app->request->post('image_id');
			$is_new_product = ($product->id === NULL && $product->main_image_id);

			if ($product->save()) {
				if ($is_new_product) {
					$image = Image::find($product->main_image_id);
					$image->parent_id = (int)$product->id;
					$image->change();
				}

				//Редирект
				$this->app->redirect($this->app->urlFor('product_edit', array('product' => $product->id)), 301);
			} else {
				//Какая то ошибка
				$this->data['errors'] = array();
				foreach ($product->errors as $k => $v) {
					$this->data['errors'][] = $v;
				}
			}
		}

		$image = array();
		if ($i = $product->getMainImage()) {
			$image = array(
				'id' => $i->id,
				'hash' => $i->getHttpImage(100, 100),
			);
		}

		$this->data['product'] = array(
			'id' => $product->id,
			'title' => $product->title,
			'code' => $product->code,
			'article' => $product->article,
			'description' => $product->description,
			'meta_description' => $product->meta_description,
			'meta_keyword' => $product->meta_keyword,
			'seo_title' => $product->seo_title,
			'seo_h1' => $product->seo_h1,
			'quantity' => $product->quantity,
			'minimum' => $product->minimum,
			'viewed' => $product->viewed,
			'video' => $product->video,
			'price' => $product->price,
			'special_price' => $product->special_price,
			'is_publish' => $product->is_publish,
			'is_new' => $product->is_new,
			'image' => $image,
		);

		//Картинки продукта
		$this->data['images'] = array();
		if($product->id){
			$filter = new \Shop\Filter\Image();
			$filter
				->parent('product')
				->parentId($product->id);
			$images = Image::fetch($filter);
			foreach ($images as $i) {
				$this->data['images'][] = array(
					'id' => $i->id,
					'hash' => $i->getHttpImage(200, 200),
				);
			}
		}

		//Аттрибуты продукта
		$product_attributes = array();
		foreach ($product->getAttributes() as $a) {
			$product_attributes[$a->catalog_product_attribute_id] = $a->text;
		}

		//Категории продукта
		$product_categories_id = array();
		$product_categories = $product->getCatalogCategories();
		foreach ($product_categories as $c) {
			$product_categories_id[] = $c->id;
		}

		//Все категории
		$filter = new FilterCategory();
		$filter->sort();

		$this->data['categories'] = ModelCategory::getCatalogCategories();

		//Все аттрибуты продукта
		$this->data['attributes'] = array();
		$filter = new FilterAttribute();
		$filter->sort();
		$attributes = ModelAttribute::fetch($filter);
		foreach ($attributes as $a) {
			$this->data['attributes'][] = array(
				'id' => $a->id,
				'title' => $a->title,
				'value' => (isset($product_attributes[$a->id])) ? $product_attributes[$a->id] : '',
			);
		}

		$this->data['breadcrumb'][] = array(
			'title' => ($product->id ? 'Изменить продукт' : 'Создать продукт'),
			'url' => '',
		);
		$this->data['http_image'] = HTTP_IMAGE_100x100;
		$this->data['http_image_200x200'] = HTTP_IMAGE_200x200;

		$this->app->render('catalog/product/save.twig', $this->data);
	}

	/**
	 * Delete catalog product
	 *
	 * @param int $product_id
	 */
	public function actionDelete($product_id)
	{
		try {
			$product = ModelProduct::fetchById($product_id);
			$product->is_publish = false;
			$product->change();
		} catch (RecordNotFound $r) {
		}
		$this->app->redirect($this->app->urlFor('product_list'), 301);
	}

	public function actionGet()
	{
		$json = array();
		if ($this->app->request->isPost()) {

			$product_title = (string)$this->app->request->post('title');
			$product = ModelProduct::find_by_title($product_title);
			if ($product instanceof ModelProduct) {
				$json = array(
					'id' => $product->id,
					'title' => $product->title,
					'article' => $product->article,
					'code' => $product->code,
					'price' => $product->price,
				);
			}
		}
		print json_encode($json);
	}
}