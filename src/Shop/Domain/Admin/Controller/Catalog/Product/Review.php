<?php
namespace Shop\Domain\Admin\Controller\Catalog\Product;

use ActiveRecord\RecordNotFound;
use Shop\Common\Facade;
use Shop\Domain\Admin\Controller\Menu;
use Shop\Filter\Catalog\Product\Review as FilterReview;
use Shop\Model\Catalog\Product\Review as ModelReview;
use Shop\Domain\Admin\Controller\AbstractController;

class Review extends AbstractController implements Menu
{
	/**
	 * @var array
	 */
	private $data = array();

	public function __construct(Facade $app)
	{
		$this->app = $app;

		if(parent::checkUserGroupRoles(__CLASS__) === false){
			$this->app->redirect($this->app->urlFor('main'), 301);
		}

		$this->getMenu($this->data, 'category');

		$this->data['breadcrumb'] = array(
			array(
				'title' => 'Главная',
				'url' => $this->app->urlFor('main'),
			),
			array(
				'title' => 'Продукты',
				'url' => $this->app->urlFor('product_list'),
			),
			array(
				'title' => 'Отзывы',
				'url' => $this->app->urlFor('review_list'),
			),
		);
		$this->data['title'] = 'Отзывы';
	}

	/**
	 * Элемент меню
	 *
	 * @param Facade $app
	 * @param string $class
	 * @return array
	 */
	public static function getElemMenu (Facade $app, $class)
	{
		return parent::checkUserGroupRoles($class) ? array(AbstractController::MENU_CATALOG_ID ,array(
			'url' => $app->urlFor('review_list'),
			'title' => 'Отзывы',
			'selected' => '',
		)) : array(false, false);
	}

	public function actionList()
	{
		$filter = new FilterReview();
		$filter->sort('is_publish asc, create_time desc');

		$this->data['reviews'] = array();
		$reviews = ModelReview::fetch($filter);

		foreach ($reviews as $r) {

			$this->data['reviews'][] = array(
				'id' => $r->id,
				'author' => $r->author,
				'product' => $r->getCatalogProduct()->title,
				'rating' => $r->rating,
				'is_publish' => $r->is_publish,
				'create_time' => $r->create_time,
				'url_edit' => $this->app->urlFor('review_edit', array('review' => $r->id)),
				'url_del' => $this->app->urlFor('review_delete', array('review' => $r->id)),
			);
		}
		$this->app->render('catalog/product/review/list.twig', $this->data);
	}

	/**
	 * @param int $review_id
	 */
	public function actionEdit($review_id)
	{
		$review = ModelReview::fetchById($review_id);
		if ($review === null) {
			$this->app->redirect($this->app->urlFor('review_list'), 301);
		}

		if ($this->app->request->isPost()) {

			$review->author = (string)$this->app->request->post('author');
			$review->text = (string)$this->app->request->post('text');
			$review->is_publish = (bool)$this->app->request->post('is_publish');

			if ($review->save()) {
				//Редирект
				$this->app->redirect($this->app->urlFor('review_list'), 301);
			}
		}

		$this->data['review'] = array(
			'id' => $review->id,
			'author' => $review->author,
			'text' => $review->text,
			'is_publish' => $review->is_publish,
			'rating' => $review->rating,
		);

		$this->data['breadcrumb'][] = array(
			'title' => 'Редактирование отзыва',
			'url' => '',
		);

		$this->app->render('catalog/product/review/save.twig', $this->data);
	}

	/**
	 * @param int $review_id
	 */
	public function actionDelete($review_id)
	{
		try {
			$review = ModelReview::fetchById($review_id);
			$review->delete();
		} catch (RecordNotFound $r) {

		}
		$this->app->redirect($this->app->urlFor('review_list'), 301);
	}

}