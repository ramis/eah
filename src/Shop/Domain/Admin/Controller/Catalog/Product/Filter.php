<?php
namespace Shop\Domain\Admin\Controller\Catalog\Product;

use Shop\Common\Facade;
use Shop\Domain\Admin\Controller\AbstractController;
use Shop\Domain\Admin\Controller\Menu;

class Filter extends AbstractController implements Menu
{
	/**
	 * @var array
	 */
	private $data = array();

	public function __construct(Facade $app)
	{
		$this->app = $app;

		if(parent::checkUserGroupRoles(__CLASS__) === false){
			$this->app->redirect($this->app->urlFor('main'), 301);
		}

		$this->getMenu($this->data, 'category');

		$this->data['breadcrumb'] = array(
			array(
				'title' => 'Главная',
				'url' => $this->app->urlFor('main'),
			),
			array(
				'title' => 'Фильтры',
				'url' => $this->app->urlFor('filter_list'),
			),
		);
	}

	/**
	 * Элемент меню
	 *
	 * @param Facade $app
	 * @param string $class
	 * @return array
	 */
	public static function getElemMenu (Facade $app, $class)
	{
		return parent::checkUserGroupRoles($class) ? array(AbstractController::MENU_CATALOG_ID ,array(
			'url' => $app->urlFor('filter_list'),
			'title' => 'Фильтра',
			'selected' => '',
		)) : array(false, false);
	}

	public function actionSave($filter_id = 0)
	{
		$this->app->render('catalog/product/filter/save.twig', $this->data);
	}

	public function actionDelete($filter_id)
	{
		$this->app->redirect($this->app->urlFor('review_list'), 301);
	}

}