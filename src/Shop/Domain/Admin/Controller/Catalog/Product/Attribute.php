<?php
namespace Shop\Domain\Admin\Controller\Catalog\Product;

use ActiveRecord\RecordNotFound;
use Shop\Common\Facade;
use Shop\Domain\Admin\Controller\AbstractController;
use Shop\Domain\Admin\Controller\Menu;
use Shop\Filter\Catalog\Product\Attribute as FilterAttribute;
use Shop\Model\Catalog\Product\Attribute as ModelAttribute;

class Attribute extends AbstractController implements Menu
{
	/**
	 * @var array
	 */
	private $data = array();

	public function __construct(Facade $app)
	{
		$this->app = $app;

		if(parent::checkUserGroupRoles(__CLASS__) === false){
			$this->app->redirect($this->app->urlFor('main'), 301);
		}

		$this->getMenu($this->data, 'attribute');

		$this->data['breadcrumb'] = array(
			array(
				'title' => 'Главная',
				'url' => $this->app->urlFor('main'),
			),
			array(
				'title' => 'Продукты',
				'url' => $this->app->urlFor('product_list'),
			),
			array(
				'title' => 'Характеристики',
				'url' => $this->app->urlFor('attribute_list'),
			),
		);
		$this->data['title'] = 'Характеристики';
	}

	/**
	 * Элемент меню
	 *
	 * @param Facade $app
	 * @param string $class
	 * @return array
	 */
	public static function getElemMenu (Facade $app, $class)
	{
		return parent::checkUserGroupRoles($class) ? array(AbstractController::MENU_CATALOG_ID ,array(
			'url' => $app->urlFor('attribute_list'),
			'title' => 'Аттрибуты',
			'selected' => '',
		)) : array(false, false);
	}

	/**
	 * List catalog attribute
	 */
	public function actionList()
	{
		$this->data['create'] = $this->app->urlFor('attribute_create');
		$filter = new FilterAttribute();
		$filter->sort();

		$this->data['attributes'] = array();
		$attributes = ModelAttribute::fetch($filter);
		foreach ($attributes as $a) {
			$this->data['attributes'][] = array(
				'id' => $a->id,
				'title' => $a->title,
				'sort' => $a->sort,
				'url_edit' => $this->app->urlFor('attribute_edit', array('attribute' => $a->id)),
				'url_del' => $this->app->urlFor('attribute_delete', array('attribute' => $a->id)),
			);
		}

		$this->app->render('catalog/product/attribute/list.twig', $this->data);
	}

	/**
	 * Save catalog product attribute
	 */
	public function actionSave($attribute_id = 0)
	{
		$attribute = ModelAttribute::fetchById($attribute_id);
		if ($attribute === null) {
			$attribute = new ModelAttribute();
		}

		if ($this->app->request->isPost()) {

			$attribute->title = (string)$this->app->request->post('title');
			$attribute->sort = (int)$this->app->request->post('sort');

			if ($attribute->save()) {
				//Редирект
				$this->app->redirect($this->app->urlFor('attribute_list'), 301);
			} else {
				//Какая то ошибка
				$this->data['errors'] = array();
				foreach ($attribute->errors as $k => $v) {
					$this->data['errors'][] = $v;
				}
			}
		}

		$this->data['attribute'] = array(
			'title' => $attribute->title,
			'sort' => $attribute->sort,
		);

		$this->data['breadcrumb'][] = array(
			'title' => ($attribute->id ? 'Изменить' : 'Добавить'),
			'url' => '',
		);

		$this->app->render('catalog/product/attribute/save.twig', $this->data);
	}

	/**
	 * Delete catalog attribute
	 *
	 * @param int $attribute_id
	 */
	public function actionDelete($attribute_id)
	{
		try {
			$attribute = ModelAttribute::fetchById($attribute_id);
			$attribute->delete();
		} catch (RecordNotFound $r) {
		}
		$this->app->redirect($this->app->urlFor('attribute_list'), 301);
	}

}