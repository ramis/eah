<?php
namespace Shop\Domain\Admin\Controller\Catalog;

use ActiveRecord\RecordNotFound;
use Shop\Common\Facade;
use Shop\Domain\Admin\Controller\AbstractController;
use Shop\Domain\Admin\Controller\Menu;
use Shop\Filter\Catalog\Category as FilterCategory;
use Shop\Model\Catalog\Category as ModelCategory;
use Shop\Filter\Catalog\Category\Dependence as FilterDependence;
use Shop\Model\Catalog\Category\Dependence as ModelDependence;
use Shop\Model\Image;

class Category extends AbstractController implements Menu
{
	/**
	 * @var array
	 */
	private $data = array();

	public function __construct(Facade $app)
	{
		$this->app = $app;

		if(parent::checkUserGroupRoles(__CLASS__) === false){
			$this->app->redirect($this->app->urlFor('main'), 301);
		}

		$this->getMenu($this->data);

		$this->data['breadcrumb'] = array(
			array(
				'title' => 'Главная',
				'url' => $this->app->urlFor('main'),
			),
			array(
				'title' => 'Категории',
				'url' => $this->app->urlFor('categories_list'),
			),
		);
		$this->data['title'] = 'Категории';
	}

	/**
	 * Элемент меню
	 *
	 * @param Facade $app
	 * @param string $class
	 * @return array
	 */
	public static function getElemMenu (Facade $app, $class)
	{
		return parent::checkUserGroupRoles($class) ? array(AbstractController::MENU_CATALOG_ID ,array(
			'url' => $app->urlFor('categories_list'),
			'title' => 'Категории',
			'selected' => '',
		)) : array(false, false);
	}

	/**
	 * List catalog category
	 */
	public function actionList()
	{
		$this->data['create'] = $this->app->urlFor('category_create');

		$categories = array();
		foreach (ModelCategory::getCatalogCategories() as $c) {
			$c['url_edit'] = $this->app->urlFor('category_edit', array('category' => $c['id']));
			$c['url_del'] = $this->app->urlFor('category_delete', array('category' => $c['id']));
			$categories[] = $c;
		}
		$this->data['categories'] = $categories;

		$this->app->render('catalog/category/list.twig', $this->data);
	}

	/**
	 * Save catalog category
	 *
	 * @param int $category_id
	 * @throws RecordNotFound
	 */
	public function actionSave($category_id = 0)
	{
		$category = ModelCategory::fetchById($category_id);
		if ($category === null) {
			$category = new ModelCategory();
		}

		if ($this->app->request->isPost()) {

			$category->title = (string)$this->app->request->post('title');
			$category->title_eng = (string)$this->app->request->post('title_eng');

			$category->url = (string)$this->app->request->post('url');
			$category->description = (string)$this->app->request->post('description');
			$category->meta_description = (string)$this->app->request->post('meta_description');
			$category->meta_keyword = (string)$this->app->request->post('meta_keyword');
			$category->seo_title = (string)$this->app->request->post('seo_title');
			$category->seo_h1 = (string)$this->app->request->post('seo_h1');
			$category->sort = (int)$this->app->request->post('sort');
			$category->is_publish = (bool)$this->app->request->post('is_publish');
			$category->update_time = (string)date('Y-m-d H:i:s');
			if ($category->id === NULL) {
				$category->create_time = $category->update_time;
			}

			//Childs
			$categories = (array)$this->app->request->post('child_categories', array());
			$childs = array();
			$filter = new FilterDependence;
			$dependence = null;
			foreach ($categories as $k=>$c) {
				$child = ModelCategory::fetchById($c);
				if ($child === null) {
					continue;
				}

				$product_category = null;
				if($category->id){
					$filter->parent($category);
					$filter->child($child);
					$dependence = ModelDependence::fetchOne($filter);
				}
				if($dependence === null){
					$dependence = new ModelDependence();
				}
				$dependence->sort = $k;
				$childs[] = $dependence;
			}
			$category->setChilds($childs);

			//Image
			$category->image_id = $this->app->request->post('image_id');
			$is_new_category = ($category->id === NULL && $category->image_id);
			if ($category->save()) {
				if ($is_new_category) {
					$image = Image::find($category->image_id);
					$image->parent_id = (int)$category->id;
					$image->change();
				}
				//Редирект
				$this->app->redirect($this->app->urlFor('categories_list'), 301);
			} else {
				//Какая то ошибка
				$this->data['errors'] = array();
				foreach ($category->errors as $k => $v) {
					$this->data['errors'][] = $v;
				}
			}
		}

		$image = array();
		if ($i = $category->getImage()) {
			$image = array(
				'id' => $i->id,
				'hash' => HTTP_IMAGE_BASE . $i->hash,
			);
		}
		$this->data['category'] = array(
			'id' => $category->id,
			'title' => $category->title,
			'title_eng' => $category->title_eng,
			'url' => $category->url,
			'description' => $category->description,
			'meta_description' => $category->meta_description,
			'meta_keyword' => $category->meta_keyword,
			'seo_title' => $category->seo_title,
			'seo_h1' => $category->seo_h1,
			'sort' => $category->sort,
			'is_publish' => $category->is_publish,
			'image' => $image,
		);

		$parents = array();
		$objParents = $category->getParents();
		if($objParents){
			foreach($category->getParents() as $parent){
				$parents[] = array(
					'id' => $parent->id,
					'title' => $parent->title,
					'url' => $this->app->urlFor('category_edit', array('category' => $parent->id)),
				);
			}
		}
		$this->data['category']['parents'] = $parents;

		$this->data['breadcrumb'][] = array(
			'title' => ($category->id ? 'Изменить категорию' : 'Создать категорию'),
			'url' => '',
		);


		$this->data['categories'] = ModelCategory::getCatalogCategories();

		$this->data['HTTP_IMAGE_BASE'] = HTTP_IMAGE_BASE;

		$this->app->render('catalog/category/save.twig', $this->data);
	}

	/**
	 * Delete catalog category
	 *
	 * @param int $category_id
	 */
	public function actionDelete($category_id)
	{
		try {
			$category = ModelCategory::fetchById($category_id);
			$category->delete();
		} catch (RecordNotFound $r) {
		}
		$this->app->redirect($this->app->urlFor('categories_list'), 301);
	}

}