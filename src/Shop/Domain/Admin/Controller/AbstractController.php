<?php
namespace Shop\Domain\Admin\Controller;

use Shop\Common\Controller;
use Shop\Common\Facade;
use Shop\Domain\Admin\Helper\Auth;

abstract class AbstractController extends Controller
{
	const MENU_CATALOG_ID = 1;
	const MENU_NEWS_ID = 2;
	const MENU_SHOP_ID = 3;
	const MENU_SHIPPING_ID = 4;
	const MENU_SETTING_ID = 5;

	protected $controllers = array(
		'Shop\Domain\Admin\Controller\Catalog\Category',

		'Shop\Domain\Admin\Controller\Catalog\Product',
		'Shop\Domain\Admin\Controller\Catalog\Product\Attribute',
//		'Shop\Domain\Admin\Controller\Catalog\Product\Filter',
		'Shop\Domain\Admin\Controller\Catalog\Product\Review',

		'Shop\Domain\Admin\Controller\Information',
//		'Shop\Domain\Admin\Controller\Tips',

		'Shop\Domain\Admin\Controller\Geo',
		'Shop\Domain\Admin\Controller\Shipping',

		'Shop\Domain\Admin\Controller\Order',
		'Shop\Domain\Admin\Controller\Message',
		'Shop\Domain\Admin\Controller\Banners',
		'Shop\Domain\Admin\Controller\Subscription',

		'Shop\Domain\Admin\Controller\User',
		'Shop\Domain\Admin\Controller\Template',
		'Shop\Domain\Admin\Controller\Logs',
		'Shop\Domain\Admin\Controller\Setting',

	);

	/**
	 * Меню
	 *
	 * @param array $data
	 */
	protected function getMenu(array &$data)
	{
		$data['menu'][self::MENU_CATALOG_ID] = array(
			'title' => 'Каталог',
			'submenu' => array(),
		);
		$data['menu'][self::MENU_NEWS_ID] = array(
			'title' => 'Статьи',
			'submenu' => array(),
		);
		$data['menu'][self::MENU_SHOP_ID] = array(
			'title' => 'Продажи',
			'submenu' => array(),
		);
		$data['menu'][self::MENU_SHIPPING_ID] = array(
			'title' => 'Доставки',
			'submenu' => array(),
		);
		$data['menu'][self::MENU_SETTING_ID] = array(
			'title' => 'Настройки',
			'submenu' => array(),
		);

		foreach ($this->controllers as $controller){
			list($menu, $elem) = $controller::getElemMenu($this->app, $controller);
			if($menu){
				$data['menu'][$menu]['submenu'][] = $elem;
			}
		}

		foreach ($data['menu'] as $key=>$menu){
			if(count($data['menu'][$key]['submenu']) === 0){
				unset($data['menu'][$key]);
			}
		}

	}

	/**
	 * Проверка прав доступа
	 *
	 * @param string $class
	 * @return bool
	 */
	public static function checkUserGroupRoles($class)
	{
		$auth = Auth::getInstance();

		if($auth->getUser() === null){
			return false;
		}

		$roles = $auth->getUser()->getUsersGroup()->getRoles();

		return in_array($class, $roles);
	}

}