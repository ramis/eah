$(function(){
	$('#product').on('change', function(){
		$('#product_id').val($("#product option:selected").val());
	});
	$('.typeahead').typeahead();

	$('#add_product').on('click', function(){
		product_title = $('#product_title').val();
		var data = new FormData();
		data.append('title', $('#product_title').val());
		$.ajax({
			url: '/get/product/',
			type: 'POST',
			data: data,
			cache: false,
			dataType: 'json',
			processData: false,
			contentType: false,
			success: function (data) {
				$('#order_products').append('' +
				'<tr><td>' + data.title + '</td>' +
					'<td class="text-center">' + data.article + '</td>' +
					'<td class="text-center">' + data.code + '</td>' +
					'<td class="text-center">' + $('#quantity').val() + '</td>' +
					'<td class="text-center">' + data.price + '</td>' +
					'<td class="text-center">' + parseInt(data.price) * parseInt($('#quantity').val()) + '</td>' +
				'</tr>');
			}
		});
		return false;
	});

	$('#add_history').on('click', function(){
		product_title = $('#product_title').val();
		var data = new FormData();
		data.append('comment', $('#status_comment').val());
		data.append('status', $('#status_id option:selected').val());
		data.append('order', $('#order_id').val());
		$.ajax({
			url: '/order/history/add/',
			type: 'POST',
			data: data,
			cache: false,
			dataType: 'json',
			processData: false,
			contentType: false,
			success: function (data) {
				$('#order_history').append('' +
				'<tr><td>' + data.date + '</td>' +
				'<td>' + data.comment + '</td>' +
				'<td>' + data.status + '</td>' +
				'</tr>');
			}
		});
		return false;
	});
	$('#add_geo_shipping').on('click', function(){
		var data = new FormData();
		data.append('description', $('#description').val());
		data.append('shipping_id', $('#shipping_id').val());
		data.append('geo_id', $('#geo_id option:selected').val());
		data.append('price', $('#price').val());
		data.append('point_id', $('#point_id').val());
		$.ajax({
			url: '/shipping/geo/add/',
			type: 'POST',
			data: data,
			cache: false,
			dataType: 'json',
			processData: false,
			contentType: false,
			success: function (data) {
				$('#values').append('' +
				'<tr><td><input type="hidden" class="form-control" name="value_id[]" value="' + data.id + '" />' +
				'<input type="hidden" class="form-control" name="value_geo[]" value="' + data.geo_id + '"/>' + data.geo_title +
				'</td><td class="text-center"><input type="text" class="form-control" name="value_price[]" value="' + data.price + '"/></td>' +
				'<td><input type="text" class="form-control" name="value_description[]" value="' + data.description + '"/></td>' +
				'<td><input type="text" class="form-control" name="value_point_id[]" value="' + data.point_id + '"/></td>' +
				'</tr>');
			}
		});
		return false;
	});

	$('#add_cat').click(function(){
		option = $('#all_categories option:selected');
		if($("#category_"+ option.val()).val() == undefined && option.val()){
			$('#prod_categories').append('<option value="' + option.val() + '" id="category_' + option.val() + '">' + option.html() + '</option>');
		}
		return false;
	});
	$('#del_cat').click(function(){
		option = $('#prod_categories option:selected');
		option.remove();
		return false;
	});
	$('#button_save').click(function(){
		$('#prod_categories option').prop('selected', true);
	});
});
