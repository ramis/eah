$(function () {
	$('#image_id').on('change', delayUpload);

	$('#product_images').on('change', function(){
		var data = new FormData();
		data.append('img', this.files[0]);
		data.append('parent', parent);
		data.append('parent_id', parent_id);
		data.append('get_cache', '200x200');

		$.ajax({
			url: '/upload/',
			type: 'POST',
			data: data,
			cache: false,
			dataType: 'json',
			processData: false,
			contentType: false,
			success: function (data, textStatus, jqXHR) {
				if (typeof data.error === 'undefined') {
					$('#product_images_list').append(
					'<div class="col-xs-6 col-md-2" id="images_' + data.image_id + '">' +
					'<span class="glyphicon glyphicon-remove product_images_remove" value="' + data.image_id + '">' +
					'</span><a href="#" class="thumbnail"><img src="' + http_image_200x200 + data.hash + '"/></a></div>');
				}
			}
		});
		return false;
	});

	$('.row span').on('click', function(){
		var data = new FormData();
		parent_id = 'images_' + $(this).attr('value');
		data.append('image_id', $(this).attr('value'));
		$.ajax({
			url: '/delete/',
			type: 'POST',
			data: data,
			cache: false,
			dataType: 'json',
			processData: false,
			contentType: false,
			success: function () {
				$('#'+ parent_id).remove();
			}
		});
	});

	$('.clear_image').on('click', deleteFiles);

	function deleteFiles() {
		var data = new FormData();
		data.append('image_id', $(this).attr('value'));
		$.ajax({
			url: '/delete/',
			type: 'POST',
			data: data,
			cache: false,
			dataType: 'json',
			processData: false,
			contentType: false,
			success: function (data, textStatus, jqXHR) {
				if (typeof data.error === 'undefined') {
					$('#image_container').html('');
					$('#image_container').html('<input type="file" accept="image/jpeg,image/png,image/gif" id="image_id" name="image"/>');
					$('#image_id').on('change', delayUpload);
				}
			}
		});
	}
	function delayUpload() {
		var data = new FormData();
		data.append('img', this.files[0]);
		data.append('parent', parent);
		data.append('parent_id', parent_id);
		data.append('get_cache', get_cache);

		$.ajax({
			url: '/upload/',
			type: 'POST',
			data: data,
			cache: false,
			dataType: 'json',
			processData: false,
			contentType: false,
			success: function (data, textStatus, jqXHR) {
				if (typeof data.error === 'undefined') {
					http_image = http_image == undefined ? '' : http_image;
					$('#image_container').html('');
					$('#image_container').html('<img alt="140x140" class="img-thumbnail" src="' +
					http_image + data.hash + '"/>' + '<input type="hidden" name="image_id" value="' +
					data.image_id + '">' + '<div class="clear_image" value="' + data.image_id +
					'">[x]</div>');
					$('.clear_image').on('click', deleteFiles);
				}
			}
		});
	}
});