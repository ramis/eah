$(function(){
	$('#clear_filter').click(function(){
		$('input.filter').val('');
		clearTimeout(timerID);
		timerID = setTimeout('filter()', 500);
	});
	$('input.filter').keypress(function () {
		clearTimeout(timerID);
		timerID = setTimeout('filter()', 500);
	});
	$('select.filter').change(function () {
		clearTimeout(timerID);
		timerID = setTimeout('filter()', 500);
	});
});

filter = function(){
	query = '?';
	$('input.filter').each(function() {
		el = $(this);
		if (el.is(":text") && el.val() != '') {
			query += el.attr('id') + '=' + el.val() + '&';
		}
	});
	$('select.filter').each(function() {
		el = $(this);
		query += el.attr('id') + '=' + $('#'+ el.attr('id') + " option:selected").val() + '&';
	});
	$.getJSON(
		url + query + 'sort=' + sort + '&page=' + page,
		function(d){
			$('#container').html(d.elements);
			$('#nav').html(d.nav);
		}
	);
};
