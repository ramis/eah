<?php

namespace Shop\Domain\Admin\Helper;

use Shop\Model;
use Shop\Filter;

class Auth
{
	/**
	 * @var Auth
	 */
	private static $instance;

	/**
	 * @var Model\Users;
	 */
	private $user;

	/**
	 * @access private
	 */
	private function __construct()
	{

	}

	/**
	 * @access private
	 */
	private function __clone()
	{

	}

	/**
	 * @return Auth
	 */
	public static function getInstance()
	{
		if (self::$instance === null) {
			if (isset($_SESSION['auth']) && $_SESSION['auth'] instanceof Auth) {
				self::$instance = $_SESSION['auth'];
			} else {
				self::$instance = new Auth;
				$_SESSION['auth'] = self::$instance;
			}
		}

		return self::$instance;
	}

	/**
	 * @return Model\Users
	 */
	public function getUser()
	{
		return $this->user;
	}

	/**
	 * @return boolean
	 */
	public function isAuth()
	{
		return ($this->user instanceof Model\Users && $this->user->is_active);
	}

	/**
	 * @param array $vars
	 * @return boolean
	 */
	public function login($vars)
	{
		if (isset($vars['login']) && isset($vars['pass'])) {
			$filter = new Filter\Users();
			$filter
				->login($vars['login'])
				->pass(md5($vars['pass']))
				->active(true);
			$this->user = Model\Users::fetchOne($filter);
		}
		return $this->isAuth();
	}

	/**
	 * @return void
	 */
	public function logout()
	{
		unset ($this->user);
	}

}