<?php

namespace Shop\Domain\Admin\Helper;

use Slim\Middleware;

class CheckAuth extends Middleware
{
	public function call()
	{

		$auth = Auth::getInstance();

		if($this->app->request->getPathInfo() !== '/login/' && $auth->isAuth() === false){
			$this->app->redirect($this->app->urlFor('login'), 301);
		}

		// Run inner middleware and application
		$this->next->call();

	}

}