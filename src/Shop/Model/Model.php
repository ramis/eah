<?php
namespace Shop\Model;

use ActiveRecord\RecordNotFound;
use Shop\Filter\Filter;

class Model extends \ActiveRecord\Model
{

	/**
	 * Выбрать записи по фильтру
	 *
	 * @param Filter $filter
	 * @return array
	 */
	public static function fetch(Filter $filter)
	{
		$query = array();
		$filter->applyFilter($query);

		return (empty($query) ? self::all() : self::all($query));
	}

	/**
	 * Выбрать 1 запись по фильтру
	 *
	 * @param Filter $filter
	 * @return Model The first matched record or null if not found
	 */
	public static function fetchOne(Filter $filter)
	{
		$query = array();
		$filter->applyFilter($query);

		return (empty($query) ? self::first() : self::first($query));
	}

	/**
	 * Кол по фильтру
	 *
	 * @param Filter $filter
	 * @return integer
	 */
	public static function fetchCount(Filter $filter)
	{
		$query = array();
		$filter->applyFilter($query);

		return parent::count($query);
	}

	/**
	 * Объект по pk
	 *
	 * @param $id
	 * @return mixed|null
	 */
	public static function fetchById($id)
	{
		try {
			return parent::find((int)$id);
		} catch (RecordNotFound $r) {
			return null;
		}
	}

	/**
	 * Имя класса
	 *
	 * @return string
	 */
	public function getClassName()
	{
		$class = explode('\\', get_called_class());
		return $class[count($class) - 1];
	}

	/**
	 * Алиасы для рассылок
	 *
	 * @return array
	 */
	public function getSendAlias()
	{
		return array('user', 'admin');
	}

//	public function __call($method, $args)
//	{
//		$pref = preg_replace('/^([a-z0-9]+)([A-Z]).+/', '$1', $method);
//
//		$method = trim($method, $pref);
//
//		foreach($this->attributes() as $key=>$value){
//			$keys[$key] = preg_replace('/^(dt_|d_)(.*)?/', '$2', $key);
//		}
//
//		preg_match_all('/([A-Z][a-z\d]*)/', $method, $match,  PREG_PATTERN_ORDER);
//
//		switch($pref){
//			case 'is':
//				$value = $pref .'_'. strtolower(implode('_', $match[0]));
//				return (isset($keys[$value])) ? $this->$value : '';
//				break;
//			case 'get':
//				$value = strtolower(implode('_', $match[0]));
//				return (isset($keys[$value])) ? $this->$value : '';
//				break;
//			default:
//				return;
//		}
//	}
}