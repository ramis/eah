<?php
namespace Shop\Model;

use Shop\Model\Behavior\SendMail;
use Shop\Model\Implement\SendMailInt;

class Subscription extends Model implements SendMailInt
{
	use SendMail;

	static $table_name = 'subscription';
	static $primary_key = 'id';

	/**
	 * Получить письмо по шаблону
	 *
	 * @param Template $tpl
	 * @return array
	 */
	public function getMessageTpl(Template $tpl)
	{

		$email = array(
			'message' => '',
			'subject' => ''
		);
		$data = array();

		$data['o_number'] = $this->id;
		$data['o_date'] = $this->create_time->format('d.m.Y');
		$data['o_fio'] = $this->fio;
		$data['o_telephone'] = $this->telephone;
		$data['o_email'] = $this->email;
		$data['o_address'] = $this->address;
		$data['o_shipping'] = $this->shipping_title;
		$data['o_comment'] = $this->comment;
		$data['o_total'] = $this->total;
		$data['o_total_full'] = $this->total + $this->shipping_cost;

		$data['products'] = array();
		foreach($this->getProducts() as $p){
			$data['products'][] = array(
				'title' => $p->product_title,
				'article' => $p->product_article,
				'code' => $p->product_code,
				'quantity' => $p->quantity,
				'price' => $p->price,
				'total' => $p->total,
			);
		}

		$loader = new \Twig_Loader_Array(array(
			'subject.html' => $tpl->subject,
			'message.html' => $tpl->message,
		));
		$twig = new \Twig_Environment($loader);
		$email['message'] = preg_replace('/(\r|\n)/', '<br/>',$twig->render('message.html', $data));
		$email['subject'] = $twig->render('subject.html', $data);
		return $email;
	}

}