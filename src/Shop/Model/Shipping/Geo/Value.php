<?php
namespace Shop\Model\Shipping\Geo;

use Shop\Model\Model;
use Shop\Model\Shipping;
use Shop\Model\Geo;

class Value extends Model
{
	static $table_name = 'shipping_geo_value';
	static $primary_key = 'id';

	/**
	 * @var Shipping
	 */
	private $shipping;

	/**
	 * @var Geo
	 */
	private $geo;

	/**
	 * Geo
	 *
	 * @return null|Geo
	 */
	public function getGeo()
	{
		if ($this->geo === null && $this->geo_id !== null) {
			$this->geo = Geo::fetchById($this->geo_id);
		}
		return $this->geo;
	}

	/**
	 * Shipping
	 *
	 * @return null|Shipping
	 */
	public function getShipping()
	{
		if ($this->shipping === null && $this->shipping_id !== null) {
			$this->shipping = Shipping::fetchById($this->shipping_id);
		}
		return $this->shipping;
	}

}