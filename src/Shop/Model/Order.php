<?php
namespace Shop\Model;

use Shop\Common\Exception;
use Shop\Model\Behavior\SendMail;
use Shop\Model\Implement\SendMailInt;
use Shop\Model\Order\History;
use Shop\Model\Order\Product;

class Order extends Model implements SendMailInt
{
	use SendMail;

	static $table_name = 'order';
	static $primary_key = 'id';

	static $validates_presence_of = array(
		array('fio', 'message' => 'Поле "Ваше имя" не может быть пустым'),
		array('telephone', 'message' => 'Поле "Телефон" не может быть пустым'),
		array('email', 'message' => 'Поле "Эл почта" не может быть пустым'),
		array('address', 'message' => 'Поле "Адрес" не может быть пустым'),
		array('shipping_geo_value_id', 'message' => 'Поле "Доставка" не может быть пустым'),
	);

	/**
	 * @var Order\Status
	 */
	private $order_status;

	/**
	 * @var array of Order\Product
	 */
	private $products;

	/**
	 * @return Order\Status|null
	 * @throws \ActiveRecord\RecordNotFound
	 */
	public function getStatus()
	{
		if ($this->order_status === null && $this->order_status_id !== null) {
			$this->order_status = Order\Status::fetchById($this->order_status_id);
		}
		return $this->order_status;
	}

	/**
	 * Продукты в заказе
	 *
	 * @return array Order\Product
	 */
	public function getProducts()
	{
		if ($this->products === null) {
			$filter = new \Shop\Filter\Order\Product();
			$filter->order($this);
			$this->products = Product::fetch($filter);
		}
		return $this->products;
	}

	/**
	 * Продукты в заказе
	 *
	 * @param array Order\Product
	 */
	public function setProducts(array $products)
	{
		$this->products = $products;
	}

	/**
	 * Сохраняем
	 *
	 * @param bool $validate = true
	 * @return bool
	 */
	public function save($validate = true)
	{
		$connection = self::connection();

		$connection->transaction();
		try {
			if (parent::save($validate) === false) {
				throw new Exception ('Not save order');
			}

			foreach ($this->getProducts() as $product) {
				$product->order_id = $this->id;
				$product->save();
			}

			$history = new History();
			$history->order_id = $this->id;
			$history->order_status_id = 1;
			$history->comment = '';
			$history->create_time = date('Y-m-d H:i:s');
			$history->save();

			$this->sendMail();

			$connection->commit();

			return true;
		} catch (Exception $e) {
			$connection->rollback();
			return false;
		}
	}

	/**
	 * Получить письмо по шаблону
	 *
	 * @param Template $tpl
	 * @return array
	 */
	public function getMessageTpl(Template $tpl)
	{

		$email = array(
			'message' => '',
			'subject' => ''
		);
		$data = array();

		$data['o_number'] = $this->id;
		$data['o_date'] = $this->create_time->format('d.m.Y');
		$data['o_fio'] = $this->fio;
		$data['o_telephone'] = $this->telephone;
		$data['o_email'] = $this->email;
		$data['o_address'] = $this->address;
		$data['o_shipping'] = $this->shipping_title;
		$data['o_comment'] = $this->comment;
		$data['o_total'] = $this->total;
		$data['o_total_full'] = $this->total + $this->shipping_cost;

		$data['products'] = array();
		foreach ($this->getProducts() as $p) {
			$data['products'][] = array(
				'title' => $p->product_title,
				'article' => $p->product_article,
				'code' => $p->product_code,
				'quantity' => $p->quantity,
				'price' => $p->price,
				'total' => $p->total,
			);
		}

		$loader = new \Twig_Loader_Array(array(
			'subject.html' => $tpl->subject,
			'message.html' => $tpl->message,
		));
		$twig = new \Twig_Environment($loader);
		$email['message'] = preg_replace('/(\r|\n)/', '<br/>', $twig->render('message.html', $data));
		$email['subject'] = $twig->render('subject.html', $data);
		return $email;
	}

}