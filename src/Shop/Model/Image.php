<?php
namespace Shop\Model;

use Shop\Common\ImagesStorage;

class Image extends Model
{
	static $table_name = 'image';
	static $primary_key = 'id';

	const PARENT_CATEGORY = 'CATEGORY';
	const PARENT_PRODUCT = 'PRODUCT';
	const PARENT_BANNERS = 'BANNERS';

	/**
	 * @var string
	 */
	public $tmp_name;

	/**
	 *
	 * @param int $width
	 * @param int $height
	 * @return string
	 */
	public function getHttpImage($width, $height)
	{
		if(ImagesStorage::imageCreateCache($this->hash, $width, $height)){
			$cache = $this->getCache();
			if(!isset($cache[$width . 'x' . $height])){
				$cache[$width . 'x' . $height] = 1;
				$this->cache = serialize($cache);
				$this->change();
			}
			return HTTP_IMAGE_CACHE . $width . 'x' . $height . '/' . $this->hash;
		}
		return '';

	}

	/**
	 * Сохраняем
	 *
	 * @return boolean
	 */
	public function save($validate = true)
	{
		//Сохраняем картинку в хранилище
		$hash = ImagesStorage::set($this->tmp_name);

		//Не смогли сохранить
		if ($hash === false) {
			return false;
		}
		$this->hash = $hash;

		//Сохраняем запись в бд
		$is_save = parent::save();

		//Не смогли сохранить, удаляем картинку из хранилища
		if ($is_save === false) {
			ImagesStorage::delete($this->hash);
		}

		return $is_save;
	}

	/**
	 * @return bool
	 */
	public function change()
	{
		return parent::save();
	}


	/**
	 * Удаляем
	 *
	 * @return bool
	 * @throws \ActiveRecord\ActiveRecordException
	 */
	public function delete()
	{
		//Удалаяем файл
		ImagesStorage::delete($this->hash);

		return parent::delete();
	}

	/**
	 * @return array
	 */
	private function getCache()
	{
		return unserialize($this->cache);
	}
}