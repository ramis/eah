<?php
namespace Shop\Model;

use Shop\Filter\Urls as FilterUrl;

class Information extends Model
{
	static $table_name = 'information';
	static $primary_key = 'id';

	static $validates_presence_of = array(
		array('title', 'message' => 'Поле "Название" не может быть пустым'),
		array('url', 'message' => 'Поле "Ссылка" не может быть пустым'),
	);

	/**
	 * Сохранить новость
	 *
	 * @param bool $validate
	 * @return bool
	 */
	public function save($validate = true)
	{
		$is_save = parent::save($validate);
		if ($is_save) {
			Urls::store('information', (int)$this->id, $this->url);
		}
		return $is_save;
	}

	public function delete()
	{
		$filter = new FilterUrl();
		$filter
			->parentTable('category')
			->parentId($this->id);
		$url = Urls::fetchOne($filter);
		$url->delete();

		parent::delete();

	}

	/**
	 * Validate
	 */
	public function validate()
	{
		if ($this->description === null) {
			$this->description = '';
		}

		$url = Urls::find_by_query($this->url);

		if ($url !== NUll && ($url->parent_table === 'information' && $url->parent_id !== $this->id)) {
			$this->errors->add("url", '"Ссылка" должна быть уникальной для сайта.');
		}
	}

}