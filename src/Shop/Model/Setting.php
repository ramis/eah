<?php
namespace Shop\Model;

class Setting extends Model
{
	static $table_name = 'setting';
	static $primary_key = 'id';

	/**
	 * Get value by key
	 *
	 * @static
	 * @param string $key
	 * @return string
	 */
	public static function getValue($key)
	{
		$value = parent::find_by_key($key);
		return $value ? $value->value : '';
	}

}