<?php
namespace Shop\Model;

use Shop\Model\Users\Group;

class Users extends Model
{
	static $table_name = 'users';
	static $primary_key = 'id';

	/**
	 * @var Group
	 */
	private $users_group;

	/**
	 * Группа пользователя
	 *
	 * @return mixed|null|Group
	 */
	public function getUsersGroup()
	{
		if($this->users_group === null && $this->users_group_id !== null){
			$this->users_group = Group::fetchById((int)$this->users_group_id);
		}
		return $this->users_group;
	}

	/**
	 * Группа пользователя
	 *
	 * @param Group $group
	 */
	public function setUsersGroup(Group $group)
	{
		$this->users_group = $group;
	}

}