<?php
namespace Shop\Model;

class Banners extends Model
{
	const BANNER_TYPE_BIG = 1;
	const BANNER_TYPE_SMALL = 2;
	const BANNER_TYPE_LEFT = 3;

	static $table_name = 'banners';
	static $primary_key = 'id';
	static $type_titles = array(
		self::BANNER_TYPE_BIG => 'Большой баннер',
		self::BANNER_TYPE_SMALL => 'Маленький баннер',
		self::BANNER_TYPE_LEFT => 'Баннер в левой колонке');

	/**
	 * Image category
	 */
	public function getImage()
	{
		return Image::fetchById($this->image_id);
	}

	/**
	 * @return bool
	 * @throws \ActiveRecord\ActiveRecordException
	 */
	public function delete()
	{
		$image = $this->getImage();
		$is_del = parent::delete();
		if ($is_del && $image instanceof Image) {
			$image->delete();
		}
		return $is_del;
	}


	/**
	 * @return string
	 */
	public function getTypeTitle()
	{
		return isset(self::$type_titles[$this->type]) ? self::$type_titles[$this->type] : '';
	}

	/**
	 * @return array
	 */
	public function getTypeTitles()
	{
		return self::$type_titles;
	}

}