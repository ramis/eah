<?php
namespace Shop\Model;

use Shop\Filter\Urls as FilterUrls;

class Urls extends Model
{
	static $table_name = 'urls';
	static $primary_key = 'id';

	/**
	 * Проверяем если такой урл, если есть то обновляем его, если нет то добавляем новый
	 *
	 * @param string $parent_table
	 * @param int $parent_id
	 * @param string $query
	 * @return boolean
	 */
	public static function store($parent_table, $parent_id, $query)
	{
		$filter = new FilterUrls();
		$filter
			->parentTable($parent_table)
			->parentId($parent_id);

		$url = self::fetchOne($filter);
		if ($url === null) {
			$url = new Urls();
		}

		$url->parent_id = $parent_id;
		$url->parent_table = $parent_table;
		$url->query = $query;
		return $url->save();
	}
}