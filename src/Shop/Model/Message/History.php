<?php
namespace Shop\Model\Message;

use Shop\Model\Model;

class History extends Model
{
	static $table_name = 'message_history';
	static $primary_key = 'id';

}