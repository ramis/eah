<?php
namespace Shop\Model\Message;

use Shop\Model\Model;

class Status extends Model
{
	static $table_name = 'message_status';
	static $primary_key = 'id';

}