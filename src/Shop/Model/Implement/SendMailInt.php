<?php
namespace Shop\Model\Implement;

use Shop\Model\Template;

interface SendMailInt
{
	/**
	 * Заполнить маcсив из шаблона
	 *
	 * @params Template $tpl
	 * @return array('subject' => '', 'message' => '')
	 */
	public function getMessageTpl(Template $tpl);

	/**
	 * Отправить письмо
	 *
	 * @return mixed
	 */
	public function send();
}