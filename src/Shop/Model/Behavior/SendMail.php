<?php
namespace Shop\Model\Behavior;

use Shop\Common\Mail;
use Shop\Model\Implement\SendMailInt;
use Shop\Model\Setting;
use Shop\Model\Template;

trait SendMail
{
	/**
	 * Отправка уведомления
	 */
	public function send()
	{
		if(!($this instanceof SendMailInt)){
			return;
		}

		//Выбираем алиасы
		$aliases = array();
		foreach($this->getSendAlias() as $a){
			$aliases[] = strtolower($this->getClassName()) . '_' . $a;
		}

		//Нашли нужные алиасы
		if(!empty($aliases)){
			//Выбираем шаблоны
			$templates = array();
			foreach($aliases as $a){
				$template = Template::find_by_alias($a);
				if($template instanceof Template){
					$templates[] = $template;
				}
			}

			//Есть шаблоны
			if(!empty($templates)){
				foreach($templates as $tpl){
					$email = $this->getMessageTpl($tpl);

					$mail = new Mail();

					if(strpos('user', $tpl->alias) !== -1){
						$mail->setTo($this->email);
					}else{
						$setting = Setting::find_by_key('email');
						if($setting){
							$mail->setTo($setting->value);
						}
					}

					$setting = Setting::find_by_key('from');
					if($setting){
						$mail->setFrom($setting->value);
					}
					$setting = Setting::find_by_key('sender');
					if($setting){
						$mail->setSender($setting->value);
					}

					$mail->setSubject($email['subject']);
					$mail->setMessage($email['message']);

					$mail->send();
				}
			}
		}
	}

}