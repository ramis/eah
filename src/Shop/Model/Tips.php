<?php
namespace Shop\Model;

class Tips extends Model
{
	static $table_name = 'tips';
	static $primary_key = 'id';

	/**
	 * @var Product
	 */
	private $product;

	/**
	 * @return null|Product
	 */
	public function getProduct()
	{
		if($this->product === null && $this->product_id !== null){
			$this->product = Product::fetchById($this->product_id);
		}
		return $this->product;
	}
}