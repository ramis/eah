<?php
namespace Shop\Model\Order;

use Shop\Model\Model;

class History extends Model
{
	static $table_name = 'order_history';
	static $primary_key = 'id';

	private $order_status;

	public function getOrderStatus()
	{
		if ($this->order_status === null && $this->order_status_id !== null) {
			$this->order_status = Status::fetchById($this->order_status_id);
		}
		return $this->order_status;
	}
}