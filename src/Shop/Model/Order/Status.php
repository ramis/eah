<?php
namespace Shop\Model\Order;

use Shop\Model\Model;

class Status extends Model
{
	static $table_name = 'order_status';
	static $primary_key = 'id';

}