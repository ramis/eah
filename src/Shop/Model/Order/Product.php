<?php
namespace Shop\Model\Order;

use Shop\Model\Model;

class Product extends Model
{
	static $table_name = 'order_product';
	static $primary_key = 'id';

}