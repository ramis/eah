<?php
namespace Shop\Model\Users;

use Shop\Model\Model;

class Group extends Model
{
	static $table_name = 'users_group';
	static $primary_key = 'id';

	/**
	 * @var Array
	 */
	private $roles;

	/**
	 * Роли
	 *
	 * @return Array
	 */
	public function getRoles()
	{
		$this->roles = json_decode($this->role, true);
		return (json_last_error() !== 'No error') ? $this->roles : array();
	}

}