<?php
namespace Shop\Model\Catalog;

use Shop\Common\Exception;
use Shop\Filter\Catalog\Product\Attribute\Value as FilterValue;
use Shop\Filter\Catalog\Category as FilterCategory;
use Shop\Model\Model;
use Shop\Model\Image;
use Shop\Model\Catalog\Product\Attribute\Value as ModelValue;
use Shop\Model\Catalog\Product\Category as ProductCategory;

class Product extends Model
{
	static $table_name = 'catalog_product';
	static $primary_key = 'id';

	static $validates_presence_of = array(
		array('title', 'message' => 'Поле "Название" не может быть пустым'),
	);

	/**
	 * @var Category
	 */
	private $main_catalog_category;

	/**
	 * @var array
	 */
	private $attributes;

	/**
	 * @var array
	 */
	private $catalog_categories;

	/**
	 * Главная категория
	 *
	 * @return mixed|null|Category
	 * @throws \ActiveRecord\RecordNotFound
	 */
	public function getMainCatalogCategory()
	{
		if ($this->main_catalog_category === null) {
			$filter = new FilterCategory();
			$filter
				->main(true)
				->product($this);
			$this->main_catalog_category = Category::fetchOne($filter);

		}
		return $this->main_catalog_category;
	}

	/**
	 * Image
	 */
	public function getMainImage()
	{
		return Image::fetchById($this->main_image_id);
	}

	/**
	 * Категории продукта
	 *
	 * @return array
	 */
	public function getCatalogCategories()
	{
		if ($this->catalog_categories === null && $this->id !== null) {
			$filter = new FilterCategory();
			$filter
				->product($this);
			$this->catalog_categories = Category::fetch($filter);
		}

		return ($this->catalog_categories) ?: array();
	}

	/**
	 * @param array $catalog_categories
	 */
	public function setCatalogCategories(array $catalog_categories)
	{
		$this->catalog_categories = $catalog_categories;
	}

	/**
	 * Аттрибуты продукта
	 *
	 * @return array
	 */
	public function getAttributes()
	{
		if ($this->attributes === null) {
			$filter = new FilterValue();
			$filter->product($this);
			$this->attributes = ModelValue::fetch($filter);
		}
		return $this->attributes;
	}

	/**
	 * Аттрибуты продукта
	 *
	 * @param array $attributes
	 */
	public function setAttributes(array $attributes)
	{
		$this->attributes = $attributes;
	}

	/**
	 * Сохраняем
	 *
	 * @param bool $validate = true
	 * @return bool
	 */
	public function save($validate = true)
	{
		$connection = self::connection();

		$connection->transaction();
		try{
			if(parent::save($validate) === false) {
				throw new Exception ('Not save catalog product');
			}

			//Сохраняем аттрибуты
			$values_id = array();
			foreach ($this->attributes as $value) {
				$value->catalog_product_id = $this->id;
				if ($value->save()) {
					$values_id[] = (int)$value->id;
				}else{
					throw new Exception ('Not save catalog product attribute');
				}
			}

			if (empty($values_id) === false) {
				$conditions = array('catalog_product_id = ? AND id NOT IN (?)', (int)$this->id, $values_id);
			} else {
				$conditions = array('catalog_product_id = ?', (int)$this->id);
			}

			ModelValue::delete_all(array('conditions' => $conditions));

			//Сохраняем категории
			$categories_id = array();
			foreach ($this->catalog_categories as $category) {
				$category->catalog_product_id = $this->id;
				if ($category->save()) {
					$categories_id[] = (int)$category->id;
				}else{
					throw new Exception ('Not save catalog product category');
				}
			}

			if (empty($categories_id) === false) {
				$conditions = array('catalog_product_id = ? AND id NOT IN (?)', (int)$this->id, $categories_id);
			} else {
				$conditions = array('catalog_product_id = ?', (int)$this->id);
			}

			ProductCategory::delete_all(array('conditions' => $conditions));

			$connection->commit();

			return true;
		}catch (Exception $e){
			$connection->rollback();
			return false;
		}

	}

	/**
	 * Change
	 */
	public function change()
	{
		parent::save();
	}

	/**
	 * Продукты которые покупают с данным
	 *
	 * @param integer $product_id
	 * @return array
	 */
	public static function productBuy($product_id)
	{
		return Product::find_by_sql('select
				catalog_product_id,
				count(catalog_product_id) as cnt
			from
				order_product
			where catalog_product_id<>' . (int)$product_id . ' and order_id in (
				select
					order_id
				from order_product
				where catalog_product_id = ' . (int)$product_id . '
			)
			group by catalog_product_id
			order by cnt desc
			limit 5');
	}

	/**
	 * Пересчет dt_ полей для отзывов
	 */
	public function calcReview()
	{
		$connection = self::connection();

		$connection->transaction();
		try {

			$review = Product::find_by_sql('select
				count(id) as cnt,
				sum(rating) as rating
			from
				catalog_product_review
			where catalog_product_id=' . (int)$this->id . ' and is_publish = 1;');
			$review = $review[0];

			$this->dt_review_count = (int)$review->cnt;
			$this->dt_review_rating = $this->dt_review_count === 0 ? $this->dt_review_count :
				(int)round(((int)$review->rating / (int)$review->cnt));

			$this->change();

			$connection->commit();
		}catch (Exception $e){
			$connection->rollback();
		}

	}

}