<?php
namespace Shop\Model\Catalog;

use Shop\Common\Exception;
use Shop\Common\Hierarchy;
use Shop\Model\Catalog\Category\Dependence;
use Shop\Model\Model;
use Shop\Model\Urls;
use Shop\Model\Image;
use Shop\Filter\Urls as FilterUrl;
use Shop\Filter\Catalog\Category as FilterCategory;

class Category extends Model
{
	const BRAND_ID = 1;
	const CATEGORY_ID = 2;

	static $table_name = 'catalog_category';
	static $primary_key = 'id';

	static $validates_presence_of = array(
		array('title', 'message' => 'Поле "Название" не может быть пустым'),
		array('url', 'message' => 'Поле "Ссылка" не может быть пустым'),
	);

	/**
	 * @var array of Category
	 */
	private $parents;

	/**
	 * @var array of Category
	 */
	private $childs;

	public static function getCatalogCategories()
	{
		$categories = array();

		$connection = self::connection();
		$builder = new \ActiveRecord\SQLBuilder($connection, "catalog_category as c");
		$joins = "left join catalog_category_dependence as d1 on d1.catalog_category_child_id = c.id " .
				 "left join catalog_category_dependence as d on d.catalog_category_parent_id = c.id " .
				 "left join catalog_category as ch on d.catalog_category_child_id = ch.id ";
		$builder->select(" c.*, ch.id as child_id, d1.id as is_parent ");
		$builder->joins($joins);
		$builder->order("c.sort, d.sort");

		$result = Category::find_by_sql($builder->to_s());
		foreach ($result as $category){
			$child_id = $category->child_id ? (int)$category->child_id : null;
			if(isset($categories[$category->id])){
				if($child_id && !in_array($child_id, $categories[$category->id]['child'])){
					$categories[$category->id]['child'][] = $child_id;
				}
			}else{
				$i = $category->getImage();
				$categories[$category->id] = array(
					'id' => $category->id,
					'title' => $category->title,
					'sort' => $category->sort,
					'hash' => ($i ? HTTP_IMAGE_BASE . $i->hash : ''),
					'is_recursion' => true,
					'is_parent' => (bool)$category->is_parent,
				);
				if($child_id){
					$categories[$category->id]['child'][] = $child_id;
				}
			}
		}

		return Hierarchy::graph($categories);

	}
	/**
	 * Сохраняем категорию
	 *
	 * @return boolean
	 */
	public function save($validate = true)
	{
		$connection = self::connection();

		$connection->transaction();
		try{
			$filter = new FilterCategory();
			$filter->sort('sort desc');
			$category = Category::fetchOne($filter);
			$this->sort = $category->sort + 1;

			if(parent::save($validate) === false) {
				throw new Exception ('Not save catalog category');
			}

			Urls::store('category', (int)$this->id, $this->url);

			//Сохраняем категории зависимости
			$values_id = array();
			foreach ($this->childs as $dependence) {
				$dependence->catalog_category_parent_id = $this->id;
				if ($dependence->save()) {
					$values_id[] = (int)$dependence->id;
				}else{
					throw new Exception ('Not save catalog category dependence');
				}
			}

			if (empty($values_id)) {
				$conditions = array('catalog_category_parent_id = ?', (int)$this->id);
			} else {
				$conditions = array('catalog_category_parent_id = ? AND id NOT IN (?)', (int)$this->id, $values_id);
			}

			Dependence::delete_all(array('conditions' => $conditions));

			$connection->commit();

			return true;
		}catch (Exception $e){
			$connection->rollback();
			return false;
		}

	}

	/**
	 * @return array|null
	 */
	public function getParents()
	{
		if($this->parents === null && $this->id !== null){
			$filter = new FilterCategory;
			$filter->parent($this);
			$this->parents = Category::fetch($filter);
		}

		return $this->parents;
	}

	/**
	 * @return array|null
	 */
	public function getChilds()
	{
		if($this->childs === null && $this->id !== null){
			$filter = new FilterCategory;
			$filter->child($this);
			$this->childs = Category::fetch($filter);
		}

		return $this->childs;
	}

	/**
	 * @param array $childs
	 */
	public function setChilds(array $childs)
	{
		$this->childs = $childs;
	}

	/**
	 * @return bool
	 * @throws \ActiveRecord\ActiveRecordException
	 */
	public function delete()
	{
		//url
		$filter = new FilterUrl();
		$filter
			->parentTable('category')
			->parentId($this->id);
		$url = Urls::fetchOne($filter);
		$url->delete();

		$image = $this->getImage();
		$is_del = parent::delete();
		if ($is_del && $image instanceof Image) {
			$image->delete();
		}
		return $is_del;
	}

	/**
	 * Image category
	 */
	public function getImage()
	{
		return Image::fetchById($this->image_id);
	}

	/**
	 * Validate
	 */
	public function validate()
	{
		if ($this->description === null) {
			$this->description = '';
		}

		$url = Urls::find_by_query($this->url);

		if ($url !== NUll && ($url->parent_table === 'category' && $url->parent_id !== $this->id)) {
			$this->errors->add("", "'Ссылка' должна быть уникальной для сайта.");
		}
	}

}