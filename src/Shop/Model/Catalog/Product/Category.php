<?php
namespace Shop\Model\Catalog\Product;

use Shop\Model\Model;

class Category extends Model
{
	static $table_name = 'catalog_product_category';
	static $primary_key = 'id';
}