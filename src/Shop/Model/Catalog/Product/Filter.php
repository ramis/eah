<?php
namespace Shop\Model\Catalog\Product;

use Shop\Model\Model;

class Filter extends Model
{
	static $table_name = 'catalog_product_filter';
	static $primary_key = 'id';

}