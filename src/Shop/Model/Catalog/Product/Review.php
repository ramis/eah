<?php
namespace Shop\Model\Catalog\Product;

use Shop\Common\Exception;
use Shop\Model\Model;
use Shop\Model\Catalog\Product;

class Review extends Model
{
	static $table_name = 'catalog_product_review';
	static $primary_key = 'id';

	/**
	 * @var Product
	 */
	private $product;

	/**
	 * @return null|Product
	 */
	public function getCatalogProduct()
	{
		if ($this->product === null && $this->catalog_product_id !== null) {
			$this->product = Product::fetchById($this->catalog_product_id);
		}
		return $this->product;
	}

	/**
	 * Save
	 *
	 * @param bool $validate
	 * @return bool
	 */
	public function save($validate = true)
	{
		if (parent::save(($validate))) {
			$this->getCatalogProduct()->calcReview();
		}

		return true;
	}

	/**
	 * Delete
	 *
	 * @return bool
	 */
	public function delete()
	{
		$connection = self::connection();
		$connection->transaction();
		try{
			$product = $this->getCatalogProduct();
			parent::delete();

			$product->calcReview();

			$connection->commit();
			return true;
		}catch (Exception $e){
			$connection->rollback();
			return false;
		}
	}
}