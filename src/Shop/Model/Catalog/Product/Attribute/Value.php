<?php
namespace Shop\Model\Catalog\Product\Attribute;

use Shop\Model\Model;
use Shop\Model\Catalog\Product\Attribute;

class Value extends Model
{
	static $table_name = 'catalog_product_attribute_value';
	static $primary_key = 'id';

	/**
	 * @return string
	 */
	public function getAttributeTitle()
	{
		$attribute = Attribute::fetchById($this->catalog_product_attribute_id);
		return $attribute ? $attribute->title : '';
	}
}