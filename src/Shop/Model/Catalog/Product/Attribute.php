<?php
namespace Shop\Model\Catalog\Product;

use Shop\Model\Model;

class Attribute extends Model
{
	static $table_name = 'catalog_product_attribute';
	static $primary_key = 'id';

	static $validates_presence_of = array(
		array('title', 'message' => 'Поле "Название" не может быть пустым')
	);


	/**
	 * Validate
	 */
	public function validate()
	{
		if ($this->sort === null) {
			$this->sort = 0;
		}
	}

}