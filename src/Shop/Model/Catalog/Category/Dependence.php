<?php
namespace Shop\Model\Catalog\Category;

use Shop\Model\Catalog\Category;
use Shop\Model\Model;

class Dependence extends Model
{
	static $table_name = 'catalog_category_dependence';
	static $primary_key = 'id';

	/**
	 * Validate
	 */
	public function validate()
	{
		if ($this->sort === null) {
			$this->sort = 0;
		}

		if(!(Category::fetchById($this->catalog_category_patent_id) instanceof Category)){
			$this->errors->add("", "Нет категории с id " . $this->catalog_category_patent_id);
		}

		if(!(Category::fetchById($this->catalog_category_child_id) instanceof Category)){
			$this->errors->add("", "Нет категории с id " . $this->catalog_category_child_id);
		}

	}

}