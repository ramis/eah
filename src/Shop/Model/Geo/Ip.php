<?php
namespace Shop\Model\Geo;

use Shop\Model\Geo;
use Shop\Filter\Geo as FilterGeo;
use Shop\Model\Model;
use Shop\Filter\Geo\Ip as FilterGeoIp;

class Ip extends Model
{
	static $table_name = 'geo_ip';
	static $primary_key = 'id';

	/**
	 * Определение гео по ip
	 *
	 * @param $ip
	 * @return int
	 */
	public static function getGeoIp($ip)
	{

		$geo_id = 0;
		$ip = sprintf('%u', ip2long($ip));
		$filter = new FilterGeoIp();
		$filter->ip($ip);
		$row = Ip::fetchOne($filter);

		if ($row) {
			$filter = new FilterGeo();
			$filter->alias($row->code);
			$geo = Geo::fetchOne($filter);
			$geo_id = (int)$geo->id;
		}

		if ($geo_id === 0) {
			$geo_id = 1;
		}

		return $geo_id;
	}

}