<?php
namespace Shop\Filter\Geo;

use Shop\Filter\Filter;
use Shop\Filter\BaseFilter;

class Ip extends BaseFilter implements Filter
{

	/**
	 * @var int
	 */
	private $ip;

	/**
	 * @param int $ip
	 * @return $this
	 */
	public function ip($ip)
	{
		$this->ip = $ip;
		return $this;
	}

	public function applyFilter(array &$query)
	{
		if ($this->ip !== null) {
			$this->addConditionsAnd($query, 'ip_start <= ?', $this->ip);
			$this->addConditionsAnd($query, 'ip_end >= ?', $this->ip);
		}

		parent::applyFilter($query);

	}
}