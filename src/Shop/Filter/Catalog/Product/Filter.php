<?php
namespace Shop\Filter\Catalog\Product;

use Shop\Filter\BaseFilter;
use Shop\Filter\Filter as ImpFilter;
use Shop\Model\Catalog\Product\Filter as ModelFilter;

class Filter extends BaseFilter implements ImpFilter
{

	/**
	 * @var boolean
	 */
	private $is_group;

	/**
	 * @var ModelFilter;
	 */
	private $parent;

	/**
	 * @param boolean $is_group
	 * @return $this
	 */
	public function group($is_group)
	{
		$this->is_group = $is_group;
		return $this;
	}

	/**
	 * @param ModelFilter $parent
	 * @return $this
	 */
	public function parent(ModelFilter $parent)
	{
		$this->parent = $parent;
		return $this;
	}

	public function applyFilter(array &$query)
	{

		parent::applyFilter($query);

		if ($this->is_group !== null) {
			$this->addConditionsAnd($query, 'is_group = ?', (int)$this->is_group);
		}

		if ($this->parent !== null) {
			$this->addConditionsAnd($query, 'parent_id = ?', $this->parent->id);
		}
	}
}