<?php
namespace Shop\Filter\Catalog\Product;

use Shop\Filter\Filter;
use Shop\Filter\BaseFilter;

class Attribute extends BaseFilter implements Filter
{

	public function applyFilter(array &$query)
	{
		parent::applyFilter($query);
	}
}