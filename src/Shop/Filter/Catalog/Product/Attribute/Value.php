<?php
namespace Shop\Filter\Catalog\Product\Attribute;

use Shop\Model\Catalog\Product;
use Shop\Model\Catalog\Product\Attribute;

use Shop\Filter\BaseFilter;
use Shop\Filter\Filter;

class Value extends BaseFilter implements Filter
{
	/**
	 * @var Product
	 */
	private $product;

	/**
	 * @var Attribute
	 */
	private $attribute;

	/**
	 * Продукт
	 *
	 * @param Product $product
	 * @return $this
	 */
	public function product(Product $product)
	{
		$this->product = $product;
		return $this;
	}
	/**
	 * Аттрибут
	 *
	 * @param Attribute $attribute
	 * @return $this
	 */
	public function attribute(Attribute $attribute)
	{
		$this->attribute = $attribute;
		return $this;
	}

	public function applyFilter(array &$query)
	{
		parent::applyFilter($query);

		if ($this->product !== null) {
			$this->addConditionsAnd($query, 'catalog_product_id = ? ', (int)$this->product->id);
		}
		if ($this->attribute !== null) {
			$this->addConditionsAnd($query, 'catalog_product_attribute_id = ? ', (int)$this->attribute->id);
		}
	}

}