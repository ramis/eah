<?php
namespace Shop\Filter\Catalog\Product;

use Shop\Filter\BaseFilter;
use Shop\Filter\Filter;
use Shop\Model\Catalog\Product;
use Shop\Model\Catalog\Category as ModelCategory;

class Category extends BaseFilter implements Filter
{

	/**
	 * @var Product
	 */
	private $product;

	/**
	 * @var Category
	 */
	private $category;

	/**
	 * @param Product $product
	 * @return $this
	 */
	public function product (Product $product)
	{
		$this->product = $product;
		return $this;
	}

	/**
	 * @param ModelCategory $category
	 * @return $this
	 */
	public function category (ModelCategory $category)
	{
		$this->category = $category;
		return $this;
	}

	/**
	 * Применить фильтр
	 *
	 * @param array $query
	 */
	public function applyFilter(array &$query)
	{
		parent::applyFilter($query);

		if($this->product !== null){
			$this->addConditionsAnd($query, 'catalog_product_id = ?', (int)$this->product->id);
		}
		if($this->category !== null){
			$this->addConditionsAnd($query, 'catalog_category_id = ?', (int)$this->category->id);
		}
	}

}