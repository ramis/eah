<?php
namespace Shop\Filter\Catalog\Product;

use Shop\Filter\BaseFilter;
use Shop\Filter\Filter;
use Shop\Model\Catalog\Product;

class Review extends BaseFilter implements Filter
{
	/**
	 * @var Product
	 */
	private $product;

	/**
	 * @param Product $product
	 * @return $this
	 */
	public function product(Product $product)
	{
		$this->product = $product;
		return $this;
	}

	/**
	 * @param array $query
	 */
	public function applyFilter(array &$query)
	{
		parent::applyFilter($query);

		if($this->product !== null){
			$this->addConditionsAnd($query, 'catalog_product_id = ? ', $this->product->id);
		}
	}
}