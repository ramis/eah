<?php
namespace Shop\Filter\Catalog\Category;

use Shop\Model\Catalog\Category as ModelCategory;
use Shop\Filter\BaseFilter;
use Shop\Filter\Filter;

class Dependence extends BaseFilter implements Filter
{

	/**
	 * @var ModelCategory
	 */
	private $parent;

	/**
	 * @var ModelCategory
	 */
	private $child;

	/**
	 * @param ModelCategory $parent
	 * @return $this
	 */
	public function parent (ModelCategory $parent){
		$this->parent = $parent;
		return $this;
	}

	/**
	 * @param ModelCategory $child
	 * @return $this
	 */
	public function child (ModelCategory $child){
		$this->child = $child;
		return $this;
	}

	public function applyFilter(array &$query)
	{

		if($this->parent !== null){
			$this->addConditionsAnd($query, 'catalog_category_parent_id = ?', $this->parent->id);
		}

		if($this->child !== null){
			$this->addConditionsAnd($query, 'catalog_category_child_id = ?', $this->child->id);
		}
	}

}