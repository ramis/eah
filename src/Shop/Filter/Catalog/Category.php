<?php
namespace Shop\Filter\Catalog;

use Shop\Model\Catalog\Category as ModelCategory;
use Shop\Model\Catalog\Product as ModelProduct;

use Shop\Filter\BaseFilter;
use Shop\Filter\Filter;

class Category extends BaseFilter implements Filter
{
	/**
	 * @var boolean
	 */
	private $is_main;

	/**
	 * @var ModelProduct
	 */
	private $product;

	/**
	 * @var ModelCategory
	 */
	private $parent;

	/**
	 * @var ModelCategory
	 */
	private $child;

	/**
	 * Parents
	 *
	 * @param ModelCategory $category
	 * @return $this
	 */
	public function parent(ModelCategory $category)
	{
		$this->parent = $category;
		return $this;
	}

	/**
	 * Childs
	 *
	 * @param ModelCategory $category
	 * @return $this
	 */
	public function child(ModelCategory $category)
	{
		$this->child = $category;
		return $this;
	}

	/**
	 * Главная категория для продукта
	 *
	 * @param boolean $is_main
	 * @return $this
	 */
	public function main($is_main)
	{
		$this->main = $is_main;
		return $this;
	}

	/**
	 * Категории продукта
	 *
	 * @param ModelProduct $product
	 * @return $this
	 */
	public function product(ModelProduct $product)
	{
		$this->product = $product;
		return $this;
	}

	public function applyFilter(array &$query)
	{

		parent::applyFilter($query);

		if ($this->product !== null) {
			$query['joins'][] = 'INNER JOIN catalog_product_category as cpc ON
							 catalog_category.id=cpc.catalog_category_id AND
							 cpc.catalog_product_id = ' . $this->product->id;
			if($this->is_main !== null) {
				$this->addConditionsAnd($query, 'cpc.is_main = ? ', (int)$this->is_main);
			}
		}

		if ($this->parent !== null) {
			$query['joins'][] = 'INNER JOIN catalog_category_dependence as ccd ON
							 catalog_category.id=ccd.catalog_category_parent_id AND
							 ccd.catalog_category_child_id = ' . $this->parent->id;
			$query['order'] = 'ccd.sort';
		}

		if ($this->child !== null) {
			$query['joins'][] = 'INNER JOIN catalog_category_dependence as ccd ON
							 catalog_category.id=ccd.catalog_category_child_id AND
							 ccd.catalog_category_parent_id = ' . $this->child->id;
			$query['order'] = 'ccd.sort';
		}

	}

}