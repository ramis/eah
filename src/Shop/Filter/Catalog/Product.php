<?php
namespace Shop\Filter\Catalog;

use Shop\Model\Catalog\Category as ModelCategory;
use Shop\Model\Catalog\Product\Filter as ModelFilter;

use Shop\Filter\BaseFilter;
use Shop\Filter\Filter;

class Product extends BaseFilter implements Filter
{
	/**
	 * @var ModelCategory
	 */
	private $category;

	/**
	 * @var ModelFilter
	 */
	private $filter;

	/**
	 * @var string
	 */
	private $title;

	/**
	 * @var string
	 */
	private $code;

	/**
	 * @var string
	 */
	private $article;

	/**
	 * @var int
	 */
	private $price;

	/**
	 * @var int
	 */
	private $special_price;

	/**
	 * @var int
	 */
	private $code_more;

	/**
	 * По категории
	 *
	 * @param ModelCategory $category
	 * @return $this
	 */
	public function category(ModelCategory $category)
	{
		$this->category = $category;
		return $this;
	}

	/**
	 * По фильтру
	 *
	 * @param ModelFilter $filter
	 * @return $this
	 */
	public function productFilter(ModelFilter $filter)
	{
		$this->filter = $filter;
		return $this;
	}

	/**
	 * По названию like
	 *
	 * @param string $title
	 * @return $this
	 */
	public function title($title)
	{
		$this->title = $title;
		return $this;
	}

	/**
	 * По коду like
	 *
	 * @param string $code
	 * @return $this
	 */
	public function code($code)
	{
		$this->code = $code;
		return $this;
	}

	/**
	 * Код больше
	 *
	 * @param int $code_more
	 */
	public function codeMore($code_more)
	{
		$this->code_more = $code_more;
	}

	/**
	 * По артиклу like
	 *
	 * @param string $article
	 * @return $this
	 */
	public function article($article)
	{
		$this->article = $article;
		return $this;
	}

	/**
	 * По цене >
	 *
	 * @param integer $price
	 * @return $this
	 */
	public function price($price)
	{
		$this->price = $price;
		return $this;
	}

	/**
	 * По спец цене >
	 *
	 * @param integer $special_price
	 * @return $this
	 */
	public function specialPrice($special_price)
	{
		$this->special_price = $special_price;
		return $this;
	}

	/**
	 * Применить фильтр к запросу
	 *
	 * @param array $query
	 * @return void
	 */
	public function applyFilter(array &$query)
	{
		parent::applyFilter($query);

		if ($this->category !== null) {
			$query['joins'][] = 'INNER JOIN catalog_product_category as cpc ON
								 catalog_product.id=cpc.catalog_product_id AND
								 cpc.catalog_category_id = ' . $this->category->id;
		}
		if ($this->filter !== null) {
		}

		if ($this->title !== null) {
			$this->addConditionsAnd($query, 'title like ?', '%' . $this->title . '%');
		}
		if ($this->code !== null) {
			$this->addConditionsAnd($query, 'code like ?', '%' . $this->code . '%');
		}
		if ($this->code_more !== null) {
			$this->addConditionsAnd($query, 'code >', (int)$this->code_more);
		}
		if ($this->article !== null) {
			$this->addConditionsAnd($query, 'article like ?', '%' . $this->article . '%');
		}
		if ($this->price !== null) {
			$this->addConditionsAnd($query, 'price >= ?', (int)$this->price);
		}
		if ($this->special_price !== null) {
			$this->addConditionsAnd($query, 'special_price >= ?', (int)$this->special_price);
		}
	}
}