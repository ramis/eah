<?php
namespace Shop\Filter;

class Order extends BaseFilter implements Filter
{
	/**
	 * @var string
	 */
	private $fio;

	/**
	 * @var int
	 */
	private $number;

	/**
	 * @var int
	 */
	private $price;

	/**
	 * @var string
	 */
	private $start;

	/**
	 * @var string
	 */
	private $end;
	/**
	 * Price
	 *
	 * @param int $price
	 * @return $this
	 */
	public function price ($price)
	{
		$this->price = $price;
		return $this;
	}

	/**
	 * Fio
	 *
	 * @param string $fio
	 * @return $this
	 */
	public function fio ($fio)
	{
		$this->fio = $fio;
		return $this;
	}

	/**
	 * Start
	 *
	 * @param string $start
	 * @return $this
	 */
	public function start ($start)
	{
		$this->start = $start;
		return $this;
	}

	/**
	 * End
	 *
	 * @param string $end
	 * @return $this
	 */
	public function end ($end)
	{
		$this->end = $end;
		return $this;
	}

	/**
	 * Number
	 *
	 * @param int $number
	 * @return $this
	 */
	public function number ($number)
	{
		$this->number = $number;
		return $this;
	}

	public function applyFilter(array &$query)
	{
		parent::applyFilter($query);

		if($this->fio !== null){
			$this->addConditionsAnd($query, 'fio like ?', '%'.$this->fio.'%');
		}
		if($this->price !== null){
			$this->addConditionsAnd($query, 'total >= ?', $this->price);
		}
		if($this->start !== null){
			$this->addConditionsAnd($query, 'create_time >= ?', $this->start);
		}
		if($this->end !== null){
			$this->addConditionsAnd($query, 'create_time <= ?', $this->end);
		}
		if($this->number !== null){
			$this->addConditionsAnd($query, 'id = ?', $this->number);
		}
	}
}