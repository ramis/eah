<?php
namespace Shop\Filter;

class BaseFilter
{
	/**
	 * @var boolean
	 */
	protected $is_publish;

	/**
	 * @var string
	 */
	protected $sort;

	/**
	 * @var integer
	 */
	protected $id;

	/**
	 * @var integer
	 */
	protected $limit;

	/**
	 * @var integer
	 */
	protected $offset;

	/**
	 * @var array
	 */
	protected $not_ids;

	/**
	 * @param boolean $is_publish
	 * @return $this
	 */
	public function publish($is_publish)
	{
		$this->is_publish = $is_publish;
		return $this;
	}

	/**
	 * @param string $sort
	 * @return $this
	 */
	public function sort($sort = 'sort ASC')
	{
		$this->sort = $sort;
		return $this;
	}

	/**
	 * @param int $id
	 * @return $this
	 */
	public function id($id)
	{
		$this->id = $id;
		return $this;
	}

	/**
	 * @param array $not_ids
	 * @return $this
	 */
	public function notIds(array $not_ids)
	{
		$this->not_ids = $not_ids;
		return $this;
	}

	/**
	 * @param integer $limit
	 * @param integer $page
	 * @return $this
	 */
	public function limit($limit, $page = 1)
	{
		$this->offset = ($page - 1) * $limit;
		$this->limit = $limit;
		return $this;
	}

	protected function addConditionsAnd(array &$query, $where, $value = null)
	{
		if (isset($query['conditions']) && isset($query['conditions'][0])) {
			$conditions = $query['conditions'];
			$conditions[0] .= ' AND ' . $where;
		} else {
			$conditions[] = $where;
		}

		if ($value !== null) {
			$conditions[] = $value;
		}

		$query['conditions'] = $conditions;

	}

	protected function applyFilter(array &$query)
	{

		if ($this->is_publish !== null) {
			$this->addConditionsAnd($query, 'is_publish = ?', (int)$this->is_publish);
		}
		if ($this->id !== null) {
			$this->addConditionsAnd($query, 'id = ?', (int)$this->id);
		}
		if ($this->not_ids !== null) {
			$this->addConditionsAnd($query, 'id NOT IN (?)', $this->not_ids);
		}

		if ($this->limit !== null) {
			$query['limit'] = $this->limit;
			$query['offset'] = $this->offset;
		}

		if ($this->sort !== null) {
			$query['order'] = $this->sort;
		}
	}

}