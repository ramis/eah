<?php
namespace Shop\Filter\Shipping\Geo;

use Shop\Filter\Filter;
use Shop\Filter\BaseFilter;
use Shop\Model\Geo;
use Shop\Model\Shipping;

class Value extends BaseFilter implements Filter
{

	/**
	 * @var Geo
	 */
	private $geo;

	/**
	 * @var Shipping
	 */
	private $shipping;

	/**
	 * @param Geo $geo
	 * @return $this
	 */
	public function geo(Geo $geo)
	{
		$this->geo = $geo;
		return $this;
	}

	/**
	 * @param Shipping $shipping
	 * @return $this
	 */
	public function shipping(Shipping $shipping)
	{
		$this->shipping = $shipping;
		return $this;
	}

	public function applyFilter(array &$query)
	{
		parent::applyFilter($query);

		if ($this->geo !== null) {
			$this->addConditionsAnd($query, 'geo_id = ?', (int)$this->geo->id);
		}
		if ($this->shipping !== null) {
			$this->addConditionsAnd($query, 'shipping_id = ?', (int)$this->shipping->id);
		}
	}
}