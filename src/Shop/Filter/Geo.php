<?php
namespace Shop\Filter;

class Geo extends BaseFilter implements Filter
{

	/**
	 * @var string
	 */
	private $alias;

	/**
	 * @var string
	 */
	private $title;

	/**
	 * @param string $alias
	 * @return $this
	 */
	public function alias($alias)
	{
		$this->alias = $alias;
		return $this;
	}

	/**
	 * @param string $title
	 * @return $this
	 */
	public function title($title)
	{
		$this->title = $title;
		return $this;
	}

	public function applyFilter(array &$query)
	{
		parent::applyFilter($query);

		if ($this->alias !== null) {
			$this->addConditionsAnd($query, 'alias like ?', '%|' . $this->alias . '|%');
		}
		if ($this->title !== null) {
			$this->addConditionsAnd($query, 'title = ?', $this->title);
		}
	}

}