<?php
namespace Shop\Filter;

class Image extends BaseFilter implements Filter
{

	private $parent;
	private $parent_id;

	/**
	 * @param string $parent
	 * @return $this
	 */
	public function parent($parent)
	{
		$this->parent = $parent;
		return $this;
	}

	/**
	 * @param int $parent_id
	 * @return $this
	 */
	public function parentId($parent_id)
	{
		$this->parent_id = $parent_id;
		return $this;
	}

	/**
	 * @param array $query
	 */
	public function applyFilter(array &$query)
	{
		parent::applyFilter($query);

		if($this->parent_id !== null){
			$this->addConditionsAnd($query, 'parent_id = ?', $this->parent_id);
		}
		if($this->parent !== null){
			$this->addConditionsAnd($query, 'parent = ?', $this->parent);
		}
	}
}