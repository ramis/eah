<?php
namespace Shop\Filter\Users;

use Shop\Filter\Filter;
use Shop\Filter\BaseFilter;

class Group extends BaseFilter implements Filter
{

	public function applyFilter(array &$query)
	{
		parent::applyFilter($query);
	}
}