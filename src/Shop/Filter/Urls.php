<?php
namespace Shop\Filter;

class Urls extends BaseFilter implements Filter
{

	/**
	 * int
	 */
	private $parent_id;

	/**
	 * string
	 */
	private $parent_table;

	/**
	 * По таблице
	 *
	 * @param string $parent_table
	 * @return $this
	 */
	public function parentTable($parent_table)
	{
		$this->parent_table = $parent_table;
		return $this;
	}

	/**
	 * По ид
	 *
	 * @param int $parent_id
	 * @return $this
	 */
	public function parentId($parent_id)
	{
		$this->parent_id = $parent_id;
		return $this;
	}


	public function applyFilter(array &$query)
	{
		parent::applyFilter($query);

		if ($this->parent_id !== null) {
			$this->addConditionsAnd($query, 'parent_id = ? ', $this->parent_id);
		}

		if ($this->parent_table !== null) {
			$this->addConditionsAnd($query, 'parent_table = ? ', $this->parent_table);
		}

	}
}