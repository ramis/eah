<?php
namespace Shop\Filter;

class Users extends BaseFilter implements Filter
{
	/**
	 * @var string
	 */
	private $login, $pass;

	/**
	 * @var boolean
	 */
	private $is_active;

	/**
	 * По имени пользователя
	 *
	 * @param string $login
	 * @return $this
	 */
	public function login ($login){
		$this->login = $login;
		return $this;
	}

	/**
	 * По паролю пользователя
	 *
	 * @param string $pass
	 * @return $this
	 */
	public function pass ($pass){
		$this->pass = $pass;
		return $this;
	}

	/**
	 * Активен
	 *
	 * @param boolean $is_active
	 * @return $this
	 */
	public function active ($is_active){
		$this->is_active = $is_active;
		return $this;
	}

	/**
	 * Применить фильтр к запросу
	 *
	 * @param array $query
	 * @return void
	 */
	public function applyFilter (array &$query){

		parent::applyFilter($query);


		if($this->login !== null){
			$this->addConditionsAnd($query, 'login = ?', $this->login);
		}

		if($this->pass !== null){
			$this->addConditionsAnd($query, 'password_md5 = ?', $this->pass);
		}

		if($this->is_active !== null){
			$this->addConditionsAnd($query, 'is_active = ?', (int)$this->is_active);
		}
	}
}