<?php
namespace Shop\Filter\Order;

use Shop\Filter;
use Shop\Model\Order;

class Product extends Filter\BaseFilter implements Filter\Filter
{
	/**
	 * @var Order
	 */
	private $order;

	public function order(Order $order)
	{
		$this->order = $order;
	}

	public function applyFilter(array &$query)
	{
		parent::applyFilter($query);

		if ($this->order !== null) {
			$this->addConditionsAnd($query, 'order_id = ? ', $this->order->id);
		}
	}
}