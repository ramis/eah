<?php
namespace Shop\Filter;

class Message extends BaseFilter implements Filter
{
	/**
	 * @var string
	 */
	private $start;
	/**
	 * @var string
	 */
	private $end;

	/**
	 * Start create_time
	 *
	 * @param string $start
	 * @return Filter $this
	 */
	public function start($start)
	{
		$this->start = $start;
		return $this;
	}

	/**
	 * End create_time
	 *
	 * @param string $end
	 * @return Filter $this
	 */
	public function end($end)
	{
		$this->end = $end;
		return $this;
	}

	public function applyFilter(array &$query)
	{
		parent::applyFilter($query);

		if($this->start !== null){
			$this->addConditionsAnd($query, 'create_time >= ?', $this->start);
		}
		if($this->end !== null){
			$this->addConditionsAnd($query, 'create_time <= ?', $this->end);
		}
	}
}