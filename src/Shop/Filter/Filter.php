<?php
namespace Shop\Filter;

interface Filter{

	/**
	 * Применить фильтр
	 *
	 * @param array $query
	 * @return void
	 */
	public function applyFilter (array &$query);

}