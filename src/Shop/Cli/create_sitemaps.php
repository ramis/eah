<?php
require 'init.php';

$logs = Log::info('Start create sitemap');

$file = PATH . "src/Shop/Domain/Front/htdocs/sitemap.xml";

if (file_exists($file)) {
	$logs .= Log::info('Delete old file sitemap');
	unlink($file);
}

/**
 * @param DOMElement $urlset
 * @param $loc
 * @param $lastmod
 * @param $changefreq
 * @param $priority
 * @return DOMNode
 */
function appChild(DOMElement $urlset, $loc, $lastmod, $changefreq, $priority)
{
	$element = $urlset->appendChild(new DOMElement('url'));
	$element->appendChild(new DOMElement('loc', $loc));
	$element->appendChild(new DOMElement('lastmod', $lastmod));
	$element->appendChild(new DOMElement('changefreq', $changefreq));
	$element->appendChild(new DOMElement('priority', $priority));
	return $element;
}

$dom = new DOMDocument('1.0', 'UTF-8');
$dom->formatOutput = true;
$urlset = $dom->appendChild(new DOMElement('urlset'));
$urlset->setAttributeNode(new DOMAttr('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9'));

$logs .= Log::info('Add page / sitemap');
$main = appChild($urlset, HTTP_FRONT, date("Y-m-d"), 'daily', '0.8');
$logs .= Log::info('Add page /search/ sitemap');
$search = appChild($urlset, HTTP_FRONT . 'search/', date("Y-m-d"), 'daily', '0.5');

//category
$filter = new \Shop\Filter\Category();
$filter->publish(true);

$categories = \Shop\Model\Category::fetch($filter);
$logs .= Log::info('Start add categories page sitemap');
foreach ($categories as $c) {
	appChild($urlset, HTTP_FRONT . $c->url . '/', date("Y-m-d"), 'daily', '0.5');
	$logs .= Log::info('Add page /' . $c->url . '/ sitemap');
}
$logs .= Log::info('End add categories page sitemap');

//information
$information = \Shop\Model\Information::fetch(new \Shop\Filter\Information());
$logs .= Log::info('Start add informations page sitemap');
foreach ($information as $i) {
	appChild($urlset, HTTP_FRONT . $i->url . '/', date("Y-m-d"), 'daily', '0.5');
	$logs .= Log::info('Add page /' . $i->url . '/ sitemap');
}
$logs .= Log::info('End add informations page sitemap');

$filter = new \Shop\Filter\Product();
$filter->publish(true);
$products = \Shop\Model\Product::fetch($filter);
$logs .= Log::info('Start add product page sitemap');
foreach ($products as $p) {
	appChild($urlset, HTTP_FRONT . 'product/' . $p->id . '/', date("Y-m-d"), 'daily', '0.6');
}
$logs .= Log::info('Add ' . count($products) . '  product page sitemap');
$logs .= Log::info('End add product page sitemap');

file_put_contents($file, $dom->saveXML());
$logs .= Log::info('End create sitemap');

$l = new \Shop\Model\Logs();
$l->create_time = date("Y-m-d H:s:i");
$l->log = $logs;
$l->title = 'Генерация sitemap file';
$l->cron = __FILE__;
$l->save();