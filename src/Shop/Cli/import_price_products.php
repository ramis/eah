<?php

require 'init.php';

class SimpleDMOZParser
{
	private $_stack = array();
	private $_file = "";
	private $_parser = null;

	private $_tag_name = '';
	private $_is_offer = false;
	private $products = array();

	public function __construct(array $products, $file)
	{
		$this->_file = $file;
		$this->products = $products;

		$this->_parser = xml_parser_create("UTF-8");
		xml_set_object($this->_parser, $this);
		xml_set_element_handler($this->_parser, "startTag", "endTag");
		xml_set_character_data_handler($this->_parser, "tagData");
	}

	public function startTag($parser, $name, $attribs)
	{
		if ($name === 'OFFER' && isset($attribs['ID']) && $id = (int)$attribs['ID']) {
			if (isset($this->products[$id])) {
				$this->_is_offer = true;
				$this->_stack = array();
			}
		}
		if ($this->_is_offer) {
			$this->_tag_name = $name;
			foreach ($attribs as $key => $val) {
				if (isset($this->_stack[$this->_tag_name])) {
					$this->_tag_name = $key;
				}
				$this->_stack[$this->_tag_name][$key] = $val;
			}
		}
	}

	public function endTag($parser, $name)
	{
		if ($this->_is_offer && $name === 'OFFER') {
			$this->_is_offer = false;
			$this->products[$this->_stack['OFFER']['ID']]['price'] =
				(isset($this->_stack['PRICE']['tagData']) ? (int)$this->_stack['PRICE']['tagData'] : 0);
			$this->products[$this->_stack['OFFER']['ID']]['quantity'] =
				(isset($this->_stack['PARAM']['tagData']) ? (int)$this->_stack['PARAM']['tagData'] : 0);
			$this->products[$this->_stack['OFFER']['ID']]['viewed'] =
				(isset($this->_stack['NAME']['tagData']) ? (int)$this->_stack['NAME']['tagData'] : 0);
		}
	}

	function tagData($parser, $tagData)
	{
		if (trim($tagData)) {
			if ($this->_is_offer) {
				$this->_stack[$this->_tag_name]['tagData'] = $tagData;
			}
		}
	}

	public function parse()
	{
		$fh = fopen($this->_file, "r");
		if (!$fh) {
			throw new \Exception('No get files');
		}

		while (!feof($fh)) {
			$data = fread($fh, 4096);
			xml_parse($this->_parser, $data, feof($fh));
		}
	}

	public function getProducts()
	{
		return $this->products;
	}
}

$logs = Log::info('Start import price v3toys');

$logs .= Log::info('Get products whose code is greater than 0');

$filter = new \Shop\Filter\Product();
$filter->codeMore(0);
$products = \Shop\Model\Product::fetch($filter);
foreach($products as $p){
	$products[$p->code] = array(
		'product_id' => $p->id,
		'import_id' => $p->code,
		'price' => 0,
		'quantity' => 0,
		'viewed' => 0,
	);
}
$logs .= Log::info('Products get - ' . count($products));

$logs .= Log::info('Start parse xml');
$file = "http://www.v3toys.ru/prods.yml";

try {
	$parser = new SimpleDMOZParser($products, $file);
	$parser->parse();

}catch (Exception $e){
	$logs .= Log::error($e->getMessage());
}
$logs .= Log::info('End parse xml');
$logs .= Log::info('Start save price quantity viewed by product');

foreach ($parser->getProducts() as $p) {
	$product = \Shop\Model\Product::fetchById($p['product_id']);
	if($product){
		$is_save = false;
		if($p['price'] !== $products[$p->code]['price']){
			$is_save = true;
		}
		if($p['quantity'] !== $products[$p->code]['quantity']){
			$is_save = true;
		}
		if($p['viewed'] !== $products[$p->code]['viewed']){
			$is_save = true;
		}
		if($is_save){
			$logs .= Log::info('Product ' . $p->code . ' changed');
			$product->price = $p['price'];
			$product->quantity = (int)$p['quantity'];
			$product->viewed = (int)$p['viewed'];
			$product->change();
			if($p['price'] !== $products[$p->code]['price']){
				$logs .= Log::info('Product new price ' . $p['price']);
			}
			if($p['quantity'] !== $products[$p->code]['quantity']){
				$logs .= Log::info('Product new quantity ' . $p['quantity']);
			}
			if($p['viewed'] !== $products[$p->code]['viewed']){
				$logs .= Log::info('Product new viewed ' . $p['viewed']);
			}
			$logs .= Log::info('Product ' . $p->code . ' save');
		}else{
			$logs .= Log::info('Product ' . $p->code . ' no changed');
		}
	}
	unset($product);
}
$logs .= Log::info('End save price quantity viewed by product');

$l = new \Shop\Model\Logs();
$l->create_time = date("Y-m-d H:s:i");
$l->log = $logs;
$l->title = 'Импорт цен с v3Toys';
$l->cron = __FILE__;
$l->save();