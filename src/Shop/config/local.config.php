<?php
mb_internal_encoding("UTF-8");

//db
define ('DB_HOST', '127.0.0.1');
define ('DB_NAME', 'kre-o');
define ('DB_USER', 'root');
define ('DB_PASS', '');

//Domains
define ('PATH_FRONT', 'kre-o.lo');
define ('PATH_ADMIN', 'admin.kre-o.lo');
define ('HTTP_FRONT', 'http://' . PATH_FRONT . '/');
define ('HTTP_ADMIN', 'http://' . PATH_ADMIN . '/');
define ('HTTP_IMAGE', 'http://i.kre-o.lo/');

//Images
define ('PATH_IMAGE', PATH . 'images/data/');
define ('PATH_IMAGE_CACHE', PATH . 'images/cache/');

define ('HTTP_IMAGE_BASE', HTTP_IMAGE . 'data/');
define ('HTTP_IMAGE_CACHE', HTTP_IMAGE . 'cache/');
//resize
define ('HTTP_IMAGE_100x100', HTTP_IMAGE . 'cache/100x100/');
define ('HTTP_IMAGE_200x200', HTTP_IMAGE . 'cache/200x200/');
define ('HTTP_IMAGE_500x500', HTTP_IMAGE . 'cache/500x500/');
define ('HTTP_IMAGE_70x70',   HTTP_IMAGE . 'cache/70x70/');
define ('HTTP_IMAGE_150x150', HTTP_IMAGE . 'cache/150x150/');
define ('HTTP_IMAGE_340x340', HTTP_IMAGE . 'cache/340x340/');


function vd($t){
	print '<pre>';var_dump($t);die;
}
