<?php

namespace Shop\Common;

use Shop\Model\Order\Product as OrderProduct;
use Shop\Model\Catalog\Product;
use Shop\Model\Shipping\Geo\Value;

class Cart
{
	/**
	 * @var array of OrderProduct
	 */
	private $products;

	/**
	 * @var Value
	 */
	private $shipping_geo_value;

	/**
	 * @var Cart
	 */
	private static $instance;

	/**
	 *
	 */
	private function __construct()
	{
		$this->products = array();
	}

	/**
	 * @return Cart
	 */
	public static function getInstance()
	{
		if (self::$instance === null) {
			if (isset($_SESSION['cart']) && $_SESSION['cart'] instanceof Cart) {
				self::$instance = $_SESSION['cart'];
			} else {
				self::$instance = new Cart;
				self::$instance->products = array();
				$_SESSION['cart'] = self::$instance;
			}
		}
		return self::$instance;
	}

	/**
	 * @param Product $product
	 * @param int $quantity
	 * @param boolean $is_quantity_replase
	 * @return bool
	 */
	public function addProduct(Product $product, $quantity, $is_quantity_replase = false)
	{
		$product_offer = isset($this->products[$product->id]) ? $this->products[$product->id] :
			new OrderProduct();

		$product_offer->catalog_product_id = $product->id;
		$product_offer->catalog_product_title = $product->title;
		$product_offer->catalog_product_article = $product->article;
		$product_offer->catalog_product_code = $product->code;
		$product_offer->price = $product->special_price ? (int)$product->special_price : (int)$product->price;

		if ($is_quantity_replase) {
			$product_offer->quantity = (int)$quantity;
		} else {
			$product_offer->quantity += (int)$quantity;
		}

		$product_offer->total = $product_offer->quantity * $product_offer->price;

		$this->products[$product->id] = $product_offer;

		return true;
	}

	/**
	 * @param Product $product
	 */
	public function removeProduct(Product $product)
	{
		if (isset($this->products[$product->id])) {
			unset($this->products[$product->id]);
		}
	}

	/**
	 * Очистить корзину
	 */
	public function clean()
	{
		unset($this->products);
		$this->products = array();
		self::$instance = null;
	}

	/**
	 * @return array of OrderProduct
	 */
	public function getProducts()
	{
		return ($this->products ?: array());
	}

	/**
	 * Итого
	 *
	 * @return int
	 */
	public function getTotal()
	{
		$total = 0;
		foreach ($this->products as $product) {
			$total += $product->total;
		}
		return $total;
	}

	/**
	 * Итого + доставка
	 *
	 * @return int
	 */
	public function getTotalFull()
	{
		return $this->getTotal() + $this->getShippingCost();
	}

	/**
	 * @return int
	 */
	public function getCountProduct()
	{
		$count = 0;
		foreach ($this->products as $p) {
			$count += $p->quantity;
		}

		return $count;
	}

	/**
	 * Стоимость доставки
	 *
	 * @return Value
	 */
	public function getShippingGeoValue()
	{
		return $this->shipping_geo_value;
	}

	/**
	 * @param Value $shipping_geo_value
	 */
	public function setShippingGeoValue(Value $shipping_geo_value)
	{
		$this->shipping_geo_value = $shipping_geo_value;
	}

	/**
	 * Стоимость доставки
	 *
	 * @return int
	 */
	public function getShippingCost()
	{
		return $this->shipping_geo_value ? (int)$this->shipping_geo_value->price : 0;
	}
}

