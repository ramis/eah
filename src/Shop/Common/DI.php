<?php

namespace Shop\Common;

class DI
{
	/**
	 * @var array
	 */
	private static $values;

	/**
	 * @var DI
	 */
	private static $instance;

	private function __construct()
	{
		$this->values = array();
	}

	/**
	 * @return DI
	 */
	public static function getInstance()
	{
		if(self::$instance === null){
			self::$instance = new DI();
		}
		return self::$instance;
	}

	/**
	 * @param $key
	 * @return value|bool
	 */
	public static function get($key)
	{
		self::getInstance();
		return isset(self::$values[$key]) ? self::$values[$key] : false;
	}

	/**
	 * @param string $key
	 * @param $value
	 * @return void
	 */
	public static function set($key, $value)
	{
		self::getInstance();
		self::$values[$key] = $value;
	}

}

