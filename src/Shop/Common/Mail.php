<?php

namespace Shop\Common;

class Mail
{
	/**
	 * @var string
	 */
	private $to;

	/**
	 * @var string
	 */
	private $from;

	/**
	 * @var string
	 */
	private $sender;

	/**
	 * @var string
	 */
	private $subject;

	/**
	 * @var string
	 */
	private $message;

	/**
	 * @param string $to
	 */
	public function setTo($to) {
		$this->to = $to;
	}

	/**
	 * @param string $from
	 */
	public function setFrom($from) {
		$this->from = $from;
	}

	/**
	 * @param string $sender
	 */
	public function setSender($sender) {
		$this->sender = $sender;
	}

	/**
	 * @param string $subject
	 */
	public function setSubject($subject) {
		$this->subject = $subject;
	}

	/**
	 * @param string $message
	 */
	public function setMessage($message) {
		$this->message = $message;
	}

	/**
	 * Send
	 */
	public function send() {

		if (!$this->to) {
			return;
		}

		if (!$this->from) {
			return;
		}

		if (!$this->sender) {
			return;
		}

		if (!$this->subject) {
			return;
		}

		if ((!$this->message)) {
			return;
		}

		$to = is_array($this->to) ? implode(',', $this->to) : $this->to;

		$header = "MIME-Version: 1.0\n";

		$header .= 'To: ' . $to . "\n";
		$header .= 'Subject: ' . $this->subject . "\n";
		$header .= 'Date: ' . date('D, d M Y H:i:s O') . "\n";
		$header .= 'From: ' . '=?UTF-8?B?' . base64_encode($this->sender) . '?=' . '<' . $this->from . '>' . "\n";
		$header .= 'Reply-To: ' . '=?UTF-8?B?' . base64_encode($this->sender) . '?=' . '<' . $this->from . '>' . "\n";
		$header .= 'Return-Path: ' . $this->from . "\n";
		$header .= 'Content-Type: text/html; charset="utf-8"' . "\n";

		$message  = 'Content-Type: text/html; charset="utf-8"' . "\n";
		$message .= 'Content-Transfer-Encoding: 8bit' . "\n\n";
		$message .= $this->message . "\n";

//		ini_set('sendmail_from', $this->from);
		mail($to, '=?UTF-8?B?' . base64_encode($this->subject) . '?=', $message, $header);

	}
}
