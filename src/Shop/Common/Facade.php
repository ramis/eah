<?php

namespace Shop\Common;

use Slim\Slim;

class Facade extends Slim
{
	/**
	 * @var string
	 */
	private $domain = '';

	/**
	 * @var Controller[]
	 */
	private static $controllers = array();

	public function __construct($template_path)
	{
		parent::__construct(array('templates.path' => $template_path));
	}

	/**
	 * @param string $domain
	 */
	public function setDomain($domain)
	{
		$this->domain = $domain;
	}

	/**
	 * Создаем контроллер
	 *
	 * @param string $controller
	 * @return Controller
	 */
	public function createController($controller)
	{

		if (!isset(self::$controllers[$controller])) {

			$me = $this;

			$controller_object = call_user_func((function ($controller, $me) {

				$namespace_controller = "\Shop\Domain\\" . $this->domain . "\Controller\%s";
				$controller = array_map(function ($c) {
					return ucfirst($c);
				}, explode('.', $controller));
				$class_name = sprintf($namespace_controller, implode('\\', $controller));

				return new $class_name($me);
			}), $controller, $me);

			self::$controllers[$controller] = $controller_object;
		}

		return self::$controllers[$controller];
	}

	/**
	 * Создает объект маршрута для урла
	 *
	 * @param string $route
	 * @param string $action
	 * @return \Slim\Route
	 */
	public function addRoute($route, $action)
	{
		$me = $this;

		$mapRoute = function () use ($me, $action) {
			if ($action instanceof \Closure) {
				call_user_func_array($action, func_get_args());
			} else {
				if (!strpos($action, "::")) {
					throw new \Exception('Wrong action format');
				}
				list($controller_name, $action) = explode("::", $action);
				$controller = $me->createController($controller_name);
				$action = 'action' . ucfirst($action);
				call_user_func_array(array($controller, $action), func_get_args());
			}
		};

		$objectRoute = $this->map($route, $mapRoute);

		$objectRoute->via('GET');

		return $objectRoute;
	}

}