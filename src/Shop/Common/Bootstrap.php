<?php

namespace Shop\Common;

use ActiveRecord\Config;

class Bootstrap
{

	/**
	 * Init session
	 */
	public function initSession()
	{
		session_start();
	}

	/**
	 * load Config
	 *
	 * @return void
	 */
	public function initConfig()
	{
		include_once(PATH . 'src/Shop/config/' . APPLICATION_ENV . '.config.php');
	}


	/**
	 * Init Active Record
	 *
	 * @return void
	 */
	public function initActiveRecord()
	{
		Config::initialize(function ($cfg) {
			$cfg->set_model_directory(PATH . 'src/Shop/Model');
			$cfg->set_connections(
				array('development' =>
					'mysql://' . DB_USER . ':' . DB_PASS . '@' . DB_HOST . '/' . DB_NAME . '?charset=utf8'));
		});

	}

	/**
	 * Init Memcached
	 *
	 * @return void
	 */
	public function initMemcached()
	{
		//$memcached = new \Memcached();
		//$memcached->addServer('localhost', 11211);
		//DI::set('memcached', $memcached);
	}

}