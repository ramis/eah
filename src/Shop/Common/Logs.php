<?php

namespace Shop\Common;

class Logs
{
	/**
	 * Info Message
	 *
	 * @param string $message
	 * @return string
	 */
	public static function info ($message){
		return '<div class="alert alert-success" role="alert"><code>' . date('Y-m-d H:i:s') . '</code> ' . $message . '</div>';
	}

	/**
	 * Error message
	 *
	 * @param string $message
	 * @return string
	 */
	public static function error ($message){
		return '<div class="alert alert-danger" role="alert"><code>' . date('Y-m-d H:i:s') . '</code> ' . $message . '</div>';
	}
}

