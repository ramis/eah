<?php

namespace Shop\Common;

class ImagesStorage
{
	const EXT_JPEG = 'jpeg';
	const QUALITY = 90;

	/**
	 * Путь к картинке , по хешу
	 *
	 * @param string $hash
	 * @return string
	 */
	public static function getPath($hash)
	{
		return (!is_file(PATH_IMAGE . $hash)) ? false : PATH_IMAGE . '' . $hash;
	}

	/**
	 * Создаем кеш картинки для заданных размеров
	 *
	 * @param string $hash
	 * @param integer $width
	 * @param integer $height
	 * @return boolean
	 */
	public static function imageCreateCache($hash, $width, $height)
	{
		/**
		 * Проверяем если ли картинка в хранилище
		 */
		if (!is_file(PATH_IMAGE . $hash)) {
			return false;
		}

		//Путь в кеш
		$path = PATH_IMAGE_CACHE . '' . $width . 'x' . $height . '/' . $hash;

		/**
		 * Если картинки нет в кеше, делаем этот кеш
		 */
		return is_file($path) ? true : self::resize($hash, $width, $height);
	}

	/**
	 * Отресайзить картинку по заданным размерам, сохранить в кеше
	 *
	 * @param string $hash
	 * @param integer $width
	 * @param integer $height
	 * @return boolean
	 */
	private static function resize($hash, $width, $height)
	{
		$path = PATH_IMAGE_CACHE . '' . $width . 'x' . $height . '/' . $hash;
		$base_path = PATH_IMAGE . $hash;
		if (!is_file($path) && is_file($base_path)) {

			$info = getimagesize($base_path);
			if($info === false){
				return false;
			}

			$width_base = (int)$info[0];
			$height_base = (int)$info[1];

			$scale_w = $width / $width_base;
			$scale_h = $height / $height_base;
			$scale = min($scale_w, $scale_h);

			if ($scale === 1 && $scale_h === $scale_w && $info['mime'] !== 'image/png') {
				return false;
			}

			$new_width = (int)($width_base * $scale);
			$new_height = (int)($height_base * $scale);

			$x_pos = (int)(($width - $new_width) / 2);
			$y_pos = (int)(($height - $new_height) / 2);

			switch ($info['mime']) {
				case 'image/gif':
					$image_old = imagecreatefromgif($base_path);
					break;
				case 'image/png':
					$image_old = imagecreatefrompng($base_path);
					break;
				case 'image/jpeg':
				default:
					$image_old = imagecreatefromjpeg($base_path);
			}

			$image = imagecreatetruecolor($width, $height);

			if (isset($info['mime']) && $info['mime'] === 'image/png') {
				imagealphablending($image, false);
				imagesavealpha($image, true);
				$background = imagecolorallocatealpha($image, 255, 255, 255, 127);
				imagecolortransparent($image, $background);
			} else {
				$background = imagecolorallocate($image, 255, 255, 255);
			}

			imagefilledrectangle($image, 0, 0, $width, $height, $background);

			imagecopyresampled(
				$image,
				$image_old,
				$x_pos, $y_pos,
				0, 0,
				$new_width, $new_height,
				$width_base, $height_base
			);

			if (is_resource($image)) {
				switch ($info['mime']) {
					case 'image/gif':
						imagegif($image, $path, self::QUALITY);
						break;
					case 'image/png':
						imagepng($image, $path, self::QUALITY / 10);
						break;
					case 'image/jpeg':
					default:
						imagejpeg($image, $path, self::QUALITY);
				}
				imagedestroy($image);
			}
			imagedestroy($image_old);

			return is_file($path);
		}

		return false;
	}

	/**
	 * Сохранить картинку в хранилище
	 *
	 * @param string $path
	 * @param string $ext
	 * return string|boolean
	 */
	public static function set($path, $ext = self::EXT_JPEG)
	{
		if (!is_file($path)) {
			return false;
		}

		return self::setContent(file_get_contents($path), $ext);
	}

	/**
	 * Сохранить контент картинки в хранилище
	 *
	 * @param string $content
	 * @param string $ext
	 * return string|boolean
	 */
	public static function setContent($content, $ext = self::EXT_JPEG)
	{
		$hash = uniqid() . rand(0, 5) . '.' . $ext;

		$result = file_put_contents(PATH_IMAGE . $hash, $content);

		return ($result === false ? false : $hash);
	}

	/**
	 * Удалить файл
	 *
	 * @static
	 * @param string $hash
	 * @return boolean
	 */
	public static function delete($hash)
	{
		if (!is_file(PATH_IMAGE . $hash)) {
			return false;
		}

		unlink(PATH_IMAGE . $hash);

		return true;
	}

}