<?php

namespace Shop\Common;

class Hierarchy
{

	/**
	 * Выстраивание массива из "дерева"
	 *
	 * @param array $arrayItem
	 * @param integer $key = 0
	 * @param string $field = 'id'
	 * @param integer $lvl = 0
	 * @return array
	 */
	public static function tree(array $arrayItem, $key = 0, $field = 'id', $lvl = 0)
	{
		$result = array();

		if (!isset($arrayItem[$key])) {
			return $result;
		}

		foreach ($arrayItem[$key] as $item) {
			$item['lvl'] = $lvl;
			$result[] = $item;
			if (isset($arrayItem[$item[$field]])) {
				$result = array_merge($result, self::tree($arrayItem, $item[$field], $field, ($lvl+1)));
			}
		}

		return $result;
	}

	/**
	 * Выстраивание массива из "графа"
	 *
	 * @param array $arrayItem
	 * @param array $arrayIteration
	 * @param array $arrayVisited
	 * @param string $field = 'id'
	 * @param integer $lvl = 0
	 * @return array
	 */
	public static function graph(
		array $arrayItem,
		array $arrayIteration = array(),
		array &$arrayVisited = array(),
		$field = 'id',
		$lvl = 0
	)
	{
		$result = array();
		if(empty($arrayIteration)){
			$arrayIteration = $arrayItem;
		}

		foreach ($arrayIteration as $item) {
			if ($lvl === 0 && $item['is_parent']) {
				continue;
			}

			$item['lvl'] = $lvl;
			$result[] = $item;
			$arrayVisited[] = $item[$field];
			if ($item['is_recursion'] && isset($item['child'])) {
				$childs = array();
				foreach($item['child'] as $child){
					if(isset($arrayItem[$child])){
						$arrayItem[$child]['is_recursion'] = !in_array($child, $arrayVisited);
						$childs[] = $arrayItem[$child];
					}
				}

				if(!empty($childs)){
					$result = array_merge($result, self::graph($arrayItem, $childs, $arrayVisited, $field, ($lvl+1)));
				}

			}

			if($lvl === 0){
				$arrayVisited = array();
			}

		}

		return $result;
	}

}